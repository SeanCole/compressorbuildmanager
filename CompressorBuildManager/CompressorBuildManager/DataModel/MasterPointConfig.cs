﻿using CompressorTemplateDataModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XmlDataModelUtility;

namespace CompressorBuildManagerWPF.DataModel
{
    public class MasterPointConfig : NotifyCopyDataModel
    {
        public MasterPointConfig()
        {
            MasterPointList = new BindingList<DTFPoint>();
        }

        public BindingList<DTFPoint> MasterPointList
        {
            get => GetPropertyValue<BindingList<DTFPoint>>();
            set => SetPropertyValue(value);
        }        

        public DTFPoint AddNewPoint(int index)
        {
            var NewPoint = new DTFPoint();
            MasterPointList.Insert(index, NewPoint);

            return NewPoint;
        }

        public void DeletePoint(int index)
        {
            if (index < 0)
                return;
            MasterPointList.RemoveAt(index);
        }
    }
}
