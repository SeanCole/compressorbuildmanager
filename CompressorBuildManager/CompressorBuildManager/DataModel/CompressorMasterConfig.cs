﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using XmlDataModelUtility;

namespace CompressorBuildManagerWPF.DataModel
{
    [Serializable]
    [XmlRoot("ConfigItems"), XmlType("ConfigItems")]
    public class CompressorMasterConfig : NotifyCopyDataModel
    {
        public CompressorMasterConfig()
        {
            PointConfig = new MasterPointConfig();
            CompressorProgramConfig = new CompProgramConfig();
        }

        public MasterPointConfig PointConfig
        {
            get => GetPropertyValue<MasterPointConfig>();
            set => SetPropertyValue(value);
        }

        public CompProgramConfig CompressorProgramConfig
        {
            get => GetPropertyValue<CompProgramConfig>();
            set => SetPropertyValue(value);
        }
    }
}
