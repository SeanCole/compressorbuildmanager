﻿
using CompressorTemplateDataModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Xml.Serialization;
using CompressorBuildManagerWPF.ViewModels;
using GenericRuleModel.Rules;
using Microsoft.Win32;
using XmlDataModelUtility;
using System.IO.Compression;

namespace CompressorBuildManagerWPF.DataModel
{
    public class CompProgramConfig : NotifyCopyDataModel
    {
        public CompProgramConfig()
        {
            CompPrograms = new BindingList<CompProgram>();
        }

        public BindingList<CompProgram> CompPrograms
        {
            get => GetPropertyValue<BindingList<CompProgram>>();
            set => SetPropertyValue(value);
        }
        public CompProgram AddNewPoint(int index)
        {
            var newProgram = new CompProgram();
            CompPrograms.Insert(index, newProgram);

            return newProgram;
        }
        
        public CompProgram SelectedProgram
        {
            get => GetPropertyValue<CompProgram>();
            set => SetPropertyValue(value);
        }

        public ICommand ExportProgramCommand => new DelegateCommand(ExportProgram);
        public void ExportProgram(object inputObject)
        {
            var saveFileDialog = new SaveFileDialog
            {
                // Browse for path
                DefaultExt = "ctmpl",
                Filter = "Compressor Template Files (*.ctmpl)|*.ctmpl"
            };

            if (saveFileDialog.ShowDialog(Application.Current.MainWindow) == true)
            {
                var filePath = System.IO.Path.GetDirectoryName(saveFileDialog.FileName);
                var fileName = System.IO.Path.GetFileName(saveFileDialog.FileName);
                SelectedProgram.CompPointsInUse = SelectedProgram.CompPoints.Where(x => x.InUse).ToList();
                SelectedProgram.EnumTables = EnumerationTableHandler.EnumTableDefinition;

                try
                {
                    using (FileStream outFile = File.Create(Path.Combine(filePath, fileName)))
                    using (GZipStream compress = new GZipStream(outFile, CompressionMode.Compress))
                    using (StreamWriter sw = new StreamWriter(compress))
                    //using (var sw = new StreamWriter(Path.Combine(filePath, fileName)))
                    {
                        var xmlSerial = new XmlSerializer(typeof(CompProgram), DerivedTypes);
                        xmlSerial.Serialize(sw, SelectedProgram);
                    }
                }
                catch (Exception ex)
                {
                    // Log.Error(ex, $@"Error saving config file to {FilePath}\{FileName}");
                    //return false;
                }
            }
        }

        public void DeletePoint(int index)
        {
            if (index < 0)
                return;
            CompPrograms.RemoveAt(index);
        }
    }
}
