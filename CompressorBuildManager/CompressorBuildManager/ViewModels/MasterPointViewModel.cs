﻿using CompressorBuildManagerWPF.DataModel;
using CompressorTemplateDataModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using XmlDataModelUtility;
using static CompressorTemplateDataModel.DTFPoint;

namespace CompressorBuildManagerWPF.ViewModels
{
    public class MasterPointViewModel : NotifyDynamicBase<MasterPointConfig>
    {
        public MasterPointViewModel(MasterPointConfig srcModel) : base(srcModel)
        {
            MasterPointListVM = new BindingList<DTFPointViewModel>();
            foreach (var DTFPoint in srcModel.MasterPointList)
            {
                MasterPointListVM.Add(new DTFPointViewModel(DTFPoint));
            }
        }
     
        public DTFPointViewModel SelectedDTFPoint
        {
            get => GetPropertyValue<DTFPointViewModel>();
            set => SetPropertyValue(value);
        }
         
        public BindingList<DTFPointViewModel> MasterPointListVM
        {
            get => GetPropertyValue<BindingList<DTFPointViewModel>>();
            set => SetPropertyValue(value);
        }

        public ICommand AddRow => new DelegateCommand(AddNewRow);
        public void AddNewRow(object inputObject)
        {
            if (SelectedDTFPoint == null)
            {
                var newPointView = new DTFPointViewModel(MyDataModel.AddNewPoint(MasterPointListVM.Count));
                MasterPointListVM.Add(newPointView);
                SelectedDTFPoint = MasterPointListVM[MasterPointListVM.Count - 1];
                return;
            }

            var index = MasterPointListVM.IndexOf(SelectedDTFPoint);
            if (index < 0)
                index = 0;
            else if (index >= MasterPointListVM.Count)
                index = MasterPointListVM.Count - 1;


            var ruleView = new DTFPointViewModel(MyDataModel.AddNewPoint(index));
            MasterPointListVM.Insert(index, ruleView);


            if (index < 0)
                index = 0;
            else if (index >= MasterPointListVM.Count)
                index = MasterPointListVM.Count - 1;

            SelectedDTFPoint = MasterPointListVM[index];
        }

        public void AddNewRow(string DefaultUDC, string Register, string Desc, string group, ModBusDataTypes DataType)
        {
            DTFPoint newPoint;
            DTFPointViewModel newPointViewModel;
            newPoint = new DTFPoint();
            if (SelectedDTFPoint== null)
            {
                newPoint.DataType = DataType;
                
                newPoint.Grouping = group;
                newPoint.Description = Desc;
               
                newPoint.DefaultUDC = DefaultUDC;
                MyDataModel.MasterPointList.Insert(MasterPointListVM.Count, newPoint);

                newPointViewModel = new DTFPointViewModel(newPoint);

                MasterPointListVM.Add(newPointViewModel);
                SelectedDTFPoint = MasterPointListVM[MasterPointListVM.Count - 1];
                return;
            }

            var index = MasterPointListVM.IndexOf(SelectedDTFPoint);
            if (index < 0)
                index = 0;
            else if (index >= MasterPointListVM.Count)
                index = MasterPointListVM.Count - 1;
            
            newPoint.DataType = DataType;
            
            newPoint.Grouping = group;
            newPoint.Description = Desc;
           
            newPoint.DefaultUDC = DefaultUDC;

            MyDataModel.MasterPointList.Insert(MasterPointListVM.Count, newPoint);

            newPointViewModel = new DTFPointViewModel(newPoint);

            MasterPointListVM.Insert(index, newPointViewModel);
            if (index < 0)
                index = 0;
            else if (index >= MasterPointListVM.Count)
                index = MasterPointListVM.Count - 1;

            SelectedDTFPoint= MasterPointListVM[index];

        }
        public ICommand DeleteRow => new DelegateCommand(DeleteSelectedRow);
        public void DeleteSelectedRow(object inputObject)
        {
            var index = MasterPointListVM.IndexOf(SelectedDTFPoint);
            MasterPointListVM.Remove(SelectedDTFPoint);
            MyDataModel.DeletePoint(index);
            if (MasterPointListVM.FirstOrDefault() == null)
                return;

            if (index < 0)
                index = 0;
            else if (index >= MasterPointListVM.Count)
                index = MasterPointListVM.Count - 1;

            SelectedDTFPoint = MasterPointListVM[index];
            
            return;

        }
    }

  
}
