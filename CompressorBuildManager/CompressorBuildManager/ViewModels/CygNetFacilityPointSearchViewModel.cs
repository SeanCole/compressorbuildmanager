using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reactive.Linq;
using System.Threading.Tasks;
using System.Windows;
using CygNetRuleModel.Models;
using DataMasterClient.Forms;
using ReactiveUI;
using Techneaux.CygNetWrapper.Facilities;
using Techneaux.CygNetWrapper.Facilities.Attributes;
using Techneaux.CygNetWrapper.Facilities.Filtering;
using Techneaux.CygNetWrapper.Points;
using Techneaux.CygNetWrapper.Services;
using Techneaux.CygNetWrapper.Services.FAC;
using Techneaux.CygNetWrapper.Summary;
using TechneauxReportingDataModel.CygNet;

namespace DataMasterClient.ViewModels.CygNet.Reactive
{
    public class CygNetFacilityPointSearchViewModel : ReactiveObject
    {
        public CygNetGeneralOptions CygNetOptsModel { get; }

        public CygNetFacilityPointSearchViewModel(CygNetGeneralOptions cygOpts)
        {
            CygNetOptsModel = cygOpts;
            CygNetOptsModel.PropertyChanged += CygNetOptsModel_PropertyChanged;

            FacilityFilteringSubViewModel = new FacilityFilteringViewModel(CygNetOptsModel.FacilityFilteringRules);

            // Get new domain with cancellation
            var domainChanged =
                this.WhenAnyValue(me => me.CygNetOptsModel.UseCurrentDomain, me => me.CygNetOptsModel.FixedDomainId)
                    .Publish().RefCount();

            var updateDomain = ReactiveCommand
                .CreateFromObservable(() => Observable
                    .StartAsync(ct => CygNetCachedFacilityPointModel
                        .GetDomain(CygNetOptsModel.UseCurrentDomain, CygNetOptsModel.FixedDomainId, ct))
                    .TakeUntil(domainChanged));

            domainChanged.Subscribe(_ => updateDomain.Execute().Subscribe());
            
            //Observable.FromEventPattern<PropertyChangedEventHandler, PropertyChangedEventArgs>
            //        (h => CygNetOptsModel.PropertyChanged += h, h => CygNetOptsModel.PropertyChanged -= h)
            //    .Subscribe(opts => { Console.WriteLine("Fac Opts event"); });

            //Console.WriteLine("First");
            CygNetOptsModel.FacilityFilteringRules = CygNetOptsModel.FacilityFilteringRules;

            
            _fixedDomainLabel = this.WhenAnyValue(me => me.CygNetOptsModel.UseCurrentDomain)
                .Select(useCurrent => useCurrent ? "Current Domain ID" : "Fixed Domain Id")
                .ToProperty(this, x => x.FixedDomainLabel, "Current Domain ID");

            // Update fac service list when domain is updated
            _facilityServices = updateDomain
                .Select(domain => domain.FacilityServices.ToList())
                .ToProperty(this, x => x.FacilityServices, new List<FacilityService>());

            // Update current facility service
            _currentFacilityService = this
                .WhenAnyValue(me => me.FacilityServices, me => me.CygNetOptsModel.FacSiteService)
                .Select(res => res.Item1.FirstOrDefault(serv => serv.SiteService.ToString() == res.Item2))
                .ToProperty(this, x => x.CurrentFacilityService, null);

            // Fac attributes task
            var getFacAttrs =
                ReactiveCommand.CreateFromTask<FacilityService, List<FacilityAttribute>>(serv =>
                    GetFacilityAttributes(serv));

            this.WhenAnyObservable(me => me.Changed)
                .Where(ch => ch.PropertyName == nameof(CurrentFacilityService))
                .Select(me => (me.Sender as CygNetFacilityPointSearchViewModel).CurrentFacilityService)
                .InvokeCommand(getFacAttrs);

            _availableFacilityAttributes = getFacAttrs.ToProperty(this, x => x.AvailableFacilityAttributes,
                new List<FacilityAttribute>());

            this.WhenAnyValue(me => me.AvailableFacilityAttributes)
                .Select(attrs => attrs.ToDictionary(attr => attr.ColumnName, attr => attr))
                .Subscribe(attrs => { UpdateFacilityAttributeDesc(attrs); });

            // Update facility preview
            var facOptsChanged = this
                .WhenAnyValue(me => me.CurrentFacilityService, me => me.SignalRuleChange)
                .Throttle(TimeSpan.FromSeconds(1), RxApp.MainThreadScheduler)
                .Publish().RefCount();

            var updateFacilitySearchResults = ReactiveCommand
                .CreateFromObservable(() => Observable.StartAsync(ct => CygNetCachedFacilityPointModel
                        .GetFacList(CurrentFacilityService, CygNetOptsModel.FacilityFilteringRules, ct))
                    .TakeUntil(facOptsChanged));

            facOptsChanged.Subscribe(_ => updateFacilitySearchResults.Execute().Subscribe());

            _cachedFacilities =
                updateFacilitySearchResults.ToProperty(this, x => x.CachedFacilities, new List<CachedCygNetFacility>());

            updateFacilitySearchResults.CanExecute.Do(x => Console.WriteLine($"can execute => {x}"));

            _isFacilitiesBusy = updateFacilitySearchResults.IsExecuting.Do(x => Console.WriteLine($"signal => {x}"))
                .ToProperty(this, x => x.IsFacilitiesBusy, false);

            // Update domain properties
            _currentDomain = updateDomain.ToProperty(this, x => x.CurrentDomain, null);
            _currentDomainId = updateDomain.Select(dom => dom?.DomainId.ToString() ?? "Unknown")
                .ToProperty(this, x => x.CurrentDomainId, null);

            _searchSummary = this.WhenAnyValue(me => me.CachedFacilities)
                .Throttle(TimeSpan.FromSeconds(.8))
                .Select(FacilityListSummary.GetSummaryResults)
                .ToProperty(this, x => x.SearchSummary, new List<FacilityListSummary.FacilitySummaryResult>());

            // Disable FAC dropdown when trying to get domain
            _isGettingDomain = updateDomain.IsExecuting.Select(stat => !stat)
                .ToProperty(this, x => x.IsGettingDomain, false);
            _isServiceSpinnerVisible = updateDomain.IsExecuting
                .Select(stat => stat ? Visibility.Visible : Visibility.Hidden)
                .ToProperty(this, x => x.IsServiceSpinnerVisible, Visibility.Hidden);

            // -- for testing
            PropertyChanged += CygNetFacilityPointSearch_PropertyChanged;

            // Update service ID list
            _serviceList = this.WhenAnyValue(me => me.FacilityServices)
                .Select(list => list.Select(serv => serv.SiteService.ToString()).ToList())
                .ToProperty(this, x => x.ServiceList, new List<string>());

            // Update domain visibility
            _isFixedDomainNumericVisible = this.WhenAnyValue(me => me.CygNetOptsModel.UseCurrentDomain)
                .Select(ucd => ucd ? Visibility.Collapsed : Visibility.Visible)
                .ToProperty(this, x => x.IsFixedDomainNumericVisible, Visibility.Collapsed);

            // Is current domain displayed
            _isCurrentDomainIdVisible = this.WhenAnyValue(me => me.CygNetOptsModel.UseCurrentDomain)
                .Select(ucd => ucd ? Visibility.Visible : Visibility.Collapsed)
                .ToProperty(this, x => x.IsCurrentDomainIdVisible, Visibility.Collapsed);

            // Set up forms
            FacilityAttributeChooser.StaticDataContext = this;
            PointAttributeChooser.StaticDataContext = this;

            AvailablePointAttributes = PointAttribute.AllPointAttributes.Values.ToList();
            CygNetOptsModel.PropertyChanged += CygNetOptsModel_PropertyChanged;
        }

        private int _signalRuleChange = 0;

        private int SignalRuleChange
        {
            get => _signalRuleChange;
            set => this.RaiseAndSetIfChanged(ref _signalRuleChange, value);
        }

        private void CygNetOptsModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(CygNetOptsModel.FacilityFilteringRules))
            {
                SignalRuleChange += 1;
            }
        }

        private readonly ObservableAsPropertyHelper<List<CachedCygNetFacility>> _cachedFacilities;
        public List<CachedCygNetFacility> CachedFacilities => _cachedFacilities.Value;

        private readonly ObservableAsPropertyHelper<bool> _isFacilitiesBusy;
        public bool IsFacilitiesBusy => _isFacilitiesBusy.Value;

        private readonly ObservableAsPropertyHelper<string> _fixedDomainLabel;
        public string FixedDomainLabel => _fixedDomainLabel.Value;

        private readonly ObservableAsPropertyHelper<List<FacilityListSummary.FacilitySummaryResult>> _searchSummary;
        public List<FacilityListSummary.FacilitySummaryResult> SearchSummary => _searchSummary.Value;

        public string FacSiteService
        {
            get => CygNetOptsModel.FacSiteService;
            set
            {
                CygNetOptsModel.FacSiteService = value;
                this.RaisePropertyChanged();
            }
        }

        private FacilityFilteringViewModel _baseFacilityFilteringSubViewModel;

        public FacilityFilteringViewModel FacilityFilteringSubViewModel
        {
            get => _baseFacilityFilteringSubViewModel;
            set => this.RaiseAndSetIfChanged(ref _baseFacilityFilteringSubViewModel, value);
        }

        private FacilityFilteringViewModel _childFacilityFilteringSubViewModel;

        public FacilityFilteringViewModel ChildFacilityFilteringSubViewModel
        {
            get => _childFacilityFilteringSubViewModel;
            set => this.RaiseAndSetIfChanged(ref _childFacilityFilteringSubViewModel, value);
        }

        private List<PointAttribute> _availablePointAttributes;

        public List<PointAttribute> AvailablePointAttributes
        {
            get => _availablePointAttributes;
            set => this.RaiseAndSetIfChanged(ref _availablePointAttributes, value);
        }

        private readonly ObservableAsPropertyHelper<List<FacilityAttribute>> _availableFacilityAttributes;
        public List<FacilityAttribute> AvailableFacilityAttributes => _availableFacilityAttributes.Value;

        public static async Task<List<FacilityAttribute>> GetFacilityAttributes(FacilityService facServ)
        {
            var availableFacAttrs = new List<FacilityAttribute>();

            if (facServ != null && facServ.IsServiceAvailable)
            {
                if (await Task.Run(() => facServ.RefreshFacServiceAttributesAsync(TimeSpan.FromMinutes(5))))
                {
                    availableFacAttrs = facServ.EnabledAttributes.Select(attr => new FacilityAttribute(attr.Value))
                        .ToList();

                    availableFacAttrs.AddRange(FacilityFmsFilterCondition.FmsFilteringAttrs);
                }
            }

            return availableFacAttrs;
        }

        private void UpdateFacilityAttributeDesc(Dictionary<string, FacilityAttribute> attrs)
        {
            foreach (var condition in CygNetOptsModel.FacilityFilteringRules.BaseRules)
            {
                if (string.IsNullOrWhiteSpace(condition.AttributeId) || attrs == null)
                    continue;

                if (attrs.TryGetValue(condition.AttributeId, out var attrDef))
                {
                    condition.AttributeDescription = attrDef.Description;
                }
                else
                {
                    condition.AttributeDescription = "[Not Found]";
                }
            }
        }

        public bool UseCurrentDomain
        {
            get => CygNetOptsModel.UseCurrentDomain;
            set
            {
                CygNetOptsModel.UseCurrentDomain = value;
                this.RaisePropertyChanged();
            }
        }

        public ushort FixedDomainId
        {
            get => CygNetOptsModel.FixedDomainId;
            set
            {
                CygNetOptsModel.FixedDomainId = value;
                this.RaisePropertyChanged();
            }
        }

        private readonly ObservableAsPropertyHelper<bool> _isGettingDomain;
        public bool IsGettingDomain => _isGettingDomain.Value;

        private readonly ObservableAsPropertyHelper<Visibility> _isServiceSpinnerVisible;
        public Visibility IsServiceSpinnerVisible => _isServiceSpinnerVisible.Value;


        private readonly ObservableAsPropertyHelper<Visibility> _isFixedDomainNumericVisible;
        public Visibility IsFixedDomainNumericVisible => _isFixedDomainNumericVisible.Value;

        private readonly ObservableAsPropertyHelper<Visibility> _isCurrentDomainIdVisible;
        public Visibility IsCurrentDomainIdVisible => _isCurrentDomainIdVisible.Value;

        private readonly ObservableAsPropertyHelper<List<string>> _serviceList;
        public List<string> ServiceList => _serviceList.Value;


        private readonly ObservableAsPropertyHelper<FacilityService> _currentFacilityService;
        public FacilityService CurrentFacilityService => _currentFacilityService?.Value;


        private readonly ObservableAsPropertyHelper<List<FacilityService>> _facilityServices;
        public List<FacilityService> FacilityServices => _facilityServices.Value;


        private void CygNetFacilityPointSearch_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(_currentDomain))
            {
                Console.WriteLine("Done");
            }
        }

        private readonly ObservableAsPropertyHelper<CygNetDomain> _currentDomain = null;
        public CygNetDomain CurrentDomain => _currentDomain.Value;

        private readonly ObservableAsPropertyHelper<string> _currentDomainId;
        public string CurrentDomainId => _currentDomainId.Value;
    }
}