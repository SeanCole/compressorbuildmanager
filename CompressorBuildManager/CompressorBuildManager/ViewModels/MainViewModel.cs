﻿using CompressorBuildManagerWPF.DataModel;
using CompressorBuildManagerWPF.ViewModels;
using XmlDataModelUtility;

namespace CompressorTemplateDesigner.ViewModels
{

    public class MainViewModel : NotifyDynamicBase<CompressorMasterConfig>
    {

        public MainViewModel(CompressorMasterConfig src) : base(src)
        {
            MasterPointVM = new MasterPointViewModel(src.PointConfig);
            CompProgramConfigVM = new CompProgramConfigViewModel(src.CompressorProgramConfig, src.PointConfig.MasterPointList);
        }

        public MasterPointViewModel MasterPointVM
        {
            get => GetPropertyValue<MasterPointViewModel>();
            set => SetPropertyValue(value);
        }

        public CompProgramConfigViewModel CompProgramConfigVM
        {
            get => GetPropertyValue<CompProgramConfigViewModel>();
            set => SetPropertyValue(value);
        }
    }

}
