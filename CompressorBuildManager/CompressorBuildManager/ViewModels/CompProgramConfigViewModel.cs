﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using CompressorBuildManagerWPF.DataModel;
using XmlDataModelUtility;
using CompressorTemplateDataModel;

namespace CompressorBuildManagerWPF.ViewModels
{

    public class CompProgramConfigViewModel : NotifyDynamicBase<CompProgramConfig>
    {
        private BindingList<DTFPoint> MasterList;

        public CompProgramConfigViewModel(CompProgramConfig srcModel, BindingList<DTFPoint> MasterPointList) : base(srcModel)
        {
            MasterList = MasterPointList;
            CompProgramList = new BindingList<CompProgramViewModel>();
            foreach (CompProgram prog in srcModel.CompPrograms)
            {
                CompProgramList.Add(new CompProgramViewModel(prog));
            }
            SaveCompressorButtonVisibility = Visibility.Collapsed;
        }

        public BindingList<CompProgramViewModel> CompProgramList
        {
            get => GetPropertyValue<BindingList<CompProgramViewModel>>();
            set => SetPropertyValue(value);
        }
        public CompProgramViewModel SelectedCompProgram
        {
            get => GetPropertyValue<CompProgramViewModel>();
            set
            {
                if(value != null)
                {
                    MyDataModel.SelectedProgram = value.MyDataModel;
                    SaveCompressorButtonVisibility = Visibility.Visible;
                }
                SetPropertyValue(value);
            }               
        }
        public ICommand AddRow => new DelegateCommand(AddNewRow);
    
        public Visibility SaveCompressorButtonVisibility
        {
            get => GetPropertyValue<Visibility>();
            set => SetPropertyValue(value);
        }

        public void AddNewRow(object inputObject)
        {
            var newProgram = new CompProgram();
            foreach (var dtfPnt in MasterList)
            {
                var tmpCompPnt = new CompPoint();
                tmpCompPnt.DTFPnt = dtfPnt.Clone() as DTFPoint;
                newProgram.CompPoints.Add(tmpCompPnt);
            }
            var newProgramView = new CompProgramViewModel(newProgram);
            if (SelectedCompProgram == null)
            {                
                MyDataModel.CompPrograms.Insert(CompProgramList.Count, newProgram);               
                CompProgramList.Add(newProgramView);
                SelectedCompProgram = CompProgramList[CompProgramList.Count - 1];
                return;
            }
            var index = CompProgramList.IndexOf(SelectedCompProgram);
            if (index < 0)
                index = 0;
            else if (index >= CompProgramList.Count)
                index = CompProgramList.Count - 1;
           
            MyDataModel.CompPrograms.Insert(index, newProgram);
                     
            CompProgramList.Insert(index, newProgramView);

            if (index < 0)
                index = 0;
            else if (index >= CompProgramList.Count)
                index = CompProgramList.Count - 1;

            SelectedCompProgram = CompProgramList[index];
        }
        public ICommand DeleteRow => new DelegateCommand(DeleteSelectedRow);
        public void DeleteSelectedRow(object inputObject)
        {

            var index = CompProgramList.IndexOf(SelectedCompProgram);
            CompProgramList.Remove(SelectedCompProgram);
            MyDataModel.DeletePoint(index);
            if (CompProgramList.FirstOrDefault() == null)
                return;

            if (index < 0)
                index = 0;
            else if (index >= CompProgramList.Count)
                index = CompProgramList.Count - 1;

            SelectedCompProgram = CompProgramList[index];

            return;

        }
    }
}
