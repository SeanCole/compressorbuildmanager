﻿using CompressorBuildManagerWPF.DataModel;
using CompressorBuildManagerWPF.Forms;
using CompressorTemplateDataModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using XmlDataModelUtility;

namespace CompressorBuildManagerWPF.ViewModels
{
    public class CompProgramViewModel : NotifyDynamicBase<CompProgram>
    {
        public CompProgramViewModel(CompProgram srcModel) : base(srcModel)
        {
            CompPointList = new BindingList<CompPointViewModel>();
            for (var i = 0; i < srcModel.CompPoints.Count; i++)
            {
                var model = new CompPointViewModel(MyDataModel.CompPoints[i]);
                CompPointList.Add(model);
            }        
        }
        public BindingList<CompPointViewModel> CompPointList
        {
            get => GetPropertyValue<BindingList<CompPointViewModel>>();
            set => SetPropertyValue(value);
        }        

    }
}
