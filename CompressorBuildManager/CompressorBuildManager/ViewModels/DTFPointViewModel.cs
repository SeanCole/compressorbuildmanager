﻿using CompressorBuildManagerWPF.DataModel;
using CompressorTemplateDataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GenericRuleModel.Helper;
using XmlDataModelUtility;

namespace CompressorBuildManagerWPF.ViewModels
{
    public class DTFPointViewModel : NotifyDynamicBase<DTFPoint>
    {
        public DTFPointViewModel(DTFPoint srcModel): base(srcModel)
        {

        }
        public ValueDescription DataTypeValue
        {
            get => new ValueDescription {Value = MyDataModel.DataType, Description = MyDataModel.DataType.ToString()};
            set => MyDataModel.DataType =(DTFPoint.ModBusDataTypes)value.Value;
        }
    }
}
