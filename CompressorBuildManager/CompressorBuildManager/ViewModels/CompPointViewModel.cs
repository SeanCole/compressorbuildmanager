﻿using CompressorBuildManagerWPF.DataModel;
using CompressorBuildManagerWPF.Forms;
using CompressorTemplateDataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using GenericRuleModel.Helper;
using XmlDataModelUtility;

namespace CompressorBuildManagerWPF.ViewModels
{
    public class CompPointViewModel : NotifyDynamicBase<CompPoint>
    {
        public CompPointViewModel(CompPoint srcModel) : base(srcModel)
        {

        }

        public ICommand OpenEnumTableWindow => new DelegateCommand(OpenSelectEnumTableWindow);

        public void OpenSelectEnumTableWindow(object inputObject)
        {
            var windowInstance = EnumTableChooser.Instance();
            var success = windowInstance.TryGetKeyDescId(out var result);
            if (!success) return;
            if (result.Value == null) return;
            MyDataModel.StatusEnumTableName = result.Value as string;
            MyDataModel.StatusEnumTableDescription = result.Description as string;
        }

        public ValueDescription DataTypeValue
        {
            get => new ValueDescription {Value = MyDataModel.DTFPnt.DataType, Description = MyDataModel.DTFPnt.DataType.ToString()};
            set => MyDataModel.DTFPnt.DataType = (DTFPoint.ModBusDataTypes?) value?.Value ?? MyDataModel.DTFPnt.DataType;
        }
    }
}
