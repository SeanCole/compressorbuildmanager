using GenericRuleModel.Rules;
using XmlDataModelUtility;

namespace CompressorBuildManagerWPF.ViewModels
{
    public class EnumerationTablePairViewModel : NotifyDynamicBase<EnumerationTable.EnumerationPair>
    {
        public EnumerationTablePairViewModel(EnumerationTable.EnumerationPair srcModel) : base(srcModel)
        {
        }  
    }
}

