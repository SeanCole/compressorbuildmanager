﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml.Serialization;
using CompressorBuildManagerWPF.DataModel;
using CompressorBuildManagerWPF.ViewModels;
using CompressorTemplateDataModel;
using CompressorTemplateDesigner.ViewModels;
using MahApps.Metro.Controls;
using Microsoft.Win32;
using Path = System.IO.Path;


namespace CompressorBuildManagerWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        readonly ConfigModelFileService _configFile;

        public MainWindow()
        {
            InitializeComponent();

            //MessageBox.Show(Assembly.GetExecutingAssembly().GetName().Version.ToString());

           // Title = $"Compressor Build Manager (Version {Assembly.GetExecutingAssembly().GetName().Version})";

            try
            {
                //   TechneauxHistorySynchronization.LibraryLogging.InitLogging("SQL Sync UI", Serilog.Events.LogEventLevel.Debug);

                // var fileName = $"client_Log_" + "{Date}.txt";


                //Log.Logger = new LoggerConfiguration()
                //    .MinimumLevel.Debug()
                //    .WriteTo
                //    .RollingFile($@"{AppDomain.CurrentDomain.BaseDirectory}Logs\{fileName}", shared: true)
                //    .CreateLogger();
                //Log.Debug("Launching client.");

                //MessageBox.Show(AppDomain.CurrentDomain.BaseDirectory);

                //var commandLineParams = Environment.GetCommandLineArgs().ToList();
                _configFile = new ConfigModelFileService(this);
                _configFile.ResetModified();

                //if (commandLineParams.Count > 0 && commandLineParams.Exists(item => item.ToLower() == @"/autorun"))
                //{
                //    CancellationTokenSource ctSource = new CancellationTokenSource();
                //    AutoOperation.RunAutoOperationAsync(DateTime.Today, Environment.GetCommandLineArgs().ToList(), _configFile, ctSource.Token);

                //    Close();
                //}
                
                FileOpsPanel.DataContext = _configFile;

                var myViewModel = new MainViewModel(_configFile.ThisConfig);
                _configFile.ResetModified();
                DataContext = myViewModel;


                Closing += MainWindow_Closing;
                BtnNewFile.Click += Button_New;
                BtnOpenFile.Click += Button_Open;
                BtnSaveFile.Click += Button_Save;
                BtnSaveFileAsNew.Click += Button_SaveAsNew;

                Loaded += MainWindow_Loaded;
            }
            catch (Exception e)
            {
               // Log.Error(e, "Fatal error");
                MessageBox.Show(e.ToString());
            }
        }

        private void PasteButton_Click(object sender, RoutedEventArgs e)
        {
            int DefaultUDCIndex, RegisterIndex, DescIndex, groupIndex, DataTypeIndex;
            string DefaultUDC = "";
            string Register = "";
            string Desc = "";
            string group = "";
            string DataType = "";
            try
            {
                var vm = DataContext as MainViewModel;

                if (Clipboard.ContainsText(TextDataFormat.Text))
                {
                    string clipboardText = Clipboard.GetText(TextDataFormat.Text);
                    var rows = clipboardText.Split(new[] { Environment.NewLine }, StringSplitOptions.None).ToList();
                    if (rows.Count - 1 > 0)
                    {
                        var columns = rows.FirstOrDefault().Split(new[] { "\t" }, StringSplitOptions.None).ToList();
                        DefaultUDCIndex = columns.Select(x => x.Trim().ToLower()).ToList().IndexOf("defaultudc");
                        RegisterIndex = columns.Select(x => x.Trim().ToLower()).ToList().IndexOf("register");
                        DescIndex = columns.Select(x => x.Trim().ToLower()).ToList().IndexOf("description");                            
                        groupIndex = columns.Select(x => x.Trim().ToLower()).ToList().IndexOf("group");
                        DataTypeIndex = columns.Select(x => x.Trim().ToLower()).ToList().IndexOf("datatype");
                        
                        foreach (var row in rows)
                        {
                            if (row.Contains("\t") && row != rows.FirstOrDefault())
                            {
                                var properties = row.Split(new[] { "\t" }, StringSplitOptions.None).ToList();
                                if (DefaultUDCIndex != -1)
                                { 
                                    DefaultUDC = properties[DefaultUDCIndex];
                                }
                                if (RegisterIndex != -1)
                                {
                                    
                                    Register = properties[RegisterIndex];
                                }
                                if (DescIndex != -1)
                                {
                                    
                                    Desc = properties[DescIndex];
                                }
                                if (groupIndex != -1)
                                {
                                    
                                    group = properties[groupIndex];
                                }
                                if (DataTypeIndex != -1)
                                {
                                    
                                   // DataType = properties[DataTypeIndex];
                                }
                                
                                //vm.MasterPointVM.AddNewRow(DefaultUDC, Register, Desc, group, DataType);
                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Failed to paste clipboard text with exception: {ex.Message}");

            }
        }

        private async void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            var vm = DataContext as MainViewModel;

            //var chart = new TestCharts();
            //var chartVm = new TestVm();
            //chart.DataContext = chartVm;
            //chart.ShowDialog();
            try
            {
              
            }
            catch (Exception ex)
            {
               // Log.Error(ex, "Fatal error");
                MessageBox.Show(ex.ToString());
            }
        }

        private void MetroDataGrid_TargetUpdated(object sender, DataTransferEventArgs e)
        {
            for (var i = 0; i < MetroDataGrid.Columns.Count; i++)
            {
                MetroDataGrid.Columns[i].Width = 0;
            }
            MetroDataGrid.UpdateLayout();
            for (var j = 0; j < MetroDataGrid.Columns.Count; j++)
            {
                MetroDataGrid.Columns[j].Width = new DataGridLength(1, DataGridLengthUnitType.Auto);
            }
        }
        private void MainWindow_Closing(object sender, CancelEventArgs e)
        {
            if (!_configFile.TryClose())
            {
                e.Cancel = true;
            }
        }

        private async void Button_New(object sender, RoutedEventArgs e)
        {
            if (_configFile.CreateNewConfig())
            {
                var myViewModel = new MainViewModel(_configFile.ThisConfig);
                
                DataContext = null;
                DataContext = myViewModel;
                _configFile.ResetModified();
            }
        }

        private async void Button_Open(object sender, RoutedEventArgs e)
        {
            if (_configFile.OpenConfigFile())
            {
                try
                {
                    var myViewModel = new MainViewModel(_configFile.ThisConfig);

                    DataContext = null;

                    MainTabControl.DataContext = myViewModel;
                    _configFile.ResetModified();
                    
                }
                catch (Exception ex)
                {
                    //Log.Error(ex, "Fatal error");
                    MessageBox.Show(ex.ToString());
                }

            }
        }

        private void Button_Save(object sender, RoutedEventArgs e)
        {
            _configFile.SaveConfigFile();
        }

        private void Button_SaveAsNew(object sender, RoutedEventArgs e)
        {
            _configFile.SaveConfigFile(true);
        }
    }
}
