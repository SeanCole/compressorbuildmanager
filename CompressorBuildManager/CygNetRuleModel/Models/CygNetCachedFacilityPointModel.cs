﻿using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Techneaux.CygNetWrapper.Facilities;
using Techneaux.CygNetWrapper.Facilities.Filtering;
using Techneaux.CygNetWrapper.Services;
using Techneaux.CygNetWrapper.Services.FAC;

namespace CygNetRuleModel.Models
{
    public static class CygNetCachedFacilityPointModel
    {
        public static async Task<CygNetDomain> GetDomain(bool useCurrentDomain, ushort fixedDomainId, CancellationToken ct)
        { 
            CygNetDomain thisDomain;
            try
            {
                if (useCurrentDomain)
                {
                    thisDomain = await Task.Run(() => CygNetDomain.GetCurrentDomainAsync());
                }
                else
                {
                    thisDomain = CygNetDomain.GetDomainAsync(fixedDomainId);
                }
                await Task.Run(() => thisDomain.RefreshSiteServiceInfoAsync(ct));

                return thisDomain;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "General exception getting domain");
            }
            return null;
        }

        public static async Task<List<CachedCygNetFacility>> GetFacList(FacilityService facService, FacilityFilteringOptions filterOpts, CancellationToken ct)
        {
            var tempOpts = filterOpts.Clone();
            // Make a copy of the config
            var facFilterOpts = filterOpts.Clone() as FacilityFilteringOptions;

            var cachedFacs = new List<CachedCygNetFacility>();
            try
            {
                if (facService != null && facFilterOpts != null && facFilterOpts.BaseRules.Any(filter => !string.IsNullOrWhiteSpace(filter.AttributeId)))
                {
                    ct.ThrowIfCancellationRequested();
                    cachedFacs = await Task.Run(() => facService.GetCachedFacilitiesWithChildrenAndParents(facFilterOpts.BaseRules, facFilterOpts.ChildReferenceAttrKey.Id, ct));

                    return cachedFacs;
                }
            }
            catch (OperationCanceledException)
            {
                Log.Debug($"{nameof(GetFacList)} Cancelled");
            }
            catch (Exception ex)
            {
                Log.Error(ex, "General exception getting facility list");
            }

            return cachedFacs;
        }
    }
}
