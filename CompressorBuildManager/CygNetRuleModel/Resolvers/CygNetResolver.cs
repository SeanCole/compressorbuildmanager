﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using CygNet.Data.Core;
using Serilog;
using Techneaux.CygNetWrapper;
using Techneaux.CygNetWrapper.Facilities;
using Techneaux.CygNetWrapper.Facilities.Filtering;
using Techneaux.CygNetWrapper.Points;
using Techneaux.CygNetWrapper.Services.VHS;
using TechneauxReportingDataModel.CygNet.FacilityPointOptions;
using TechneauxReportingDataModel.CygNet.FmsSubOptions;
using TechneauxReportingDataModel.CygNet.Rules;
using TechneauxReportingDataModel.CygNet.Utility;
using TechneauxUtility;

namespace CygNetRuleModel.Resolvers
{
    public static partial class CygNetResolver
    {
        // Static methods


        // Variables
        //private CachedCygNetFacility BaseFacility { get; set; }
        //private CygNetRollupRuleBase Rule { get; set; }
        //private Dictionary<string, ChildFacilityGroup> ChildGroups { get; set; }

        //// Constructors
        //public CygNetResolver(
        //    ICachedCygNetFacility cachedFacilityRef,
        //    CygNetRollupRuleBase rule,
        //    IEnumerable<ChildFacilityGroup> childGroupsRef)
        //{
        //    BaseFacility = cachedFacilityRef as CachedCygNetFacility;
        //    ChildGroups = new Dictionary<string, ChildFacilityGroup>();
        //    Rule = rule;

        //    foreach (var grp in childGroupsRef)
        //    {
        //        ChildGroups[grp.GroupKey.Id] = grp;
        //    }
        //}

        //private ICachedCygNetFacility KnownFac;
        //private ICachedCygNetPoint KnownPoint;

        //public CygNetResolver(
        //    ICachedCygNetPoint srcPoint,
        //    CygNetRuleBase rule)
        //{
        //    KnownFac = srcPoint.Facility as CachedCygNetFacility;
        //    KnownPoint = srcPoint;
        //    Rule = rule;
        //}


        //public CygNetResolver(
        //    ICachedCygNetFacility cachedFacilityRef,
        //    ICygNetPointReference pointRule,
        //    CygNetRuleBase shortElmRule,
        //    IEnumerable<ChildFacilityGroup> childGroupsRef)
        //{
        //    CygNetRollupRuleBase CombinedElmRule = new CygNetRollupRuleBase()
        //    {
        //        FacilityPointOptions = new FacilityPointSelection()
        //        {
        //            FacilityChoice = pointRule.FacilityChoice,
        //            ChildOrdinal = pointRule.ChildOrdinal,
        //            UDC = pointRule.UDC,
        //            ChildGroupKey = pointRule.ChildGroupKey
        //        },

        //        DataElement = shortElmRule.DataElement,

        //        AttributeOptions = new FacilityPointAttributeOptions()
        //        {
        //            AttributeKey = shortElmRule.AttributeOptions.AttributeKey,
        //            IdDescOrValue = shortElmRule.AttributeOptions.IdDescOrValue
        //        },

        //        DefaultIfError = shortElmRule.DefaultIfError
        //    };

        //    BaseFacility = cachedFacilityRef as CachedCygNetFacility; ;
        //    ChildGroups = new Dictionary<string, ChildFacilityGroup>();
        //    Rule = CombinedElmRule;

        //    foreach (var grp in childGroupsRef)
        //    {
        //        ChildGroups[grp.GroupKey.Id] = grp;
        //    }
        //}

        // Enabled Editable Attribute Rules - CygNet Element
        //public async Task<CygNetConvertableValue> ResolveAttributeValueAsync(
        //    CancellationToken ct)
        //{
        //    try
        //    {
        //        if (KnownFac != null && KnownPoint != null)
        //        {
        //            return await ResolveCygnetElmRuleWithKnownPoint(ct);
        //        }
        //        else
        //        {
        //            return await ResolveCygnetElmRule(ct);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return new CygNetConvertableValue(CygNetConvertableValue.ErrorType.UnknownErr, "Unknown exception thrown[13]: " + ex.Message);
        //    }
        //}

        //public static async Task<CygNetConvertableValue> ResolveAttributeValueAsync(CygNetHistoryEntry histEntry, CancellationToken ct)
        //{
        //    try
        //    {
        //        if (KnownFac != null && KnownPoint != null)
        //        {
        //            return ResolveCygnetElmRuleKnownPoint(histEntry, ct);
        //        }
        //        else
        //        {
        //            return await ResolveCygnetHistoryElmRule(histEntry, ct);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return new CygNetConvertableValue(CygNetConvertableValue.ErrorType.UnknownErr, "Unknown exception thrown[13]: " + ex.Message);
        //    }
        //}

        public static async Task<CygNetConvertableValue> ResolveRule(
            CygNetRuleBase rule,
            ICachedCygNetPoint knownPoint,
            CancellationToken ct)
        {
            try
            {

                CygNetConvertableValue errorResult;

                var knownFac = knownPoint.Facility as CachedCygNetFacility;

                // Branch here based on fac or point attribute
                switch (rule.DataElement)
                {
                    case CygNetRuleBase.DataElements.PointAttribute:
                        var pointAttrValResults = await GetPointAttributeValue(rule.AttributeOptions, knownPoint, ct);
                        return pointAttrValResults;

                    case CygNetRuleBase.DataElements.PointCurrentValue:

                        var pointCurrentValResults = await GetPointCurrentValue(rule.PointHistoryOptions, knownPoint, ct);
                        return pointCurrentValResults;

                    case CygNetRuleBase.DataElements.FacilityAttribute:
                    default:
                        var (value, error) = GetFacAttributeValue(knownFac as CachedCygNetFacility,
                            rule.AttributeOptions.AttributeKey.Id, rule.AttributeOptions.IdDescOrValue);

                        if (error == ResolverErrorConditions.None)
                        {
                            return value;
                        }
                        else
                        {
                            errorResult = new CygNetConvertableValue(
                                CygNetConvertableValue.ErrorType.AttrError,
                                error.GetErrorDescription());

                            return CheckErrorReplacement(rule.DefaultIfError, errorResult);
                        }
                }
            }
            catch (Exception ex)
            {
                Log.Debug(ex, "General error");
                return new CygNetConvertableValue(CygNetConvertableValue.ErrorType.UnknownErr, "Uknown exception thrown[5C]: " + ex.Message);
            }           
        }

        public static async Task<CygNetConvertableValue> ResolveRule(
            ICachedCygNetFacility baseFacility,
            IEnumerable<ChildFacilityGroup> childGroups,
            CygNetRollupRuleBase rule,
            RollupPeriod rollupPer,
            CancellationToken ct)
        {
            try
            {
                // Check if base facility is null
                if (baseFacility == null)
                {
                    return new CygNetConvertableValue(CygNetConvertableValue.ErrorType.FacError, "Base facility is null");
                }

                // Begin by trying to get the chosen facility object
                var (facility, error) = ResolveChildFacility(rule.FacilityPointOptions, baseFacility, childGroups);
                var srcFac = facility;

                CygNetConvertableValue errorResult;
                if (error != ResolverErrorConditions.None)
                {
                    errorResult = new CygNetConvertableValue(
                        CygNetConvertableValue.ErrorType.FacError,
                        error.GetErrorDescription());

                    return CheckErrorReplacement(rule.DefaultIfError, errorResult);
                }

                // Branch here based on fac or point attribute
                switch (rule.DataElement)
                {
                    case CygNetRuleBase.DataElements.FmsProperty:
                        var fmsMeter = await srcFac.TryGetLinkedFmsMeter();

                        switch (rule.FmsPropertyOptions.FmsDataElm)
                        {
                            case FmsOptions.FmsDataElms.NodeName:
                                if (fmsMeter == null)
                                {
                                    errorResult = new CygNetConvertableValue(
                                        CygNetConvertableValue.ErrorType.FmsError,
                                        ResolverErrorConditions.NoLinkedFmsMeter.GetErrorDescription()
                                    );
                                }

                                return new CygNetConvertableValue(string.Join(",", fmsMeter.AssociatedNodeNames));

                            case FmsOptions.FmsDataElms.IsFmsMeter:
                                return new CygNetConvertableValue(fmsMeter != null);

                            default:
                                throw new ArgumentOutOfRangeException();
                        }

                    case CygNetRuleBase.DataElements.PointAttribute:
                        var (thisPoint, pntAttrError) = await GetPoint(srcFac, rule.FacilityPointOptions.UDC, ct);

                        if (pntAttrError == ResolverErrorConditions.None)
                        {
                            return await GetPointAttributeValue(rule.AttributeOptions, thisPoint, ct);
                        }
                        else
                        {
                            errorResult = new CygNetConvertableValue(
                                    CygNetConvertableValue.ErrorType.PointErr,
                                    pntAttrError.GetErrorDescription());

                            return CheckErrorReplacement(rule.DefaultIfError, errorResult);
                        }

                    case CygNetRuleBase.DataElements.PointCurrentValue:
                        var srcPointCurrentValueResults = await GetPoint(srcFac, rule.FacilityPointOptions.UDC, ct);
                        if (srcPointCurrentValueResults.Error == ResolverErrorConditions.None)
                        {
                            return await GetPointCurrentValue(rule.PointHistoryOptions, srcPointCurrentValueResults.thisPoint, ct);
                        }
                        else
                        {
                            errorResult = new CygNetConvertableValue(CygNetConvertableValue.ErrorType.PointErr);
                            return CheckErrorReplacement(rule.DefaultIfError, errorResult);
                        }

                    case CygNetRuleBase.DataElements.PointHistory:
                        return new CygNetConvertableValue("[Feature Not Sorted]");

                    case CygNetRuleBase.DataElements.FacilityAttribute:
                        var (value, facAttrError) = GetFacAttributeValue(srcFac, rule.AttributeOptions.AttributeKey.Id, rule.AttributeOptions.IdDescOrValue);

                        if (facAttrError == ResolverErrorConditions.None)
                        {
                            return value;
                        }
                        else
                        {
                            errorResult = new CygNetConvertableValue(
                                CygNetConvertableValue.ErrorType.AttrError,
                                facAttrError.GetErrorDescription());

                            return CheckErrorReplacement(rule.DefaultIfError, errorResult);
                        }

                    //case CygNetRuleBase.DataElements.FixedText:
                    //    return new CygNetConvertableValue(rule.FixedText);

                    default:
                        throw new ArgumentOutOfRangeException("Rule type not supported");
                }
            }
            catch (Exception ex)
            {
                Log.Debug(ex, "General error");
                return new CygNetConvertableValue(CygNetConvertableValue.ErrorType.UnknownErr, "Uknown exception thrown[5C]: " + ex.Message);
            }

        }

        //function just resolves point history
        //public static async Task<CygNetConvertableValue> ResolveCygnetHistoryElmRule(
        //    ICachedCygNetFacility srcBaseFacility,
        //    FacilityFilteringOptions facFilters,
        //    FacilityPointSelection facPointRule,
        //    CygNetRuleBase Rule,
        //    CygNetHistoryEntry histEntry, 
        //    CancellationToken ct)
        //{
        //    // Check if base facility is null
        //    if (srcBaseFacility == null)
        //    {
        //        return new CygNetConvertableValue(CygNetConvertableValue.ErrorType.FacError, "Base facility is null");
        //    }

        //    // Begin by trying to get the chosen facility object
        //    var SrcFacResults = CygNetResolver.ResolveChildFacility(facPointRule, srcBaseFacility, facFilters.ChildGroups);
        //    var SrcFac = SrcFacResults.Facility;

        //    CygNetConvertableValue ErrorResult;
        //    if (SrcFacResults.Error != ResolverErrorConditions.None)
        //    {
        //        ErrorResult = new CygNetConvertableValue(
        //            CygNetConvertableValue.ErrorType.FacError,
        //            SrcFacResults.Error.GetErrorDescription());

        //        return CheckErrorReplacement(Rule.DefaultIfError, ErrorResult);
        //    }

        //    switch (Rule.DataElement)
        //    {
        //        case DataElements.PointHistory:
        //            var srcPointValueResults = await GetPoint(SrcFac, Rule.FacilityPointOptions.UDC, ct);
        //            if (srcPointValueResults.Error == ResolverErrorConditions.None)
        //            {
        //                var PointValResults = GetPointValue(srcPointValueResults.thisPoint, histEntry, ct);
        //                if (PointValResults.Error == ResolverErrorConditions.None)
        //                {
        //                    return PointValResults.Value;
        //                }
        //                else
        //                {
        //                    ErrorResult = new CygNetConvertableValue(CygNetConvertableValue.ErrorType.PointErr, srcPointValueResults.Error.GetErrorDescription());
        //                    return CheckErrorReplacement(Rule.DefaultIfError, ErrorResult);
        //                }
        //            }
        //            else
        //            {
        //                ErrorResult = new CygNetConvertableValue(CygNetConvertableValue.ErrorType.PointErr);
        //                return CheckErrorReplacement(Rule.DefaultIfError, ErrorResult);
        //            }

        //        case DataElements.FacilityAttribute:
        //        default:
        //            ErrorResult = new CygNetConvertableValue(
        //                CygNetConvertableValue.ErrorType.UnknownErr, "Not Point History.");

        //            return CheckErrorReplacement(Rule.DefaultIfError, ErrorResult);

        //    }

        //    // throw new NotImplementedException();
        //}

        private static CygNetConvertableValue CheckErrorReplacement(string defaultIfError,
            CygNetConvertableValue errorResult)
        {
            if (string.IsNullOrWhiteSpace(defaultIfError))
            {
                return errorResult;
            }
            else
            {
                return new CygNetConvertableValue(defaultIfError);
            }
        }

        public static (CygNetConvertableValue Value, ResolverErrorConditions Error) GetFacAttributeValue(
            CachedCygNetFacility thisFac,
            string facAttrId,
            FacilityPointAttributeOptions.AttributeMetaType valType)
        {
            // Check if attribute is valid
            var attrIdIsValid = thisFac.FacService.IsValidAttributeName(facAttrId);

            if (!attrIdIsValid)
            {
                return (null, ResolverErrorConditions.InvalidFacAttribute);
            }

            // Try to retrieve fac attr value
            if (thisFac.TryGetAttributeValue(facAttrId, out var thisVal))
            {
                var finalValue = GetAttrMetaValue(thisVal, valType);

                return (finalValue, ResolverErrorConditions.None);
            }
            else
            {
                return (null, ResolverErrorConditions.FacAttrValueRetrievalError);
            }
        }

        private static async Task<(CachedCygNetPoint thisPoint, ResolverErrorConditions Error)> GetPoint(
            CachedCygNetFacility thisFac,
            string udc,
            CancellationToken ct)
        {
            // Try to retrieve point from Facility (it will be null if failed)
            var thisPoint = await thisFac.GetPoint(udc, ct);

            //TODO Use this to get the point
            if (thisPoint == null)
            {
                return (null, ResolverErrorConditions.UdcNotFound);
            }
            else
            {
                return (thisPoint as CachedCygNetPoint, ResolverErrorConditions.None);
            }
        }

        public static async Task<CygNetConvertableValue> GetPointAttributeValue(
            FacilityPointAttributeOptions attributeOptions,
            ICachedCygNetPoint thisPoint,
            CancellationToken ct)
        {
            // Try to get attribute value
            var thisValue = await thisPoint.GetAttributeValueAsync(attributeOptions.AttributeKey.Id, ct, true);

            if (thisValue == null)
            {
                return new CygNetConvertableValue(CygNetConvertableValue.ErrorType.PointErr,
                    ResolverErrorConditions.PointAttrValueRetrievalError.ToString());
            }
            else
            {
                var finalValue = GetAttrMetaValue(thisValue, attributeOptions.IdDescOrValue);
                return finalValue;
            }
        }

        public static async Task<CygNetConvertableValue> GetPointCurrentValue(
            PointHistoryGeneralOptions pointHistoryOptions,
            ICachedCygNetPoint thisPoint,
            CancellationToken ct)
        {
            // Try to get attribute value

            var tag = thisPoint.Tag;

            try
            {
                var thisValue = await Task.Run(() => thisPoint.CvsService.GetPointValue(tag));

                if (thisValue == null)
                {
                    return new CygNetConvertableValue(CygNetConvertableValue.ErrorType.PointErr,
                        ResolverErrorConditions.PointCurrentValueRetrievalError.ToString());
                }
                else
                {
                    var finalValue = ResolvePointCurrentValue(thisValue, pointHistoryOptions);
                    return finalValue;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Error in CygNet resolver while trying to read current value");

                return new CygNetConvertableValue(CygNetConvertableValue.ErrorType.PointErr,
                        ResolverErrorConditions.PointCurrentValueRetrievalError.ToString());
            }

        }

        public static CygNetConvertableValue GetPointHistoryValue(
            ICachedCygNetPoint thisPoint,
            PointHistoryGeneralOptions pointHistoryOptions,
            CygNetHistoryEntry histEntry)
        {
            CygNetConvertableValue finalValue;

            if (Enum.TryParse(pointHistoryOptions.PointValueType.ToString(), out BaseStatusFlags thisBaseEnum))
            {
                finalValue = new CygNetConvertableValue(histEntry.BaseStatus.HasFlag(thisBaseEnum));
                return finalValue;
            }

            if (Enum.TryParse(pointHistoryOptions.PointValueType.ToString(), out UserStatusFlags thisUserEnum))
            {
                finalValue = new CygNetConvertableValue(histEntry.UserStatus.HasFlag(thisUserEnum));
                return finalValue;
            }

            //CygNetConvertableValue FinalValue = ResolvePointCurrentValue(ThisValue, Rule.PointHistoryOptions);
            switch (pointHistoryOptions.PointValueType)
            {
                case PointHistoryGeneralOptions.PointValueTypesNew.Value:
                    if (histEntry.Value.StringValue == "")
                    {
                        if (pointHistoryOptions.BlankValueHandlingType ==
                            PointHistoryGeneralOptions.BlankValueHandlingTypes.ExcludeBlanks)
                        {
                            finalValue = new CygNetConvertableValue(CygNetConvertableValue.ErrorType.BlankErr,
                                "Value is blank");
                        }
                        else if (pointHistoryOptions.BlankValueHandlingType ==
                                 PointHistoryGeneralOptions.BlankValueHandlingTypes.ReplaceWithDefault)
                        {
                            finalValue = new CygNetConvertableValue(pointHistoryOptions.DefaultValueIfBlank);
                        }
                        else
                        {
                            finalValue = new CygNetConvertableValue(histEntry.Value.RawValue);
                        }
                    }
                    else
                    {
                        finalValue = new CygNetConvertableValue(histEntry.Value.RawValue);
                    }

                    break;
                case PointHistoryGeneralOptions.PointValueTypesNew.AltValue:
                    finalValue = new CygNetConvertableValue(CygNetConvertableValue.ErrorType.PointErr, "No Alt Value");
                    break;

                case PointHistoryGeneralOptions.PointValueTypesNew.Timestamp:
                    if (histEntry.AdjustedTimestamp.ToString() == "")
                    {
                        if (pointHistoryOptions.BlankValueHandlingType ==
                            PointHistoryGeneralOptions.BlankValueHandlingTypes.ExcludeBlanks)
                        {
                            finalValue = new CygNetConvertableValue(CygNetConvertableValue.ErrorType.BlankErr,
                                "Timestamp is blank");
                        }
                        else if (pointHistoryOptions.BlankValueHandlingType ==
                                 PointHistoryGeneralOptions.BlankValueHandlingTypes.ReplaceWithDefault)
                        {
                            finalValue = new CygNetConvertableValue(pointHistoryOptions.DefaultValueIfBlank);
                        }
                        else
                        {
                            finalValue = new CygNetConvertableValue(histEntry.AdjustedTimestamp);
                        }
                    }
                    else
                    {
                        finalValue = new CygNetConvertableValue(histEntry.AdjustedTimestamp);
                    }

                    break;

                default:
                    finalValue = new CygNetConvertableValue(histEntry.Value);
                    break;
            }

            return finalValue;
        }

        private static CygNetConvertableValue ResolvePointCurrentValue(
            PointValueRecord thisValue,
            PointHistoryGeneralOptions pointHistoryOptions)
        {
            if (Enum.TryParse(pointHistoryOptions.PointValueType.ToString(), out BaseStatusFlags thisBaseEnum))
            {
                return new CygNetConvertableValue(thisValue.Status.HasFlag(thisBaseEnum));
            }

            if (Enum.TryParse(pointHistoryOptions.PointValueType.ToString(), out UserStatusFlags thisUserEnum))
            {
                return new CygNetConvertableValue(thisValue.Status.HasFlag(thisUserEnum));
            }

            switch (pointHistoryOptions.PointValueType)
            {
                case PointHistoryGeneralOptions.PointValueTypesNew.Value:
                    if (thisValue.Value.ToString() == "")
                    {
                        if (pointHistoryOptions.BlankValueHandlingType ==
                            PointHistoryGeneralOptions.BlankValueHandlingTypes.ExcludeBlanks)
                        {
                            return new CygNetConvertableValue(CygNetConvertableValue.ErrorType.BlankErr,
                                "Value is blank");
                        }
                        else if (pointHistoryOptions.BlankValueHandlingType ==
                                 PointHistoryGeneralOptions.BlankValueHandlingTypes.ReplaceWithDefault)
                        {
                            return new CygNetConvertableValue(pointHistoryOptions.DefaultValueIfBlank);
                        }
                        else
                        {
                            return new CygNetConvertableValue(thisValue.Value);
                        }
                    }
                    else
                    {
                        return new CygNetConvertableValue(thisValue.Value);
                    }
                case PointHistoryGeneralOptions.PointValueTypesNew.AltValue:
                    if (thisValue.AlternateValue.ToString() == "")
                    {
                        if (pointHistoryOptions.BlankValueHandlingType ==
                            PointHistoryGeneralOptions.BlankValueHandlingTypes.ExcludeBlanks)
                        {
                            return new CygNetConvertableValue(CygNetConvertableValue.ErrorType.BlankErr,
                                "Alt Value is blank");
                        }
                        else if (pointHistoryOptions.BlankValueHandlingType ==
                                 PointHistoryGeneralOptions.BlankValueHandlingTypes.ReplaceWithDefault)
                        {
                            return new CygNetConvertableValue(pointHistoryOptions.DefaultValueIfBlank);
                        }
                        else
                        {
                            return new CygNetConvertableValue(thisValue.AlternateValue);
                        }
                    }
                    else
                    {
                        return new CygNetConvertableValue(thisValue.AlternateValue);
                    }

                case PointHistoryGeneralOptions.PointValueTypesNew.Timestamp:
                    if (thisValue.Timestamp.ToString() == "")
                    {
                        if (pointHistoryOptions.BlankValueHandlingType ==
                            PointHistoryGeneralOptions.BlankValueHandlingTypes.ExcludeBlanks)
                        {
                            return new CygNetConvertableValue(CygNetConvertableValue.ErrorType.BlankErr,
                                "Timestamp is blank");
                        }
                        else if (pointHistoryOptions.BlankValueHandlingType ==
                                 PointHistoryGeneralOptions.BlankValueHandlingTypes.ReplaceWithDefault)
                        {
                            return new CygNetConvertableValue(pointHistoryOptions.DefaultValueIfBlank);
                        }
                        else
                        {
                            return new CygNetConvertableValue(thisValue.Timestamp);
                        }
                    }
                    else
                    {
                        return new CygNetConvertableValue(thisValue.Timestamp);
                    }

                default:
                    return new CygNetConvertableValue(thisValue.Value);
            }
        }

        private static CygNetConvertableValue GetAttrMetaValue(IAttributeValue attrVal,
            FacilityPointAttributeOptions.AttributeMetaType metaType)
        {
            switch (metaType)
            {
                case FacilityPointAttributeOptions.AttributeMetaType.ID:
                    return new CygNetConvertableValue(attrVal.AttributeId);
                case FacilityPointAttributeOptions.AttributeMetaType.Description:
                    return new CygNetConvertableValue(attrVal.AttributeDesc);
                case FacilityPointAttributeOptions.AttributeMetaType.Value:
                default:
                    return new CygNetConvertableValue(attrVal.RawValue);
            }
        }


        public static (CachedCygNetFacility Facility, ResolverErrorConditions Error) ResolveChildFacility(
            ICygNetPointReference srcRule,
            ICachedCygNetFacility srcFacility,
            IEnumerable<ChildFacilityGroup> srcChildGroups)
        {
            var childGroups = new Dictionary<string, ChildFacilityGroup>();
            foreach (var grp in srcChildGroups)
            {
                childGroups[grp.GroupKey.Id] = grp;
            }

            // -- If the type is Base, we have that already
            switch (srcRule.FacilityChoice)
            {
                case FacilitySelection.FacilityChoices.ParentFacility:
                    if (srcFacility.ParentFacility != null)
                    {
                        // Take first, multiple not currently supported
                        var parentFac = srcFacility.ParentFacility;
                        return (parentFac as CachedCygNetFacility, ResolverErrorConditions.None);
                    }
                    else
                    {
                        return (null, ResolverErrorConditions.ParentFacNotAvailable);
                    }
                case FacilitySelection.FacilityChoices.ChildFacility:
                    if (string.IsNullOrWhiteSpace(srcRule.ChildGroupKey.Id))
                    {
                        return (null, ResolverErrorConditions.ChildGroupIdBlank);
                    }

                    // Check if group id exists

                    if (childGroups.TryGetValue(srcRule.ChildGroupKey.Id, out var thisGrp))
                    {
                        var childFacs = srcFacility.ChildFacilities;
                        var childGrps = new FindChildGroups(childFacs);

                        var facsInGroup =
                            childGrps.GetOrdinalizedChildrenInGroup(thisGrp.FilterConditions
                                .ToList<IFacilityFilterCondition>());

                        int thisOrd = Convert.ToInt16(srcRule.ChildOrdinal);
                        if (facsInGroup.TryGetValue(thisOrd, out var thisFac))
                        {
                            return (thisFac as CachedCygNetFacility, ResolverErrorConditions.None);
                        }
                        else
                        {
                            return (null, ResolverErrorConditions.ChildFacOrdinalNotFound);
                        }
                    }
                    else
                    {
                        return (null, ResolverErrorConditions.ChildInvalidGroupId);
                    }

                case FacilitySelection.FacilityChoices.BaseFacility:
                default:
                    var fac = srcFacility as CachedCygNetFacility;
                    return (fac, ResolverErrorConditions.None);
            }
        }
    }
}