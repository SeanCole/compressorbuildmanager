﻿using Serilog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Techneaux.CygNetWrapper.Facilities;
using Techneaux.CygNetWrapper.Points;
using Techneaux.CygNetWrapper.Services;
using Techneaux.CygNetWrapper.Services.FAC;
using TechneauxReportingDataModel.CygNet;

namespace CygNetRuleModel.Resolvers
{
    public class CachedFacilityResolver
    {
        private readonly CygNetGeneralOptions _myopts;
        private readonly CygNetResolverSearchResults _myResults = new CygNetResolverSearchResults();

        public CachedFacilityResolver(CygNetGeneralOptions opts)
        {
            _myopts = opts;
        }

        public async Task<CygNetResolverSearchResults> FindFacilities(CancellationToken ct)
        {
            // Get domain
            var myDomain = await GetCygNetDomain();

            if (myDomain == null)
            {
                return _myResults;
            }

            // Get facility service
            var myFacService = await GetFacilityService(myDomain, ct);

            if (myFacService == null)
            {
                return _myResults;
            }

            await myFacService.RefreshFacServiceAttributesAsync();

            // Get cached facilities
            try
            {
                var cachedFacs = await myFacService.GetCachedFacilitiesWithChildrenAndParents(
                    _myopts.FacilityFilteringRules.BaseRules,
                    _myopts.FacilityFilteringRules.ChildReferenceAttrKey.Id,
                    ct);

                _myResults.SetResults(myFacService, cachedFacs);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "General exception while trying to find facilities");
                _myResults.SetException(ex, CygNewSearchResolverExceptions.FacilityQueryError);
            }

            return _myResults;
        }

        private async Task<CygNetDomain> GetCygNetDomain()
        {
            CygNetDomain thisDomain;
            if (_myopts.UseCurrentDomain)
            {
                thisDomain = await Task.Run(() => CygNetDomain.GetCurrentDomainAsync());

                if (thisDomain == null)
                {
                    _myResults.SetException(null, CygNewSearchResolverExceptions.AmbientDomainNotFound);

                    return null;
                }
            }
            else
            {
                thisDomain = CygNetDomain.GetDomainAsync(_myopts.FixedDomainId);
            }

            return thisDomain;
        }

        private async Task<FacilityService> GetFacilityService(CygNetDomain currentDomain, CancellationToken ct)
        {
            if (!await currentDomain.RefreshSiteServiceInfoAsync(ct))
            {
                _myResults.SetException(null, CygNewSearchResolverExceptions.FailedToConnectToDomain);
                return null;
            }

            var availableFacServs = currentDomain.FacilityServices.ToDictionary(item => item.SiteService.ToString(), item => item);
            //FacilityService ThisFacServ =  //(Myopts.FacSiteService) as FacilityService;

            if (availableFacServs.TryGetValue(_myopts.FacSiteService, out var thisFacServ))
            {
                if (thisFacServ.IsServiceAvailable)
                {
                    return thisFacServ;
                }
                else
                {
                    _myResults.SetException(null, CygNewSearchResolverExceptions.FacilityServiceNotAvailable);
                    return null;
                }               
            }
            else
            {
                _myResults.SetException(null, CygNewSearchResolverExceptions.FacilityServiceNotAvailable);
                return null;
            }
        }

        public async Task<CygNetResolverSearchResults> GetPoints(
            IEnumerable<string> udcList, 
            CancellationToken ct)
        {
            var udcs = udcList.ToList();
            var pointList = new List<ICachedCygNetPoint>();

            var facSearchResult = await FindFacilities(ct);

            if (facSearchResult.SearchSucceeded)
            {
                // Precache points
                var firstFac = facSearchResult.FacsFound.FirstOrDefault();

                //await Task.Run(() => firstFac?.PointCache?.CachePoints(udcs, ct));

                foreach (var fac in facSearchResult.FacsFound)
                {  
                    foreach (var udc in udcs)
                    {
                        var point = await fac.GetPoint(udc, ct);
                        if (!string.IsNullOrWhiteSpace(udc) && point != null)
                        {
                            pointList.Add(point);
                        }
                    }
                }
            }

            facSearchResult.SetResults(pointList);

            return facSearchResult;
        }
    }


    public class CygNetResolverSearchResults
    {
        public FacilityService FacService { get; private set; }
        public List<CachedCygNetFacility> FacsFound { get; private set; }

        public List<ICachedCygNetPoint> PointsFound { get; set; }

        public CygNewSearchResolverExceptions ExcpDetails { get; private set; }
        public Exception Excp { get; private set; }
        public bool SearchSucceeded { get; private set; }

        public void SetException(Exception ex, CygNewSearchResolverExceptions details)
        {
            FacService = null;
            FacsFound = null;

            SearchSucceeded = false;

            Excp = ex;
            ExcpDetails = details;
        }

        public void SetResults(FacilityService thisServ, IEnumerable<CachedCygNetFacility> facsFound)
        {
            Excp = null;
            ExcpDetails = CygNewSearchResolverExceptions.None;

            SearchSucceeded = true;

            FacService = thisServ;
            FacsFound = facsFound.ToList();
        }

        public void SetResults(IEnumerable<ICachedCygNetPoint> pntsFound)
        {
            Excp = null;
            ExcpDetails = CygNewSearchResolverExceptions.None;

            SearchSucceeded = true;
          
            PointsFound = pntsFound.ToList();
        }
    }

    public enum CygNewSearchResolverExceptions
    {
        None,

        [Description("No ambient CygNet domain was found")]
        AmbientDomainNotFound,

        [Description("Could not connect to designated domain")]
        FailedToConnectToDomain,

        [Description("Could not find or connect to the designated facility service")]
        FacilityServiceNotAvailable,

        [Description("Error while searching for facilities")]
        FacilityQueryError

    }
}
