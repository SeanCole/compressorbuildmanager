﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Media;
using CompressorBuilder.DataGrids;
using CompressorBuilder.DataModel;
using CompressorBuilder.DataModel.Sub;
using CompressorBuilder.ViewModels;

using CompressorTemplateDataModel;
using CygNet.Data.Facilities;
using MahApps.Metro.Controls;
using Microsoft.Win32;
using Serilog;
using Serilog.Sinks.RollingFile;
using Techneaux.CygNetWrapper.Facilities.Attributes;
using XmlDataModelUtility;

namespace CompressorBuilder
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        readonly SavedFacPointDefaultsConfigModel _defaultsDataModel;
        readonly string _defaultsFilePath = $@"{AppDomain.CurrentDomain.BaseDirectory}defaults.json";

        public MainWindow()
        {
            InitializeComponent();
            //MessageBox.Show(Assembly.GetExecutingAssembly().GetName().Version.ToString());          
            Title = $"Compressor Builder Client (Version {Assembly.GetExecutingAssembly().GetName().Version})";
            try
            {
                var fileName = $"Compressor Builder " + "{Date}.txt";
                Log.Logger = new LoggerConfiguration()
                    .MinimumLevel.Debug()
                    .WriteTo
                    .RollingFile($@"{AppDomain.CurrentDomain.BaseDirectory}Logs\{fileName}", shared: true)
                    .CreateLogger();
                Log.Debug("Launching client.");

                //MessageBox.Show(AppDomain.CurrentDomain.BaseDirectory);

                _defaultsDataModel = SavedFacPointDefaultsConfigModel.LoadJsonFromFile(_defaultsFilePath);

                var myViewModel = new MainViewModel(_defaultsDataModel);
                DataContext = myViewModel;

                _defaultsDataModel.PropertyChanged += _defaultsDataModel_PropertyChanged;

                //this.FacAttributeSelector.DataContextChanged += FacSelector_DataContextChanged;
                Closing += MainWindow_Closing;
                BtnLoadConfig.Click += Button_LoadConfig;

                //BtnNewFile.Click += Button_New;
                //BtnOpenFile.Click += Button_Open;
                //BtnSaveFile.Click += Button_Save;
                //BtnSaveFileAsNew.Click += Button_SaveAsNew;

                Loaded += MainWindow_Loaded;

                // Initialize nested VMs
                InitPointsGrid();
            }
            catch (Exception e)
            {
                // Log.Error(e, "Fatal error");
                MessageBox.Show(e.ToString());
            }
        }

        private async void _defaultsDataModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            await SavedFacPointDefaultsConfigModel.SaveJsonToFile(_defaultsDataModel, _defaultsFilePath);
        }

        private void DefaultsChanged(Object sender, DataTransferEventArgs args)
        {
            SaveDefaults();
        }

        #region PointsTab
        /// <summary>
        /// Performs initialization routine for <see cref="PointDefaultGrid"/>.
        /// </summary>
        private void InitPointsGrid()
        {
            // Add columns to datagrid
            DefaultPointGridConfigViewModel.AddGridColumns(ref PointDefaultGrid);
            // Initialize column selector
            DefaultPointGridConfigViewModel.UseColumnSelector(ref PointColumnSelectorListbox,
                new ReadOnlyCollection<DataGridColumn>(PointDefaultGrid.Columns));
        }

        #endregion

        public async void SaveDefaults()
        {
            await SavedFacPointDefaultsConfigModel.SaveJsonToFile(_defaultsDataModel, _defaultsFilePath);
            Console.WriteLine("Saved defaults");
        }

        //private async void FacSelector_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        //{
        //    var columnsListVm = FacilityDefaultsDataGrid.DataContext as DefaultFacilityGridViewModel;
        //    columnsListVm.ReactiveCygNetViewModel.AvailableFacilityAttributes.ListChanged += Attr_ListChanged;
        //    await AddGridColumnsAsync();
        //}

        private void FacilityDefaultsDataGrid_TargetUpdated(object sender, DataTransferEventArgs e)
        {
            for (var i = 0; i < FacilityDefaultsDataGrid.Columns.Count; i++)
            {
                FacilityDefaultsDataGrid.Columns[i].Width = 0;
            }
            FacilityDefaultsDataGrid.UpdateLayout();
            for (var j = 0; j < FacilityDefaultsDataGrid.Columns.Count; j++)
            {
                FacilityDefaultsDataGrid.Columns[j].Width = new DataGridLength(1, DataGridLengthUnitType.Auto);
            }
        }

        private async void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            var vm = DataContext as MainViewModel;

            try
            {

                await AddGridColumnsAsync();
                ApplyColumnSelector(ref FacilityColumnSelectorListbox,
                    new ReadOnlyCollection<DataGridColumn>(FacilityDefaultsDataGrid.Columns),
                    true,
                    new[] { 0, 1, 2, 3 });
                SetVisibleFacilityColumns();
                SetVisiblePointColumns();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void PointDefaultDataGrid_TargetUpdated(object sender, DataTransferEventArgs e)
        {
            for (var i = 0; i < PointDefaultGrid.Columns.Count; i++)
            {
                PointDefaultGrid.Columns[i].Width = 0;
            }
            PointDefaultGrid.UpdateLayout();
            for (var j = 0; j < PointDefaultGrid.Columns.Count; j++)
            {
                PointDefaultGrid.Columns[j].Width = new DataGridLength(1, DataGridLengthUnitType.Auto);
            }
        }

        private void MainWindow_Closing(object sender, CancelEventArgs e)
        {
            //foreach (var pnt in _defaultsDataModel.DefaultPointOptions.DefaultCompressorPoints)
            //{
            //    pnt.PointRecord.Tag = null;
            //}

            //foreach (var fac in _defaultsDataModel.DefaultFacilityOptions.DefaultCompressorFacs)
            //{
            //    fac.Facility.Tag = null;
            //}

            _defaultsDataModel.DefaultPointOptions.VisibleColumns.Clear();
            _defaultsDataModel.DefaultFacilityOptions.VisibleColumns.Clear();
            foreach (KeyValuePair<int, string> item in FacilityColumnSelectorListbox.SelectedItems)
            {
                _defaultsDataModel.DefaultFacilityOptions.VisibleColumns.Add(item.Key);
            }

            foreach (KeyValuePair<int, string> item in PointColumnSelectorListbox.SelectedItems)
            {
                _defaultsDataModel.DefaultPointOptions.VisibleColumns.Add(item.Key);
            }

            SaveDefaults();
        }

        //private async void Button_New(object sender, RoutedEventArgs e)
        //{

        //    if (_configFile.CreateNewConfig())
        //    {
        //        var myViewModel = new MainViewModel(_defaultsDataModel);

        //        DataContext = null;
        //        DataContext = myViewModel;
        //        _configFile.ResetModified();                
        //    }
        //}

        //private async void Button_Open(object sender, RoutedEventArgs e)
        //{
        //    if (_configFile.OpenConfigFile())
        //    {
        //        try
        //        {
        //            var myViewModel = new MainViewModel(_defaultsDataModel);

        //            DataContext = null;

        //            MainTabControl.DataContext = myViewModel;
        //            _configFile.ResetModified();

        //        }
        //        catch (Exception ex)
        //        {
        //            //Log.Error(ex, "Fatal error");
        //            MessageBox.Show(ex.ToString());
        //        }

        //    }
        //}

        private async void Button_LoadConfig(object sender, RoutedEventArgs e)
        {
            DataContext = null;

            var loadedCompProgram = OpenTemplateConfigFile();

            if (loadedCompProgram != null)
            {
                try
                {
                    var currentDefaultPoints = _defaultsDataModel.DefaultPointOptions.DefaultCompressorPoints;
                    var AllPointList = _defaultsDataModel.DefaultPointOptions.AllSavedCompressorPoints;

                    currentDefaultPoints.Clear();                 

                    // Load DTF points
                    foreach (var compPoint in loadedCompProgram.CompPointsInUse)
                    {
                        var matchingCompPoint = AllPointList
                            .FirstOrDefault(rule => rule.DTFPnt.RegTag == compPoint.DTFPnt.RegTag);

                        if (matchingCompPoint == null)
                        {
                            matchingCompPoint = new CompPointExtended(compPoint)
                            {
                                Udc = compPoint.DTFPnt.DefaultUDC
                            };

                            matchingCompPoint.PointRecord.EnumTableName = matchingCompPoint.StatusEnumTableName;
                            AllPointList.Add(matchingCompPoint);
                        }
                        else
                        {
                            matchingCompPoint.DTFPnt = compPoint.DTFPnt;
                        }

                        if (!_defaultsDataModel
                            .DefaultFacilityOptions
                            .DefaultCompressorFacs
                            .Any(fac => fac.FacilityGrpId == compPoint.DTFPnt.FacGroupTag))
                        {
                            var newDefFac = new DeviceFacilityConfig(compPoint.DTFPnt.FacGroupTag);
                            _defaultsDataModel.DefaultFacilityOptions.DefaultCompressorFacs.Add(newDefFac);
                        }

                        currentDefaultPoints.Add(matchingCompPoint);
                    }

                    _defaultsDataModel.DefaultPointOptions.EnumTables = loadedCompProgram.EnumTables;
                    _defaultsDataModel.DeviceOpts.Description = loadedCompProgram.Description;

                    _defaultsDataModel.DeviceOpts.BaseAddress = loadedCompProgram.BaseAddress;
                    _defaultsDataModel.DeviceOpts.OrdinalOffset = loadedCompProgram.OrdinalOffset;

                    _defaultsDataModel.DeviceOpts.program = loadedCompProgram;
                }
                catch (Exception ex)
                {
                    //Log.Error(ex, "Fatal error");
                    MessageBox.Show(ex.ToString());
                }
            }

            var myViewModel = new MainViewModel(_defaultsDataModel);

            DataContext = myViewModel;
        }

        public CompProgram OpenTemplateConfigFile()
        {
            {
                var openFileDlg = new OpenFileDialog
                {
                    DefaultExt = "ctmpl",
                    Filter = "Compressor Template Files (*.ctmpl)|*.ctmpl"
                };

                if (openFileDlg.ShowDialog(this) == true)
                {
                    var thisConfigPath = openFileDlg.FileName;

                    var loadedConfig = XmlFileSerialization<CompProgram>.LoadModelFromGzipFile(thisConfigPath);

                    if (loadedConfig == null)
                    {
                        MessageBox.Show(this, "Failed to load template file. Please check the format or create a new one.");

                        return null;
                    }
                    else
                    {
                        return loadedConfig;
                    }
                }

                return null;
            }
        }

        //private void Button_Save(object sender, RoutedEventArgs e)
        //{
        //    /////////////////////////////////////////////////////////////////////////
        //    // TODO: Remove me
        //    var model = DataContext as MainViewModel;
        //    var str = SavedFacPointDefaultsConfigModel.ToJson(model.MyDataModel);
        //    var newModel = SavedFacPointDefaultsConfigModel.FromJson(str);
        //    /////////////////////////////////////////////////////////////////////////

        //    _configFile.SaveConfigFile();
        //}

        //private void Button_SaveAsNew(object sender, RoutedEventArgs e)
        //{
        //    _configFile.SaveConfigFile(true);
        //}

        private void TextBox_SourceUpdated(object sender, DataTransferEventArgs e)
        {

        }

        /// <summary>
        /// Adapts a <see cref="ListBox"/> control for use as a column selector.
        /// </summary>
        /// <param name="columnSelectorListBox">The <see cref="ListBox"/> control to use as a selector.</param>
        /// <param name="columns">A <see cref="ReadOnlyCollection{T}"/> of the datagrid's columns.</param>
        /// <param name="hideColumnsByDefault">Sets all columns to hidden.</param>
        /// <param name="columnIndexesToIgnore">A collection of indexes for columns that will be excluded from the listbox.</param>
        public static void ApplyColumnSelector(ref ListBox columnSelectorListBox,
            ReadOnlyCollection<DataGridColumn> columns,
            bool hideColumnsByDefault = true,
            IEnumerable<int> columnIndexesToIgnore = null)
        {
            // Create column selector
            int i = 0;
            var items = new Dictionary<int, string>();
            var indexesToIgnore = columnIndexesToIgnore as int[] ?? new int[] { };

            foreach (var c in columns)
            {
                if (!indexesToIgnore.Contains(i) && (string)c.Header != "Fac Group Tag" && (string)c.Header != "Facility Id" &&
                    (string)c.Header != "Ordinal" && (string)c.Header != "facility_site" && (string)c.Header != "facility_id" &&
                    (string)c.Header != "facility_service" && (string)c.Header != "facility_category")
                    items.Add(i, c.Header.ToString());
                i++;
            }
            columnSelectorListBox.ItemsSource = items;
            columnSelectorListBox.SelectedValue = "Key";
            columnSelectorListBox.SelectionMode = SelectionMode.Multiple;
            columnSelectorListBox.DisplayMemberPath = "Value";
            columnSelectorListBox.SelectionChanged += (o, args) =>
            {
                if (!(o is ListBox sender)) return;
                foreach (KeyValuePair<int, string> item in sender.Items)
                {
                    if (sender.SelectedItems.Contains(item))
                    {
                        columns[item.Key].Visibility = Visibility.Visible;
                    }
                    else
                    {
                        columns[item.Key].Visibility = Visibility.Hidden;
                    }
                }
            };

            if (hideColumnsByDefault)
            {
                RoutedEvent routed = ListBox.SelectionChangedEvent;
                var e = new SelectionChangedEventArgs(routed, new List<object>(), new List<object>());
                columnSelectorListBox.RaiseEvent(e);
            };
        }

        private void SetVisibleFacilityColumns()
        {
            var indexes = this._defaultsDataModel.DefaultFacilityOptions.VisibleColumns;
            foreach (KeyValuePair<int, string> item in FacilityColumnSelectorListbox.Items)
            {
                if (indexes.Contains(item.Key)) FacilityColumnSelectorListbox.SelectedItems.Add(item);
            }
        }

        private void SetVisiblePointColumns()
        {
            var indexes = this._defaultsDataModel.DefaultPointOptions.VisibleColumns;
            foreach (KeyValuePair<int, string> item in PointColumnSelectorListbox.Items)
            {
                if (indexes.Contains(item.Key)) PointColumnSelectorListbox.SelectedItems.Add(item);
            }
        }

        //TODO: Get reactivecygnetviewmodel from datacontext for column visibility . Fac attr wrappers have to come from data model.
        private async Task AddGridColumnsAsync()
        {
            if (!(this.DataContext is MainViewModel columnsListVm)) return;
            var facAttributes = FacilityAttributeWrapper.AllPossibleAttrWrappers;
            facAttributes = facAttributes.Where(x => x.ColumnName != "facility_site" && x.ColumnName != "facility_service" && x.ColumnName != "facility_id" && x.ColumnName != "facility_category").ToList();
            foreach (var attr in facAttributes)
            {
                // Identify the category to of the attribute.
                string category = "";
                switch (attr.AttributeType)
                {
                    case FacilityAttributeTypes.YesNo:
                    case FacilityAttributeTypes.Info:
                    case FacilityAttributeTypes.Table:
                        category = $"{attr.AttributeType:G}";
                        break;
                    case FacilityAttributeTypes.Text:
                        category = "Attribute";
                        break;
                    case FacilityAttributeTypes.General:
                        foreach (var cn in new[] { "Category", "Description", "Type" })
                        {
                            if (attr.ColumnName.ToLower().Contains(cn.ToLower()))
                            {
                                category = cn;
                                break;
                            }
                        }
                        break;
                }

                // Using regular expressions to identify arrays.
                var rx = new Regex("[0-9]$");
                var bindingPath = rx.IsMatch(attr.ColumnName) ? $"Facility.{category}[{attr.AttributeIndex}]" : $"Facility.{category}";
                DataGridBoundColumn col = null;

                // Note: All attribute types are text except YesNo.
                // Use this switch statement to implement different/additional types if necessary.
                switch (attr.AttributeType)
                {
                    case FacilityAttributeTypes.YesNo:
                        col = new DataGridCheckBoxColumn();
                        break;
                    default:
                        col = new DataGridTextColumn();
                        break;
                }

                col.Binding = new Binding(bindingPath)
                {
                    Mode = BindingMode.TwoWay,
                    UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged
                };
                col.Header = attr.ColumnName;
                FacilityDefaultsDataGrid.Columns.Add(col);
            }

            var refreshDataPreviewBinding = new Binding("FacilityDefaultsVM");
            FacilityDefaultsDataGrid.ItemsSource = null;
            //DataPreview.ItemsSource = null;
            BindingOperations.SetBinding(FacilityDefaultsDataGrid, ItemsControl.ItemsSourceProperty, refreshDataPreviewBinding);

        }

        private static T GetFrameworkElementByName<T>(FrameworkElement referenceElement) where T : FrameworkElement

        {

            FrameworkElement child = null;

            for (Int32 i = 0; i < VisualTreeHelper.GetChildrenCount(referenceElement); i++)

            {

                child = VisualTreeHelper.GetChild(referenceElement, i) as FrameworkElement;

                System.Diagnostics.Debug.WriteLine(child);

                if (child != null && child.GetType() == typeof(T))

                { break; }

                else if (child != null)

                {

                    child = GetFrameworkElementByName<T>(child);

                    if (child != null && child.GetType() == typeof(T))

                    {

                        break;

                    }

                }

            }

            return child as T;

        }
        private int GetAttrIndex(string description)
        {
            return Convert.ToInt32(description.Substring(description.Length - 1, 1), 10);

        }
    }
}
