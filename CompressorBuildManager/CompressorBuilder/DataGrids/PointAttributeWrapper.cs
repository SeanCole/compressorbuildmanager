﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Reflection;
using System.Runtime.CompilerServices;
using Serilog;
using Techneaux.CygNetWrapper.Points;

namespace CompressorBuilder.DataGrids
{
    public class PointAttributeWrapper : PointAttribute, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        // This method is called by the Set accessor of each property.
        // The CallerMemberName attribute that is applied to the optional propertyName
        // parameter causes the property name of the caller to be substituted as an argument.
        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        public static Dictionary<string, PointAttributeWrapper> AllPointAttributeWrappers { get; private set; }
        static PointAttributeWrapper()
        {
            AllPointAttributeWrappers = new Dictionary<string, PointAttributeWrapper>
            {
                    // Add tag attributes
                    { "Tag-Site", new PointAttributeWrapper("[n/a]", "Tag-Site", "[Site]", "Tag", "String", null, null, null, null) },
                    { "Tag-Service", new PointAttributeWrapper("[n/a]", "Tag-Service", "[Service]", "Tag", "String", null, null, null, null) },
                    { "Tag-SiteService", new PointAttributeWrapper("[n/a]", "Tag-SiteService", "[Site].[Service]", "Tag", "String", null, null, null, null) },
                    { "Tag-PointId", new PointAttributeWrapper("[n/a]", "Tag-PointId", "[PointId]", "Tag", "String", null, null, null, null) },
                    { "Tag-SiteServPointId", new PointAttributeWrapper("[n/a]", "Tag-SiteServPointId", "[Site].[Service].[PointId]", "Tag", "String", null, null, null, null) },
                    { "Tag-FacilityId", new PointAttributeWrapper("[n/a]", "Tag-FacilityId", "[FacilityId]", "Tag", "String", null, null, null, null) },
                    { "Tag-Udc", new PointAttributeWrapper("[n/a]", "Tag-Udc", "[UDC]", "Tag", "String", null, null, null, null) },
                    { "Tag-FacUdc", new PointAttributeWrapper("[n/a]", "Tag-FacUdc", "[FacilityId]_[UDC]", "Tag", "String", null, null, null, null) },
                    { "Tag-SiteServFacUdc", new PointAttributeWrapper("[n/a]", "Tag-SiteServFacUdc", "[Site].[Service]::[FacilityId].[UDC]", "Tag", "String", null, null, null, null) },
                    { "Tag-PointIdLong", new PointAttributeWrapper("[n/a]", "Tag-PointIdLong", "[PointIdLong]", "Tag", "String", null, null, null, null) },
                    { "Tag-SiteServPointIdLong", new PointAttributeWrapper("[n/a]", "Tag-SiteServPointIdLong", "[Site].[Service]:[PointIdLong]", "Tag", "String", null, null, null, null) },
                    { "Tag-Full", new PointAttributeWrapper("[n/a]", "Tag-Full", "[Site].[Service].[PointId]:[PointIdLong]", "Tag", "String", null, null, null, null) }
            };

            // Parse included tab-delimited list
            var assembly = Assembly.Load("CygNetWrapper, Version = 1.0.0.0, Culture = neutral, PublicKeyToken = null");
            var resourceName = "Techneaux.CygNetWrapper.Points.PointAttributes.tab";

            using (var stream = assembly.GetManifestResourceStream(resourceName))
            using (var sr = new StreamReader(stream))
            {
                // Skip header row
                sr.ReadLine();

                while (!sr.EndOfStream)
                {
                    var thisLine = sr.ReadLine();

                    var lineItems = thisLine.Split('\t');

                    try
                    {
                        if (Convert.ToBoolean(lineItems[(int)SourceColumnIndexes.Supported]))
                        {
                            int? thisIndex = null;
                            if (int.TryParse(lineItems[(int)SourceColumnIndexes.Index], out var testIndex))
                                thisIndex = testIndex;

                            var newAttr = new PointAttributeWrapper(
                                    lineItems[(int)SourceColumnIndexes.SqlColumnName],
                                    lineItems[(int)SourceColumnIndexes.ClassPropertyName],
                                    lineItems[(int)SourceColumnIndexes.Description],
                                    lineItems[(int)SourceColumnIndexes.Category],
                                    lineItems[(int)SourceColumnIndexes.DataType],
                                    lineItems[(int)SourceColumnIndexes.EnumName],
                                    thisIndex,
                                    lineItems[(int)SourceColumnIndexes.SqlRawValues],
                                    lineItems[(int)SourceColumnIndexes.EnumEquivalentIndices]);

                            // Validate that class property with the associated name exists
                            //Debug.Assert(TestPntRecord.HasProperty(NewAttr.ClassPropertyName));

                            // CODE HERE => Check for properties existing, getting null ref exception in the nested properties

                            //if (TestPntRecord.HasProperty(NewAttr.ClassPropertyName))
                            //{
                            AllPointAttributes[newAttr.Id] = newAttr;
                            //}
                        }
                    }
                    catch (Exception ex)
                    {
                        Log.Fatal(ex, "");
                    }
                }
            }
        }

        private PointAttributeWrapper(
          string newSqlColumnName,
          string newClassPropertyName,
          string newAttrDesc,
          string newCategory,
          string newValueType,
          string newEnumName,
          int? newEnumIndex,
          string newSqlRawValues,
          string newEnumEquivalentInt) : base(newSqlColumnName, newClassPropertyName, newAttrDesc, newCategory, newValueType, newEnumName, newEnumIndex, newSqlRawValues, newEnumEquivalentInt)
        {
            IsEnabled = false;
        }

        private bool _isEnabled;
        public bool IsEnabled
        {
            get
            {
                return this._isEnabled;
            }
            set
            {
                if (value != this._isEnabled)
                {
                    this._isEnabled = value;
                    NotifyPropertyChanged();
                }
            }
        }

    }
}
