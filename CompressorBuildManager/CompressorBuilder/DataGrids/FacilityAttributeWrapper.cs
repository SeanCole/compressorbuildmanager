﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using CygNet.Data.Facilities;
using Techneaux.CygNetWrapper.Facilities.Attributes;

namespace CompressorBuilder.DataGrids
{
    public class FacilityAttributeWrapper : FacilityAttribute, INotifyPropertyChanged 
    {
        public FacilityAttributeWrapper()
        {

        }

        public event PropertyChangedEventHandler PropertyChanged;

        // This method is called by the Set accessor of each property.
        // The CallerMemberName attribute that is applied to the optional propertyName
        // parameter causes the property name of the caller to be substituted as an argument.
        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public FacilityAttributeWrapper(AttributeDefinition newAttribute) : base(newAttribute)
        {
         
        }

        static FacilityAttributeWrapper()
        {
            var allAttrs = new List<FacilityAttributeWrapper>();

            allAttrs.Add(new FacilityAttributeWrapper
            {
                AttributeType = FacilityAttributeTypes.General,
                ColumnName = $"facility_site",
                IsBoolean = false
            });

            allAttrs.Add(new FacilityAttributeWrapper
            {
                AttributeType = FacilityAttributeTypes.General,
                ColumnName = $"facility_service",
                IsBoolean = false
            });

            allAttrs.Add(new FacilityAttributeWrapper
            {
                AttributeType = FacilityAttributeTypes.General,
                ColumnName = $"facility_id",
                Description = "ID",
                IsBoolean = false
            });

            allAttrs.Add(new FacilityAttributeWrapper
            {
                AttributeType = FacilityAttributeTypes.General,
                ColumnName = $"facility_type",
                Description = "Type",
                IsBoolean = false
            });

            allAttrs.Add(new FacilityAttributeWrapper
            {
                AttributeType = FacilityAttributeTypes.General,
                ColumnName = $"facility_description",
                IsBoolean = false
            });

            allAttrs.Add(new FacilityAttributeWrapper
            {
                AttributeType = FacilityAttributeTypes.General,
                ColumnName = $"facility_category",
                Description = "Category",
                IsBoolean = false
            });

            for (uint i = 0; i <= 29; i++)
            {
                allAttrs.Add(new FacilityAttributeWrapper
                {
                    AttributeType = FacilityAttributeTypes.Text,
                    AttributeIndex = i,
                    ColumnName = $"facility_attr{i}",
                    IsBoolean = false
                });
            }

            for (uint i = 0; i <= 29; i++)
            {
                allAttrs.Add(new FacilityAttributeWrapper
                {
                    AttributeType = FacilityAttributeTypes.Table,
                    AttributeIndex = i,
                    ColumnName = $"facility_table{i}",
                    IsBoolean = false
                });
            }

            for (uint i = 0; i <= 19; i++)
            {
                allAttrs.Add(new FacilityAttributeWrapper
                {
                    AttributeType = FacilityAttributeTypes.YesNo,
                    AttributeIndex = i,
                    ColumnName = $"facility_yesno{i}",
                    IsBoolean = true
                });
            }

            for (uint i = 0; i < 2; i++)
            {
                allAttrs.Add(new FacilityAttributeWrapper
                {
                    AttributeType = FacilityAttributeTypes.Info,
                    AttributeIndex = i,
                    ColumnName =  $"facility_info{i}",
                    IsBoolean = true

                });
            }

            AllPossibleAttrWrappers = allAttrs;
        }
        public static List<FacilityAttributeWrapper> AllPossibleAttrWrappers { get; set; }
        private bool _isEnabled;
        public bool IsEnabled
        {
            get
            {
                return this._isEnabled;
            }
            set
            {
                if(value != this._isEnabled)
                {
                    this._isEnabled = value;
                    NotifyPropertyChanged();
                }
            }
        }
     
    }
}
