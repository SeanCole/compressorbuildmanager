﻿using System;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Serialization;
using CompressorBuilder.DataModel.Sub;
using CompressorTemplateDesigner.DataModel.JsonUtility;
using Newtonsoft.Json;
using Serilog;
using Techneaux.CygNetWrapper.Services.DDS.Devices;
using XmlDataModelUtility;

namespace CompressorBuilder.DataModel
{
    [Serializable]
    [XmlRoot("ConfigItems"), XmlType("ConfigItems")]
    public class SavedFacPointDefaultsConfigModel : NotifyModelBase
    {

        public SavedFacPointDefaultsConfigModel()
        {
            FacilityDefaultGridConfig = new DefaultsConfig();
            DefaultPointOptions = new DefaultPointOptions();
            DefaultFacilityOptions = new DefaultFacilityOptions();
            DeviceOpts = new DeviceOptions();
        }

        public DefaultPointOptions DefaultPointOptions
        {
            get => GetPropertyValue<DefaultPointOptions>();
            set => SetPropertyValue(value);
        }

        public DeviceOptions DeviceOpts
        {
            get => GetPropertyValue<DeviceOptions>();
            set => SetPropertyValue(value);
        }

        public DefaultFacilityOptions DefaultFacilityOptions
        {
            get => GetPropertyValue<DefaultFacilityOptions>();
            set => SetPropertyValue(value);
        }

        public DefaultsConfig FacilityDefaultGridConfig
        {
            get => GetPropertyValue<DefaultsConfig>();
            set => SetPropertyValue(value);
        }

        #region Static Helpers

        private static JsonSerializerSettings _SerializerSettings { get; set; }
        private static JsonSerializerSettings SerializerSettings
        {
            get
            {
                if (_SerializerSettings == null)
                {
                    _SerializerSettings = new JsonSerializerSettings();
                    _SerializerSettings.Error += (o, args) =>
                    {
                        if (args.ErrorContext.Error.InnerException is NotImplementedException)
                            args.ErrorContext.Handled = true;
                    };
                }


                //_SerializerSettings.Converters.Add(new IndexedStringConverter());
                return _SerializerSettings;
            }
        }

        /// <summary>
        /// Returns the serialized string for the given <see cref="SavedFacPointDefaultsConfigModel"/> object.
        /// </summary>
        /// <param name="model">The <see cref="SavedFacPointDefaultsConfigModel"/> object to serialize.</param>
        /// <returns></returns>
        public static string ToJson(SavedFacPointDefaultsConfigModel model)
        {
            //var jsonSettings = new JsonSerializerSettings();
            //SerializerSettings.Converters.Add(new IndexedStringConverter());

            return JsonConvert.SerializeObject(model, Formatting.Indented, SerializerSettings);
        }

        private static SemaphoreSlim saveSem = new SemaphoreSlim(1, 1);
        public static async Task<bool> SaveJsonToFile(
            SavedFacPointDefaultsConfigModel model,
            string filePath)
        {
            try
            {
                var jsonStr = ToJson(model);

                await saveSem.WaitAsync();
                using (var sw = new StreamWriter(filePath, false, Encoding.ASCII))
                {
                    await sw.WriteAsync(jsonStr);
                }

                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);

                Log.Error(ex, "Error saving defaults file");
                return false;
            }
            finally
            {
                saveSem.Release();
            }
        }

        public static SavedFacPointDefaultsConfigModel LoadJsonFromFile(
            string filePath)
        {
            try
            {
                using (var sr = new StreamReader(filePath, Encoding.ASCII))
                {
                    var jsonStr = sr.ReadToEnd();
                    var newModel = FromJson(jsonStr);
                    if (newModel == null)
                    {
                        newModel = new SavedFacPointDefaultsConfigModel();
                    }
                    return newModel;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);

                Log.Error(ex, "Error loading defaults file");
                return new SavedFacPointDefaultsConfigModel();
            }
        }

        /// <summary>
        /// Returns the deserialized <see cref="SavedFacPointDefaultsConfigModel"/> object from the given string.
        /// </summary>
        /// <param name="model">The JSON-formatted string containing the <see cref="SavedFacPointDefaultsConfigModel"/> data.</param>
        /// <returns></returns>
        public static SavedFacPointDefaultsConfigModel FromJson(string model)
        {
            var jsonSettings = new JsonSerializerSettings();
            jsonSettings.Converters.Add(new FacilityTagJsonConverter());
            jsonSettings.Converters.Add(new PointTagJsonConverter());
            jsonSettings.Converters.Add(new FacilityRecordConverter());

            var retval = JsonConvert
                .DeserializeObject<SavedFacPointDefaultsConfigModel>(model, jsonSettings);
            return retval;
        }




        #endregion

    }
}
