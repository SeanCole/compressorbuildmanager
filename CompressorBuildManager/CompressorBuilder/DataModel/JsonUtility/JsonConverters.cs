﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CygNet.Core;
using CygNet.Data.Core;
using CygNet.Data.Facilities;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CompressorTemplateDesigner.DataModel.JsonUtility
{
    public class PointTagJsonConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return (objectType == typeof(PointTag));
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if (reader.Value == null)
                return null;
            
            // Load the JSON for the Result into a JObject
            JObject jo = JObject.Load(reader);

            // Read the properties which will be used as constructor parameters
            string site = (string)jo["Site"];
            string serv = (string)jo["Service"];
            string facilityid = (string)jo["FacilityId"];
            string udc = (string) jo["UDC"];

            if (string.IsNullOrWhiteSpace(site) ||
                string.IsNullOrWhiteSpace(serv) ||
                string.IsNullOrWhiteSpace(facilityid) ||
                string.IsNullOrWhiteSpace(udc))
            {
                return null;
            }

            // Construct the Result object using the non-default constructor
            try
            {
                var newTag = new PointTag(
                    new FacilityTag(
                        new SiteService(site,serv),
                        facilityid
                        ),
                    udc);

                return newTag;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public override bool CanWrite
        {
            get { return false; }
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }

    public class FacilityTagJsonConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return (objectType == typeof(FacilityTag));
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if (reader.Value == null)
                return null;
            
            // Load the JSON for the Result into a JObject
            JObject jo = JObject.Load(reader);

            // Read the properties which will be used as constructor parameters
            string site = (string)jo["Site"];
            string serv = (string)jo["Service"];
            string facilityid = (string)jo["FacilityId"];
            string udc = (string) jo["UDC"];

            if (string.IsNullOrWhiteSpace(site) ||
                string.IsNullOrWhiteSpace(serv) ||
                string.IsNullOrWhiteSpace(facilityid) ||
                string.IsNullOrWhiteSpace(udc))
            {
                return null;
            }

            // Construct the Result object using the non-default constructor
            try
            {
                var newTag = new FacilityTag(
                        new SiteService(site,serv),
                        facilityid);

                return newTag;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public override bool CanWrite
        {
            get { return false; }
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }

    class IndexedStringConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return (objectType == typeof(Indexer<string>));
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            writer.WriteStartObject();
            var i = 0;
            foreach (string val in (Indexer<string>)value)
            {
                writer.WritePropertyName(i.ToString());
                serializer.Serialize(writer, val);

                i++;
            }
            writer.WriteEndObject();
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }

    public class FacilityRecordConverter : JsonConverter
    {

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var obj = new FacilityRecord();    
            var jObj = JObject.Load(reader);
            JsonConvert.PopulateObject(jObj.ToString(), obj);           // populate fields we don't need any special handling for
            
            var attrVals = jObj["Attribute"];
            
            if (attrVals != null)
            {
                var strings = attrVals.ToObject<List<string>>();
                var i = 0;
                foreach (var s in strings)
                {
                    obj.Attribute[i] = s;
                    i++;
                }
            }

            var tableVals = jObj["Table"];
            
            if (tableVals != null)
            {
                var strings = tableVals.ToObject<List<string>>();
                var i = 0;
                foreach (var s in strings)
                {
                    obj.Table[i] = s;
                    i++;
                }
            }
            
            var yesNoVals = jObj["YesNo"];
            
            if (yesNoVals != null)
            {
                var bools = yesNoVals.ToObject<List<bool>>();
                var i = 0;
                foreach (var s in bools)
                {
                    obj.YesNo[i] = s;
                    i++;
                }
            }

            var infoVals = jObj["Info"];
            
            if (infoVals != null)
            {
                var strings = infoVals.ToObject<List<string>>();
                var i = 0;
                foreach (var s in strings)
                {
                    obj.Info[i] = s;
                    i++;
                }
            }


            return obj;
        }



        public override bool CanWrite
        {
            get { return false; }
        }

        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(FacilityRecord);
        }
    }



}
