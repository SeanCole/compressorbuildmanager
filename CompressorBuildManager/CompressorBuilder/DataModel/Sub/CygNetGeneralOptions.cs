using System;
using System.Xml.Serialization;
using XmlDataModelUtility;

namespace CompressorBuilder.DataModel.Sub
{
    public class CygNetGeneralOptions : NotifyCopyDataModel, IValidatedRule
    {       
        public CygNetGeneralOptions()
        {
            UseCurrentDomain = true;
            FixedDomainId = 5410;
            FacSiteService = "";
            DDSService = "";
        }

        [XmlAttribute]
        public bool UseCurrentDomain
        {
            get => GetPropertyValue<bool>();
            set => SetPropertyValue(value);
        }

        [XmlAttribute]
        public ushort FixedDomainId
        {
            get => GetPropertyValue<ushort>();
            set => SetPropertyValue(value);
        }

        [XmlAttribute]
        public string FacSiteService
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }

        [XmlAttribute]
        public string DDSService
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }

        //[XmlAttribute]
        //public string AssocPntService
        //{
        //    get => GetPropertyValue<string>();
        //    set => SetPropertyValue(value);
        //}

        //[XmlAttribute]
        //public string AssocFacService
        //{
        //    get => GetPropertyValue<string>();
        //    set => SetPropertyValue(value);
        //}
        

        public bool IsRuleValid => CheckIfValid.isValid;

        public string ValidationErrorMessage => CheckIfValid.message;

        private (bool isValid, string message) CheckIfValid => (true, "");
    }
}
