﻿using GenericRuleModel.Rules;
using System.Collections.Generic;
using System.ComponentModel;
using XmlDataModelUtility;

namespace CompressorBuilder.DataModel.Sub
{
    public class DefaultPointOptions : NotifyCopyDataModel
    {
        
        public DefaultPointOptions()
        {
            DefaultCompressorPoints = new BindingList<CompPointExtended>();
            AllSavedCompressorPoints = new List<CompPointExtended>();
        }

        public BindingList<CompPointExtended> DefaultCompressorPoints
        {
            get => GetPropertyValue<BindingList<CompPointExtended>>();
            set => SetPropertyValue(value);
        }

        public List<CompPointExtended> AllSavedCompressorPoints
        {
            get => GetPropertyValue<List<CompPointExtended>>();
            set => SetPropertyValue(value);
        }

        public EnumerationTables EnumTables { get; set; } = new EnumerationTables();
        public List<int> VisibleColumns { get; set; } = new List<int>();
    }
}
