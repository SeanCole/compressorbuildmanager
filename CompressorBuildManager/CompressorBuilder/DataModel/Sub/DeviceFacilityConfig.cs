﻿using System.Xml.Serialization;
using CygNet.Data.Facilities;
using XmlDataModelUtility;

namespace CompressorBuilder.DataModel.Sub
{
    public class DeviceFacilityConfig : NotifyCopyDataModel
    {

        public DeviceFacilityConfig()
        {
            FacilityGrpId = "";
            Ordinal = 0;
            Facility = new  FacilityRecord();

            for (int i = 0; i < 30; i++)
            {
                Facility.Attribute[i] = "";
            }

            FacilityId = "";
        }

        public DeviceFacilityConfig(string FacGrpId)
        {
            FacilityGrpId = FacGrpId;
            Ordinal = 0;
            Facility = new FacilityRecord();
        }
        
        public string FacilityId
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }

        public string FacilityGrpId
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }

        public int Ordinal
        {
            get => GetPropertyValue<int>();
            set => SetPropertyValue(value);
        }

        [XmlIgnore]
        public FacilityRecord Facility
        {
            get => GetPropertyValue<FacilityRecord>();
            set => SetPropertyValue(value);
        }
    }
}
