﻿using System.Xml.Serialization;
using CompressorTemplateDataModel;
using CygNet.API.Points;
using CygNet.Data.Points;

namespace CompressorBuilder.DataModel.Sub
{
    public class CompPointExtended : CompPoint
    {
        public CompPointExtended() : base()
        {
            PointRecord = new PointConfigRecord();
            Udc = "";
        }

        public CompPointExtended(CompPoint point) : base()
        {
            this.DTFPnt = point.DTFPnt;
            this.InUse = point.InUse;
            this.StatusEnumTableDescription = point.StatusEnumTableDescription;
            this.StatusEnumTableName = point.StatusEnumTableName;
            PointRecord = ConfigClient.ConstructPointRecord();
        }
        
        public string Udc
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }

        [XmlIgnore]
        public PointConfigRecord PointRecord
        {
            get => GetPropertyValue<PointConfigRecord>();
            set => SetPropertyValue(value);
        }
    }
}
