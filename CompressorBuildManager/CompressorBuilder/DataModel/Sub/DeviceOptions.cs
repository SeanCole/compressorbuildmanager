﻿using CompressorTemplateDataModel;
using Techneaux.CygNetWrapper.Services.DDS.Devices;
using XmlDataModelUtility;

namespace CompressorBuilder.DataModel.Sub
{
    public class DeviceOptions : NotifyCopyDataModel
    {
        public DeviceOptions()
        {
            ID = "DefaultId";
            BaseAddress = 0;
            OrdinalOffset = 0;
            Description = "";
            DeviceDefinition = new DeviceDefinitions();
        }

        public CompProgram program { get; set; }
        
        
        public string ID
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }

        public uint BaseAddress
        {
            get => GetPropertyValue<uint>();
            set => SetPropertyValue(value);
        }
        public uint OrdinalOffset
        {
            get => GetPropertyValue<uint>();
            set => SetPropertyValue(value);
        }

        public string Description
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }

        public DeviceDefinitions DeviceDefinition
        { 
            get => GetPropertyValue<DeviceDefinitions>();
            set => SetPropertyValue(value);
        }

        //public NotifyDynamicBase<DeviceDefinitions.CygNetDevice.DeviceAttributes> DeviceAttrs
        //{

        //}

    }
}
