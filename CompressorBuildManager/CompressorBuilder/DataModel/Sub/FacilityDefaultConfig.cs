﻿using XmlDataModelUtility;

namespace CompressorBuilder.DataModel.Sub
{
    public class DefaultsConfig : NotifyCopyDataModel
    {
        public DefaultsConfig()
        {
            CygNetGeneral = new CygNetGeneralOptions();
        }

        public CygNetGeneralOptions CygNetGeneral
        {
            get => GetPropertyValue<CygNetGeneralOptions>();
            set => SetPropertyValue(value);
        }
    }
}
