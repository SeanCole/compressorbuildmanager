﻿using System.Collections.Generic;
using System.ComponentModel;
using XmlDataModelUtility;

namespace CompressorBuilder.DataModel.Sub
{
    public class DefaultFacilityOptions : NotifyCopyDataModel
    {
        public DefaultFacilityOptions()
        {
            DefaultCompressorFacs = new BindingList<DeviceFacilityConfig>();
        }

        public BindingList<DeviceFacilityConfig> DefaultCompressorFacs
        {
            get => GetPropertyValue<BindingList<DeviceFacilityConfig>>();
            set => SetPropertyValue(value);
        }
        
        public List<int> VisibleColumns { get; set; } = new List<int>();
    }
}
