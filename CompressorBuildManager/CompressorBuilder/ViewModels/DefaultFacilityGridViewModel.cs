﻿using System.ComponentModel;
using System.Linq;
using System.Windows.Input;
using CompressorBuilder.DataModel.Sub;
using CompressorBuilder.Forms;
using XmlDataModelUtility;

namespace CompressorBuilder.ViewModels
{
    public class DefaultFacilityGridViewModel : NotifyDynamicBase<DefaultFacilityOptions>
    {
        public DefaultFacilityGridViewModel(DefaultFacilityOptions srcModel) : base(srcModel)
        {
          //  ReactiveCygNetViewModel = new CygNetFacilityPointSearchViewModel(srcModel.CygNetGeneral);
            FacilityDefaultsVM = new BindingList<DefaultDeviceFacilityViewModel>(
                srcModel.DefaultCompressorFacs
                    .Select(fac => new DefaultDeviceFacilityViewModel(fac)).ToList());
            
            FacGroupWindow.StaticDataContext = this;
        }
        
        public DefaultDeviceFacilityViewModel SelectedDefault
        {
            get => GetPropertyValue<DefaultDeviceFacilityViewModel>();
            set => SetPropertyValue(value);
        }
        
        public BindingList<DefaultDeviceFacilityViewModel> FacilityDefaultsVM
        {
            get => GetPropertyValue<BindingList<DefaultDeviceFacilityViewModel>>();
            set => SetPropertyValue(value);
        }

        public ICommand AddFacDefaultRow => new DelegateCommand(AddSelectedFacilityDefaultRow);
        public ICommand DeleteFacDefaultRow => new DelegateCommand(DeleteSelectedFacilityDefaultRow);

        public void DeleteSelectedFacilityDefaultRow(object inputObject)
        {
            if(SelectedDefault == null)
                return;

            var index = FacilityDefaultsVM.IndexOf(SelectedDefault);
            MyDataModel.DefaultCompressorFacs.Remove(SelectedDefault.MyDataModel);
            FacilityDefaultsVM.Remove(SelectedDefault);
           
            if (FacilityDefaultsVM.FirstOrDefault() == null)
                return;
            if (index < 0)
                index = 0;
            else if (index >= FacilityDefaultsVM.Count)
                index = FacilityDefaultsVM.Count - 1;
            SelectedDefault = FacilityDefaultsVM[index];
        }

        public void AddSelectedFacilityDefaultRow(object inputObject)
        {
            var newGroup = new DeviceFacilityConfig();
            var newGroupView = new DefaultDeviceFacilityViewModel(newGroup);
            if (SelectedDefault == null)
            {

                //MyDataModel.FacilityDefaults.Add(newGroup);
                MyDataModel.DefaultCompressorFacs.Add(newGroup);
                FacilityDefaultsVM.Add(newGroupView);
                SelectedDefault = FacilityDefaultsVM[FacilityDefaultsVM.Count - 1];
                return;
            }

            var index = FacilityDefaultsVM.IndexOf(SelectedDefault);
            if (index < 0)
                index = 0;
            else if (index >= FacilityDefaultsVM.Count)
                index = FacilityDefaultsVM.Count - 1;

            //  MyDataModel.FacilityDefaults.Insert(index, newGroup);
            MyDataModel.DefaultCompressorFacs.Insert(index, newGroup);
            FacilityDefaultsVM.Insert(index, newGroupView);

            if (index < 0)
                index = 0;
            else if (index >= FacilityDefaultsVM.Count)
                index = FacilityDefaultsVM.Count - 1;

            SelectedDefault= FacilityDefaultsVM[index];

        }

    }
}
