﻿using CompressorTemplateDataModel;
using GenericRuleModel.Helper;
using XmlDataModelUtility;

namespace CompressorBuilder.ViewModels
{
    public class DTFPointViewModel : NotifyDynamicBase<DTFPoint>
    {
        public DTFPointViewModel(DTFPoint srcModel): base(srcModel)
        {

        }

        public ValueDescription DataTypeValue
        {
            get => new ValueDescription {Value = MyDataModel.DataType, Description = MyDataModel.DataType.ToString()};
            set => MyDataModel.DataType =(DTFPoint.ModBusDataTypes)value.Value;
        }
    }
}
