﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using CompressorBuilder.DataModel.Sub;
using CompressorBuilder.DeviceModel;
using CompressorTemplateDataModel;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using Serilog;
using Xceed.Wpf.DataGrid;
using XmlDataModelUtility;

namespace CompressorBuilder.ViewModels
{
    public class DeviceOptionsViewModel : NotifyDynamicBase<DeviceOptions>
    {
        public CompProgram program { get; set; }
        private CygNetFacilityPointSearchViewModel reactiveCygNet;
        private DefaultFacilityGridViewModel facilityGridViewModel;
        private DefaultPointGridConfigViewModel pointGridViewModel;
        public DeviceOptionsViewModel(DeviceOptions srcModel, CygNetFacilityPointSearchViewModel reactiveCygNetModel, DefaultPointGridConfigViewModel pointViewModel, DefaultFacilityGridViewModel facilityViewModel) : base(srcModel)
        {
            ErrorMessage = "";
            reactiveCygNet = reactiveCygNetModel;
            facilityGridViewModel = facilityViewModel;
            pointGridViewModel = pointViewModel;          
        }

        public ICommand BuildDevice => new DelegateCommand(BuildDeviceTask);
        
        public async void BuildDeviceTask(object inputObject)
        {
            try
            {

                var progDialog = await
                    (Application.Current.MainWindow as MetroWindow).ShowProgressAsync("Building Device", "Starting...");

                ErrorMessage = "";
                var builder = new ModbusDeviceBuilder(
                    MyDataModel.DeviceDefinition.Device,
                    pointGridViewModel.MyDataModel.DefaultCompressorPoints,
                    facilityGridViewModel.MyDataModel.DefaultCompressorFacs,
                    reactiveCygNet.CurrentDDSService, reactiveCygNet.AssocTrsService,
                    pointGridViewModel.MyDataModel.EnumTables,
                    MyDataModel);
                // reactiveCygNet.CurrentDDSService.AddOrReplaceDevice(null);           

                progDialog.SetMessage("Building UIS Commands...");
                var uisSuccess = await Task.Run(() => builder.BuildUisCommands());
                if (!uisSuccess)
                {
                    //ErrorMessage = uisSuccess;
                    //return;
                }

                progDialog.SetMessage("Building Datagroups...");
                var dgSuccess = await Task.Run(() => builder.BuildDataGroups());
                if (!dgSuccess.Item1)
                {
                    ErrorMessage = dgSuccess.Item2;
                    //return;
                }

                progDialog.SetMessage("Building device...");


                var distinctFacGroupIdCount = facilityGridViewModel.MyDataModel.DefaultCompressorFacs
                    .Select(x => x.FacilityGrpId).Distinct().Count();
                var facCount = facilityGridViewModel.MyDataModel.DefaultCompressorFacs.Count();
                var distinctPntFacGroupIdCount = pointGridViewModel.MyDataModel.DefaultCompressorPoints
                    .Select(x => x.DTFPnt.FacGroupTag).Distinct().Count();
                var distinctOrdinalCount = facilityGridViewModel.MyDataModel.DefaultCompressorFacs
                    .Select(x => x.Ordinal).Distinct().Count();
                if (facilityGridViewModel.MyDataModel.DefaultCompressorFacs.Select(x => x.FacilityGrpId).ToList()
                    .Contains(""))
                {
                    ErrorMessage = "Facility Defaults contains blank facility group tags.";
                    await progDialog.CloseAsync();
                    return;
                }

                if (facilityGridViewModel.MyDataModel.DefaultCompressorFacs.Select(x => x.FacilityId).Contains(""))
                {
                    ErrorMessage = "Facility Defaults contains blank facility ID's.";
                    await progDialog.CloseAsync();
                    return;
                }

                if (pointGridViewModel.MyDataModel.DefaultCompressorPoints.Select(x => x.Udc).Contains(""))
                {
                    ErrorMessage = "Point Defaults contains blank udcs.";
                    await progDialog.CloseAsync();
                    return;
                }

                if (pointGridViewModel.MyDataModel.DefaultCompressorPoints.Select(x => x.Udc).Distinct().Count() !=
                    pointGridViewModel.MyDataModel.DefaultCompressorPoints.Count())
                {
                    ErrorMessage = "UDC's must be unique.";
                    await progDialog.CloseAsync();
                    return;
                }

                if (facilityGridViewModel.MyDataModel.DefaultCompressorFacs.Select(x => x.FacilityId).Distinct()
                        .Count() != facCount)
                {
                    ErrorMessage = "Facility ID's must be unique.";
                    await progDialog.CloseAsync();
                    return;
                }

                if (distinctFacGroupIdCount != facCount)
                {
                    ErrorMessage = "Not all facility group id's are unique in Facility Defaults tab.";
                    await progDialog.CloseAsync();
                    return;
                }

                //if(distinctFacGroupIdCount != distinctPntFacGroupIdCount)
                //{
                //    ErrorMessage = "Not every facility group id has a linked point.";
                //    return;
                //}
                if (distinctOrdinalCount != facCount)
                {
                    ErrorMessage = "Every facility must have a unique ordinal.";
                    await progDialog.CloseAsync();
                    return;
                }

                await builder.BuildDevice();
                progDialog.SetMessage("Building facilities...");
                var facSuccess = await Task.Run(() => builder.BuildFacilities());
                if (!facSuccess.Item1)
                {
                    ErrorMessage = facSuccess.Item2;
                    await progDialog.CloseAsync();
                    return;
                }

                progDialog.SetMessage("Building points...");
                var pntSuccess = await Task.Run(() => builder.BuildPoints());
                if (!pntSuccess.isSuccessful)
                {
                    ErrorMessage = $"Failed to build points. Error={pntSuccess.message}";
                    //return; 
                }

                builder.BuildTrsEntries();

                await progDialog.CloseAsync();


                await (Application.Current.MainWindow as MetroWindow).ShowMessageAsync("Status", "Device is built");
            }
            catch (Exception ex)
            {
                ErrorMessage = $"General device build failure with message: {ex.Message}";
                Log.Error(ex, "General device failure");
            }
        }

        public string ErrorMessage
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }


      

    }
}
