using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reactive.Linq;
using System.Threading.Tasks;
using System.Windows;
using CompressorBuilder.DataGrids;
using CompressorBuilder.DataModel.Sub;
using CygNetRuleModel.Models;
using ReactiveUI;
using Techneaux.CygNetWrapper.Facilities;
using Techneaux.CygNetWrapper.Services;
using Techneaux.CygNetWrapper.Services.DDS;
using Techneaux.CygNetWrapper.Services.FAC;
using Techneaux.CygNetWrapper.Services.PNT;
using Techneaux.CygNetWrapper.Services.TRS;
using Techneaux.CygNetWrapper.Summary;

namespace CompressorBuilder.ViewModels
{
    public class CygNetFacilityPointSearchViewModel : ReactiveObject
    {
        public CygNetGeneralOptions CygNetOptsModel { get; }

        public CygNetFacilityPointSearchViewModel(CygNetGeneralOptions cygOpts)
        {
            CygNetOptsModel = cygOpts;
                        
            // Get new domain with cancellation
            var domainChanged =
                this.WhenAnyValue(me => me.CygNetOptsModel.UseCurrentDomain, me => me.CygNetOptsModel.FixedDomainId)
                    .Publish().RefCount();

            

            var updateDomain = ReactiveCommand
                .CreateFromObservable(() => Observable
                    .StartAsync(ct => CygNetCachedFacilityPointModel
                        .GetDomain(CygNetOptsModel.UseCurrentDomain, CygNetOptsModel.FixedDomainId, ct))
                    .TakeUntil(domainChanged));


            domainChanged.Subscribe(_ => updateDomain.Execute().Subscribe());            
            //Observable.FromEventPattern<PropertyChangedEventHandler, PropertyChangedEventArgs>
            //        (h => CygNetOptsModel.PropertyChanged += h, h => CygNetOptsModel.PropertyChanged -= h)
            //    .Subscribe(opts => { Console.WriteLine("Fac Opts event"); });
            
            //Console.WriteLine("First");                  
            _fixedDomainLabel = this.WhenAnyValue(me => me.CygNetOptsModel.UseCurrentDomain)
                .Select(useCurrent => useCurrent ? "Current Domain ID" : "Fixed Domain Id")
                .ToProperty(this, x => x.FixedDomainLabel, "Current Domain ID");

            // Update fac service list when domain is updated
            //_facilityServices = updateDomain
            //    .Select(domain => domain.FacilityServices.ToList())
            //    .ToProperty(this, x => x.FacilityServices, new List<FacilityService>());

         

            _ddsServices = updateDomain
                .Select(domain => domain.DeviceDefinitionServices.ToList())
                .ToProperty(this, x => x.DDSServices, new List<DeviceDefinitionService>());
            // Update current facility service
            //_currentFacilityService = this
            //    .WhenAnyValue(me => me.FacilityServices, me => me.CygNetOptsModel.FacSiteService)
            //    .Select(res => res.Item1.FirstOrDefault(serv => serv.SiteService.ToString() == res.Item2))
            //    .ToProperty(this, x => x.CurrentFacilityService, null);

            _currentDDSService = this
                .WhenAnyValue(me => me.DDSServices, me => me.CygNetOptsModel.DDSService)
                .Select(res => res.Item1.FirstOrDefault(serv => serv.SiteService.ToString() == res.Item2))
                .ToProperty(this, x => x.CurrentDDSService, null);
            // Fac attributes task
            var getFacAttrs =
                ReactiveCommand.CreateFromTask<FacilityService, BindingList<FacilityAttributeWrapper>>(serv =>
                    GetFacilityAttributes(serv));

            this.WhenAnyObservable(me => me.Changed)
                .Where(ch => ch.PropertyName == nameof(CurrentFacilityService))
                .Select(me => (me.Sender as CygNetFacilityPointSearchViewModel).CurrentFacilityService)
                .InvokeCommand(getFacAttrs);

            //_availableFacilityAttributes = getFacAttrs.ToProperty(this, x => x.AvailableFacilityAttributes,
            //    new BindingList<FacilityAttributeWrapper>());

            //this.WhenAnyValue(me => me.AvailableFacilityAttributes)
            //    .Select(attrs => attrs.ToDictionary(attr => attr.ColumnName, attr => attr))
            //    .Subscribe(attrs => { UpdateFacilityAttributeDesc(attrs); });

            // Update domain properties
            _currentDomain = updateDomain.ToProperty(this, x => x.CurrentDomain, null);
            _currentDomainId = updateDomain.Select(dom => dom?.DomainId.ToString() ?? "Unknown")
                .ToProperty(this, x => x.CurrentDomainId, null);

            // Disable FAC dropdown when trying to get domain
            _isGettingDomain = updateDomain.IsExecuting.Select(stat => !stat)
                .ToProperty(this, x => x.IsGettingDomain, false);
            _isServiceSpinnerVisible = updateDomain.IsExecuting
                .Select(stat => stat ? Visibility.Visible : Visibility.Hidden)
                .ToProperty(this, x => x.IsServiceSpinnerVisible, Visibility.Hidden);

            // -- for testing
            PropertyChanged += CygNetFacilityPointSearch_PropertyChanged;

            // Update service ID list
            //_serviceList = this.WhenAnyValue(me => me.FacilityServices)
            //    .Select(list => list.Select(serv => serv.SiteService.ToString()).ToList())
            //    .ToProperty(this, x => x.ServiceList, new List<string>());

            _ddsServiceList = this.WhenAnyValue(me => me.DDSServices)
                .Select(list => list.Select(serv => serv.SiteService.ToString()).ToList())
                .ToProperty(this, x => x.DDSServiceList, new List<string>());

            // Update domain visibility
            _isFixedDomainNumericVisible = this.WhenAnyValue(me => me.CygNetOptsModel.UseCurrentDomain)
                .Select(ucd => ucd ? Visibility.Collapsed : Visibility.Visible)
                .ToProperty(this, x => x.IsFixedDomainNumericVisible, Visibility.Collapsed);



            // Is current domain displayed
            _isCurrentDomainIdVisible = this.WhenAnyValue(me => me.CygNetOptsModel.UseCurrentDomain)
                .Select(ucd => ucd ? Visibility.Visible : Visibility.Collapsed)
                .ToProperty(this, x => x.IsCurrentDomainIdVisible, Visibility.Collapsed);

            var updateAssociatedServices =
             ReactiveCommand.CreateFromTask<DeviceDefinitionService, (FacilityService, PointService, TableReferenceService)>(serv =>
                 CygNetCachedFacilityPointModel.GetAssociatedServices(serv));

            this.WhenAnyObservable(me => me.Changed)
                .Where(ch => ch.PropertyName == nameof(CurrentDDSService))
                .Select(me => (me.Sender as CygNetFacilityPointSearchViewModel).CurrentDDSService)
                .InvokeCommand(updateAssociatedServices);
            
            _assocFacService = updateAssociatedServices
             .Select(x => x.Item1)
             .ToProperty(this, y => y.AssocFacService);

            _assocPntService = updateAssociatedServices
                .Select(x => x.Item2)
                .ToProperty(this, y => y.AssocPntService);

            _assocTrsService = updateAssociatedServices
                .Select(x => x.Item3)
                .ToProperty(this, y => y.AssocTrsService);
            AvailablePointAttributes = PointAttributeWrapper.AllPointAttributeWrappers.Values.ToList();
            AvailableFacilityAttributes = new BindingList<FacilityAttributeWrapper>(FacilityAttributeWrapper.AllPossibleAttrWrappers);
        }

        private readonly ObservableAsPropertyHelper<List<CachedCygNetFacility>> _cachedFacilities;
        public List<CachedCygNetFacility> CachedFacilities => _cachedFacilities.Value;

        private readonly ObservableAsPropertyHelper<bool> _isFacilitiesBusy;
        public bool IsFacilitiesBusy => _isFacilitiesBusy.Value;

        private readonly ObservableAsPropertyHelper<string> _fixedDomainLabel;
        public string FixedDomainLabel => _fixedDomainLabel.Value;

        private readonly ObservableAsPropertyHelper<List<FacilityListSummary.FacilitySummaryResult>> _searchSummary;
        public List<FacilityListSummary.FacilitySummaryResult> SearchSummary => _searchSummary.Value;

        private readonly ObservableAsPropertyHelper<FacilityService> _assocFacService;
        public FacilityService AssocFacService => _assocFacService.Value;

        private readonly ObservableAsPropertyHelper<TableReferenceService> _assocTrsService;

        public TableReferenceService AssocTrsService => _assocTrsService.Value;

        private readonly ObservableAsPropertyHelper<PointService> _assocPntService;
        public PointService AssocPntService => _assocPntService.Value;        

        public string FacSiteService
        {
            get => CygNetOptsModel.FacSiteService;
            set
            {
                CygNetOptsModel.FacSiteService = value;
                this.RaisePropertyChanged();
            }
        }
        public string DDSSvc
        {
            get => CygNetOptsModel.DDSService;
            set
            {
                CygNetOptsModel.DDSService = value;
                this.RaisePropertyChanged();
            }
        }

        private List<PointAttributeWrapper> _availablePointAttributes;

        public List<PointAttributeWrapper> AvailablePointAttributes
        {
            get => _availablePointAttributes;
            set => this.RaiseAndSetIfChanged(ref _availablePointAttributes, value);
        }

        private BindingList<FacilityAttributeWrapper> _availableFacilityAttributes;

        public BindingList<FacilityAttributeWrapper> AvailableFacilityAttributes
        {
            get => _availableFacilityAttributes;
            set => this.RaiseAndSetIfChanged(ref _availableFacilityAttributes, value);
        }


        //private readonly ObservableAsPropertyHelper<BindingList<FacilityAttributeWrapper>> _availableFacilityAttributes;
        //public BindingList<FacilityAttributeWrapper> AvailableFacilityAttributes => _availableFacilityAttributes.Value;

        public static async Task<BindingList<FacilityAttributeWrapper>> GetFacilityAttributes(FacilityService facServ)
        {
            var availableFacAttrs = new BindingList<FacilityAttributeWrapper>();

            if (facServ != null && facServ.IsServiceAvailable)
            {
                if (await Task.Run(() => facServ.RefreshFacServiceAttributesAsync(TimeSpan.FromMinutes(5))))
                {
                    availableFacAttrs = new BindingList<FacilityAttributeWrapper>(facServ.EnabledAttributes.Select(attr => new FacilityAttributeWrapper(attr.Value))
                        .ToList());
                    
                }
            }

            return availableFacAttrs;
        }

        public bool UseCurrentDomain
        {
            get => CygNetOptsModel.UseCurrentDomain;
            set
            {
                CygNetOptsModel.UseCurrentDomain = value;
                this.RaisePropertyChanged();
            }
        }

        public ushort FixedDomainId
        {
            get => CygNetOptsModel.FixedDomainId;
            set
            {
                CygNetOptsModel.FixedDomainId = value;
                this.RaisePropertyChanged();
            }
        }

        private readonly ObservableAsPropertyHelper<bool> _isGettingDomain;
        public bool IsGettingDomain => _isGettingDomain.Value;

        private readonly ObservableAsPropertyHelper<Visibility> _isServiceSpinnerVisible;
        public Visibility IsServiceSpinnerVisible => _isServiceSpinnerVisible.Value;

        private readonly ObservableAsPropertyHelper<Visibility> _isFixedDomainNumericVisible;
        public Visibility IsFixedDomainNumericVisible => _isFixedDomainNumericVisible.Value;

        private readonly ObservableAsPropertyHelper<Visibility> _isCurrentDomainIdVisible;
        public Visibility IsCurrentDomainIdVisible => _isCurrentDomainIdVisible.Value;

        private readonly ObservableAsPropertyHelper<List<string>> _serviceList;
        public List<string> ServiceList => _serviceList.Value;

        private readonly ObservableAsPropertyHelper<List<string>> _ddsServiceList;

        public List<string> DDSServiceList => _ddsServiceList.Value;

        private readonly ObservableAsPropertyHelper<FacilityService> _currentFacilityService;
        public FacilityService CurrentFacilityService => _currentFacilityService?.Value;

        private readonly ObservableAsPropertyHelper<DeviceDefinitionService> _currentDDSService;

        public DeviceDefinitionService CurrentDDSService => _currentDDSService.Value;

        private readonly ObservableAsPropertyHelper<List<FacilityService>> _facilityServices;
        public List<FacilityService> FacilityServices => _facilityServices.Value;

        private readonly ObservableAsPropertyHelper<List<DeviceDefinitionService>> _ddsServices;

        public List<DeviceDefinitionService> DDSServices => _ddsServices.Value;     

        private void CygNetFacilityPointSearch_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(_currentDomain))
            {
                Console.WriteLine("Done");
            }
        }

        private readonly ObservableAsPropertyHelper<CygNetDomain> _currentDomain = null;
        public CygNetDomain CurrentDomain => _currentDomain.Value;

        private readonly ObservableAsPropertyHelper<string> _currentDomainId;
        public string CurrentDomainId => _currentDomainId.Value;
    }
}