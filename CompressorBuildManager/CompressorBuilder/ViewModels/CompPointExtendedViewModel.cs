﻿using System.Windows.Input;
using CompressorBuilder.DataModel.Sub;
using CompressorBuilder.Forms;
using XmlDataModelUtility;

namespace CompressorBuilder.ViewModels
{
    public class CompPointExtendedViewModel : NotifyDynamicBase<CompPointExtended>
    {
        public CompPointExtendedViewModel(CompPointExtended srcModel) : base(srcModel)
        {
            
        }

        public ICommand OpenFacGroupWindow => new DelegateCommand(OpenSelectedFacGroupWindow);

        public void OpenSelectedFacGroupWindow(object inputObject)
        {
            var windowInstance = FacGroupWindow.Instance();

            var success = windowInstance.TryGetKeyDescId(out var result);
            if (!success) return;

            if (result.Value != null)
            {
                MyDataModel.DTFPnt.FacGroupTag = result.Value as string;
            }
        }
    }
}
