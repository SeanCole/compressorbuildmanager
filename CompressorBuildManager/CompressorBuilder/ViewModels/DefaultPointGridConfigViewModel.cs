﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using CompressorBuilder.DataModel.Sub;
using CygNet.Core;

using CygNet.Data.Points;
using Techneaux.CygNetWrapper.Points;
using XmlDataModelUtility;

namespace CompressorBuilder.ViewModels
{
    public class DefaultPointGridConfigViewModel : NotifyDynamicBase<DefaultPointOptions>
    {
        public DefaultPointGridConfigViewModel(DefaultPointOptions srcModel) : base(srcModel)
        {
            //ReactiveCygNetViewModel = new CygNetFacilityPointSearchViewModel(srcModel.CygNetGeneral);
            Data = new BindingList<CompPointExtendedViewModel>();
            PointColumns = new List<ListBoxItem>();
            foreach (var pnt in srcModel.DefaultCompressorPoints)
            {
                Data.Add(new CompPointExtendedViewModel(pnt));
            }
           
        }

        public List<ListBoxItem> PointColumns
        {
            get => GetPropertyValue<List<ListBoxItem>>();
            set => SetPropertyValue(value);
        }

        public CompPointExtendedViewModel SelectedPoint
        {
            get => GetPropertyValue<CompPointExtendedViewModel>();
            set => SetPropertyValue(value);
        }
        
        public BindingList<CompPointExtendedViewModel> Data
        {
            get => GetPropertyValue<BindingList<CompPointExtendedViewModel>>();
            set => SetPropertyValue(value);
        }

        public ICommand AddDefaultRow => new DelegateCommand(AddSelectedDefaultRow);
        public ICommand DeleteDefaultRow => new DelegateCommand(DeleteSelectedDefaultRow);

        public void DeleteSelectedDefaultRow(object inputObject)
        {
            var index = Data.IndexOf(SelectedPoint);
            Data.Remove(SelectedPoint);

            if (Data.FirstOrDefault() == null)
                return;
            if (index < 0)
                index = 0;
            else if (index >= Data.Count)
                index = Data.Count - 1;
            SelectedPoint = Data[index];
        }

        public void AddSelectedDefaultRow(object inputObject)
        {
            var newGroup = new CompPointExtended();
            var newGroupView = new CompPointExtendedViewModel(newGroup);
            if (SelectedPoint == null)
            {
                //MyData.FacilityDefaults.Add(newGroup);
                MyDataModel.DefaultCompressorPoints.Add(newGroup);
                Data.Add(newGroupView);
                SelectedPoint = Data[Data.Count - 1];
                return;
            }

            var index = Data.IndexOf(SelectedPoint);
            if (index < 0)
                index = 0;
            else if (index >= Data.Count)
                index = Data.Count - 1;

            //  MyData.FacilityDefaults.Insert(index, newGroup);
            MyDataModel.DefaultCompressorPoints.Insert(index, newGroup);
            Data.Insert(index, newGroupView);

            if (index < 0)
                index = 0;
            else if (index >= Data.Count)
                index = Data.Count - 1;

            SelectedPoint= Data[index];

        }

        #region Static Helpers

        /// <summary>
        /// Initializes the given <see cref="ListBox"/> to allow hiding/showing columns at runtime.
        /// </summary>
        /// <param name="columnSelectorListBox">The <see cref="ListBox"/> control to use as a selector.</param>
        /// <param name="columns">A <see cref="ReadOnlyCollection{T}"/> of the datagrid's columns.</param>
        public static void UseColumnSelector(ref ListBox columnSelectorListBox, ReadOnlyCollection<DataGridColumn> columns)
        {
            // Create column selector
            int i = 0;
            var items = new Dictionary<int, string>();
            foreach (var c in columns)
            {
                if (i > 4) items.Add(i, c.Header.ToString());
                i++;
            }

            columnSelectorListBox.ItemsSource = items;
            columnSelectorListBox.SelectedValue = "Key";
            columnSelectorListBox.SelectionMode = SelectionMode.Multiple;
            columnSelectorListBox.DisplayMemberPath = "Value";
            columnSelectorListBox.SelectionChanged += (o, args) =>
            {
                if (!(o is ListBox sender)) return;
                foreach (KeyValuePair<int, string> item in sender.Items)
                {
                    if (sender.SelectedItems.Contains(item))
                    {
                        columns[item.Key].Visibility = Visibility.Visible;
                    }
                    else
                    {
                        columns[item.Key].Visibility = Visibility.Hidden;
                    }
                }
            };
        }

        /// <summary>
        /// Adds the necessary datagrid columns to the given datagrid to support the <see cref="Data"/> object for this view model.
        /// </summary>
        /// <param name="pointsDataGrid">The datagrid bound to <see cref="Data"/>.</param>
        public static void AddGridColumns(ref DataGrid pointsDataGrid)
        {
            var attrs = PointAttribute.AllPointAttributes.Where(x=>x.Value.Category != PointAttribute.Categories.Tag && x.Value.Description != "Enumeration Table Name").ToList();

            foreach (PointAttribute.AttrValueType avt in Enum.GetValues(typeof(PointAttribute.AttrValueType)))
            {
                // add the columns for each group
                foreach (var attr in attrs.Where(x => x.Value.ValueType == avt).ToList())
                {
                    var val = attr.Value;

                    string bindingPath = val.EnumClassIndex.HasValue
                        ? $"PointRecord.{val.ClassPropertyName}[{val.EnumClassIndex.Value}]"
                        : $"PointRecord.{val.ClassPropertyName}";

                    // Handle enum type special
                    if (avt == PointAttribute.AttrValueType.Enum)
                    {
                        // Do enum/combobox binding logic here
                        var enumPropInfo = typeof(PointConfigRecord).GetProperty(val.ClassPropertyName);
                        if (enumPropInfo == null) continue;
                        var valDict = Enum.GetValues(enumPropInfo.PropertyType)
                            .Cast<int>()
                            .ToDictionary(num => Enum.ToObject(enumPropInfo.PropertyType, num), num => Enum.GetName(enumPropInfo.PropertyType,num));


                        var newCol = new DataGridComboBoxColumn
                        {
                            DisplayMemberPath = "Value",
                            SelectedValuePath = "Key",
                            ItemsSource = valDict,
                            Header = val.Description,
                            SelectedValueBinding = new Binding($"{bindingPath}")
                            {
                                Mode = BindingMode.TwoWay,
                                UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged
                            },
                            Visibility = Visibility.Hidden
                        };
                        
                        pointsDataGrid.Columns.Add(newCol);

                        // continue to break out of loop
                        continue;
                    }

                    var propInfo = typeof(PointConfigRecord).GetProperty(val.ClassPropertyName);
                    //if (propInfo?.CanWrite != true && (propInfo.PropertyType != typeof(Indexer<string>) ||
                    //                                   propInfo.PropertyType != typeof(Indexer<uint>) ||
                    //                                   propInfo.PropertyType != typeof(Indexer<bool>)))
                    //{
                    //    continue;
                    //}
                    DataGridBoundColumn col = null;
                    switch (attr.Value.ValueType)
                    {
                        case PointAttribute.AttrValueType.Bool:
                            col = new DataGridCheckBoxColumn();
                            break;
                        case PointAttribute.AttrValueType.DateTime:
                            continue;
                        // TODO: Are there any other types that won't default to DataGridTextColumn?
                        default:
                            col = new DataGridTextColumn();
                            break;
                    }

                    // Hidden by default
                    col.Visibility = Visibility.Hidden;
                    // Set column header
                    col.Header = val.Description;
                    // Bind the class property name based on nullable EnumClassIndex
                    col.Binding = new Binding(bindingPath)
                    {
                        Mode = BindingMode.TwoWay,
                        UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged
                    };
                    // Add finished column to data grid
                    pointsDataGrid.Columns.Add(col);
                }
            }

        }

        #endregion

    }
}
