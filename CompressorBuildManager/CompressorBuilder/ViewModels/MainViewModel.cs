﻿using System.Collections.Generic;
using System.Windows.Controls;
using CompressorBuilder.DataModel;
using XmlDataModelUtility;


namespace CompressorBuilder.ViewModels
{

    public class MainViewModel : NotifyDynamicBase<SavedFacPointDefaultsConfigModel>
    {
        public MainViewModel(SavedFacPointDefaultsConfigModel srcModel) : base(srcModel)
        {

            ReactiveCygNetViewModel = new CygNetFacilityPointSearchViewModel(srcModel.FacilityDefaultGridConfig.CygNetGeneral);
            DefaultFacilityGridVm = new DefaultFacilityGridViewModel(srcModel.DefaultFacilityOptions);
            PointDefaultsGridVM = new DefaultPointGridConfigViewModel(srcModel.DefaultPointOptions);
            DeviceOptionsVM = new DeviceOptionsViewModel(srcModel.DeviceOpts, ReactiveCygNetViewModel, PointDefaultsGridVM, DefaultFacilityGridVm);
            //BuildDeviceVm = new BuildDeviceVm(); 
        }
     
        public CygNetFacilityPointSearchViewModel ReactiveCygNetViewModel
        {
            get => GetPropertyValue<CygNetFacilityPointSearchViewModel>();
            set => SetPropertyValue(value);
        }

        
        
        public DefaultFacilityGridViewModel DefaultFacilityGridVm
        {
            get => GetPropertyValue<DefaultFacilityGridViewModel>();
            set => SetPropertyValue(value);
        }

        public DeviceOptionsViewModel DeviceOptionsVM
        {
            get => GetPropertyValue<DeviceOptionsViewModel>();
            set => SetPropertyValue(value);
        }

        public DefaultPointGridConfigViewModel PointDefaultsGridVM
        {
            get => GetPropertyValue<DefaultPointGridConfigViewModel>();
            set => SetPropertyValue(value);
        }

    }

}
