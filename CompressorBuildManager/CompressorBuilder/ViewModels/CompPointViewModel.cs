﻿using System.Windows.Input;
using CompressorBuilder.Forms;
using CompressorTemplateDataModel;
using XmlDataModelUtility;

namespace CompressorBuilder.ViewModels
{
    public class CompPointViewModel : NotifyDynamicBase<CompPoint>
    {
        public CompPointViewModel(CompPoint srcModel) : base(srcModel)
        {

        }
        
        public ICommand OpenEnumTableWindow => new DelegateCommand(OpenSelectEnumTableWindow);

        public void OpenSelectEnumTableWindow(object inputObject)
        {
            var windowInstance = EnumTableChooser.Instance();
            var success = windowInstance.TryGetKeyDescId(out var result);
            if (!success) return;
            if (result.Value == null) return;
            MyDataModel.StatusEnumTableName = result.Value as string;
            MyDataModel.StatusEnumTableDescription = result.Description as string;
        }
    }
}
