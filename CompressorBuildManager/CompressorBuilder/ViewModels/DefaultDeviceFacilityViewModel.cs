﻿using CompressorBuilder.DataModel.Sub;
using XmlDataModelUtility;

namespace CompressorBuilder.ViewModels
{
    public class DefaultDeviceFacilityViewModel: NotifyDynamicBase<DeviceFacilityConfig>
    {
        public DefaultDeviceFacilityViewModel(DeviceFacilityConfig srcModel) : base(srcModel)
        {
            
        }
    }
}
