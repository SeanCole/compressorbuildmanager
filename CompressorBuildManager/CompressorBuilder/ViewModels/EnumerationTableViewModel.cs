using System.ComponentModel;
using System.Linq;
using System.Windows.Input;
using GenericRuleModel.Rules;
using XmlDataModelUtility;

namespace CompressorBuilder.ViewModels
{
    public class EnumerationTableViewModel : NotifyDynamicBase<EnumerationTable>
    {
        public EnumerationTableViewModel(EnumerationTable srcModel) : base(srcModel)
        {
            EnumerationPairViewModelList = new BindingList<EnumerationTablePairViewModel>();
            foreach (var enumPair in srcModel.EnumerationPairs)
            {
                EnumerationPairViewModelList.Add(new EnumerationTablePairViewModel(enumPair));
            }
        }

        public BindingList<EnumerationTablePairViewModel> EnumerationPairViewModelList
        {
            get => GetPropertyValue<BindingList<EnumerationTablePairViewModel>>();
            set => SetPropertyValue(value);
        }

        public EnumerationTablePairViewModel SelectedPair
        {
            get => GetPropertyValue<EnumerationTablePairViewModel>();
            set => SetPropertyValue(value);
        }

        public ICommand AddEnumPair => new DelegateCommand(AddSelectEnumPair);

        public ICommand DeleteEnumPair => new DelegateCommand(DeleteSelectEnumPair);

        public void DeleteSelectEnumPair(object inputObject)
        {

            var index = EnumerationPairViewModelList.IndexOf(SelectedPair);
            EnumerationPairViewModelList.Remove( SelectedPair);

            if (MyDataModel.EnumerationPairs.ElementAtOrDefault(index) == null)
                return;

            MyDataModel.EnumerationPairs.RemoveAt(index);

            if (EnumerationPairViewModelList.FirstOrDefault() == null)
                return;

            if (index < 0)
                index = 0;
            else if (index >= EnumerationPairViewModelList.Count)
                index = EnumerationPairViewModelList.Count - 1;

            SelectedPair = EnumerationPairViewModelList[index];
        }

        public void AddEnumPairValues(string key, string value)
        {
            if (SelectedPair == null)
            {

                var newPair = new EnumerationTable.EnumerationPair();
                newPair.Key = key;
                newPair.Value = value;
                MyDataModel.EnumerationPairs.Insert(EnumerationPairViewModelList.Count, newPair);
                
                var pairViewModel = new EnumerationTablePairViewModel(newPair);

                EnumerationPairViewModelList.Add(pairViewModel);
                SelectedPair = EnumerationPairViewModelList[EnumerationPairViewModelList.Count - 1];
                return;
            }

            var index = EnumerationPairViewModelList.IndexOf(SelectedPair);
            if (index < 0)
                index = 0;
            else if (index >= EnumerationPairViewModelList.Count)
                index = EnumerationPairViewModelList.Count - 1;

            var newEnumPair = new EnumerationTable.EnumerationPair();
            newEnumPair.Key = key;
            newEnumPair.Value = value;
            MyDataModel.EnumerationPairs.Insert(EnumerationPairViewModelList.Count, newEnumPair);

            var newTablePairViewModel = new EnumerationTablePairViewModel(newEnumPair);

            EnumerationPairViewModelList.Insert(index, newTablePairViewModel);
            if (index < 0)
                index = 0;
            else if (index >= EnumerationPairViewModelList.Count)
                index = EnumerationPairViewModelList.Count - 1;

            SelectedPair = EnumerationPairViewModelList[index];
        }


        public void AddSelectEnumPair(object inputObject)
        {
            if (SelectedPair == null)
            {
                
                var newPair = new EnumerationTable.EnumerationPair();
                MyDataModel.EnumerationPairs.Insert(EnumerationPairViewModelList.Count, newPair);



                var pairViewModel = new EnumerationTablePairViewModel(newPair);


                EnumerationPairViewModelList.Add(pairViewModel);
                SelectedPair = EnumerationPairViewModelList[EnumerationPairViewModelList.Count - 1];
                return;
            }

            var index = EnumerationPairViewModelList.IndexOf(SelectedPair);
            if (index < 0)
                index = 0;
            else if (index >=  EnumerationPairViewModelList.Count)
                index = EnumerationPairViewModelList.Count - 1;


            var newEnumPair = new EnumerationTable.EnumerationPair();
            MyDataModel.EnumerationPairs.Insert(EnumerationPairViewModelList.Count, newEnumPair);

            var newTablePairViewModel = new EnumerationTablePairViewModel(newEnumPair);

            EnumerationPairViewModelList.Insert(index, newTablePairViewModel);


            if (index < 0)
                index = 0;
            else if (index >= EnumerationPairViewModelList.Count)
                index = EnumerationPairViewModelList.Count - 1;

            SelectedPair = EnumerationPairViewModelList[index];

        }


    }
}

