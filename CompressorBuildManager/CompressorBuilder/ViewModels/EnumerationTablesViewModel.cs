using System.ComponentModel;
using System.Linq;
using System.Windows.Input;
using GenericRuleModel.Rules;
using XmlDataModelUtility;

namespace CompressorBuilder.ViewModels
{
    public class EnumerationTablesViewModel : NotifyDynamicBase<EnumerationTables>
    {
        public EnumerationTablesViewModel(EnumerationTables srcModel) : base(srcModel)
        {
            EnumerationTablesViewModelList = new BindingList<EnumerationTableViewModel>();
            foreach (var enumTable in srcModel.EnumTables)
            {
                EnumerationTablesViewModelList.Add(new EnumerationTableViewModel(enumTable));
            }
        }

        public BindingList<EnumerationTableViewModel> EnumerationTablesViewModelList
        {
            get => GetPropertyValue<BindingList<EnumerationTableViewModel>>();
            set => SetPropertyValue(value);
        }

        public ICommand AddEnumTableRow => new DelegateCommand(AddSelectEnumTableRow);

        public EnumerationTableViewModel SelectedEnumTable
        {
            get => GetPropertyValue<EnumerationTableViewModel>();
            set => SetPropertyValue(value);
        }

        public ICommand DeleteEnumTableRow => new DelegateCommand(DeleteSelectEnumTableRow);

        public void DeleteSelectEnumTableRow(object inputObject)
        {

            var index = EnumerationTablesViewModelList.IndexOf(SelectedEnumTable);
            EnumerationTablesViewModelList.Remove(SelectedEnumTable);

            if (MyDataModel.EnumTables.ElementAtOrDefault(index) == null)
                return;

            MyDataModel.EnumTables.RemoveAt(index);

            if (EnumerationTablesViewModelList.FirstOrDefault() == null)
                return;

            if (index < 0)
                index = 0;
            else if (index >= EnumerationTablesViewModelList.Count)
                index = EnumerationTablesViewModelList.Count - 1;

            SelectedEnumTable = EnumerationTablesViewModelList[index];
        }

        public void AddSelectEnumTableRow(object inputObject)
        {
            if (SelectedEnumTable == null)
            {


                var newEnumTable = new EnumerationTable();
                MyDataModel.EnumTables.Insert(EnumerationTablesViewModelList.Count, newEnumTable);



                var tableViewModel = new EnumerationTableViewModel(newEnumTable);


                EnumerationTablesViewModelList.Add(tableViewModel);
                SelectedEnumTable = EnumerationTablesViewModelList[EnumerationTablesViewModelList.Count - 1];
                return;
            }

            var index = EnumerationTablesViewModelList.IndexOf(SelectedEnumTable);
            if (index < 0)
                index = 0;
            else if (index >= EnumerationTablesViewModelList.Count)
                index = EnumerationTablesViewModelList.Count - 1;


            var newTable = new EnumerationTable();
            MyDataModel.EnumTables.Insert(EnumerationTablesViewModelList.Count, newTable);

            var newTableViewModel = new EnumerationTableViewModel(newTable);

            EnumerationTablesViewModelList.Insert(index, newTableViewModel);


            if (index < 0)
                index = 0;
            else if (index >= EnumerationTablesViewModelList.Count)
                index = EnumerationTablesViewModelList.Count - 1;

            SelectedEnumTable = EnumerationTablesViewModelList[index];
        }


    }
}
