﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Windows.Input;
using ReactiveUI;
using XmlDataModelUtility;

namespace CompressorBuilderWPF.ViewModels
{
    public class RuleGridViewModel<T1, T2> : NotifyModelBase where T1 : NotifyCopyDataModel, new() where T2 : NotifyDynamicBase<T1>
    {
        // T1 = source rule data model
        // T2 = rule row view model

        public BindingList<T1> SourceRules { get; }

        public RuleGridViewModel(BindingList<T1> srcRules)
        {
            Rows = new BindingList<T2>(srcRules.Select(rule => (T2)Activator.CreateInstance(typeof(T2), rule)).ToList());
            SourceRules = srcRules;
        }

        public BindingList<T2> Rows
        {
            get => GetPropertyValue<BindingList<T2>>();
            set => SetPropertyValue(value);
        }

        public ICommand AddRow => new DelegateCommand(AddNewRow);

        public void AddNewRow(object inputObject)
        {
            int index;

            if (SelectedRule == null)
            {
                index = Rows.Count;
            }
            else
            {
                index = Rows.IndexOf(SelectedRule);
            }

            var newRule = new T1();
            SourceRules.Insert(index, newRule);

            var newRuleView = (T2)Activator.CreateInstance(typeof(T2), newRule);
            Rows.Insert(index, newRuleView);
            SelectedRule = Rows[Rows.Count - 1];
        }

        public ICommand DeleteRow => new DelegateCommand(DeleteSelectedRow);

        public void DeleteSelectedRow(object inputObject)
        {

            var index = Rows.IndexOf(SelectedRule);

            if (index == -1)
                return;
            SourceRules.RemoveAt(index);
            Rows.Remove(SelectedRule);
            if (Rows.FirstOrDefault() == null)
                return;

            if (index < 0)
                index = 0;
            else if (index >= Rows.Count)
                index = Rows.Count - 1;

            SelectedRule = Rows[index];
        }

        public T2 SelectedRule
        {
            get => GetPropertyValue<T2>();
            set => SetPropertyValue(value);
        }

        public void DragAndDrop(int dragIndex, int dropIndex)
        {
            if (Rows.FirstOrDefault() == null)
                return;

            var draggedRow = Rows[dragIndex];
            var draggedRule = SourceRules[dragIndex];
            Rows.RemoveAt(dragIndex);

            SourceRules.RemoveAt(dragIndex);
            if (dragIndex < 0)
                dragIndex = 0;
            else if (dragIndex >= Rows.Count)
                dragIndex = Rows.Count - 1;     

            SourceRules.Insert(dropIndex, draggedRule);
            Rows.Insert(dropIndex, draggedRow);
            SelectedRule = Rows[dragIndex];
        }
        

    }
}
