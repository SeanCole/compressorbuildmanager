using System;
using System.IO;
using System.Windows;
using System.Xml.Serialization;
using CompressorBuilder.DataModel;
using CompressorTemplateDataModel;
using Microsoft.Win32;
using XmlDataModelUtility;

namespace CompressorBuilder.ViewModels
{
    public class ConfigModelFileService : NotifyCopyDataModel
    {
        public event EventHandler ConfigModified;
        public event EventHandler NewConfigCreated;

        [XmlIgnore]
        public Window OwnerWindow { get; private set; } = null;

        public ConfigModelFileService(Window owner)
        {
            OwnerWindow = owner;
            InitNewConfig();
        }

        public ConfigModelFileService()
        {
        }

        public bool ConfigIsModified
        {
            get => GetPropertyValue<bool>();
            set
            {
                SetPropertyValue(value);

                if (value)
                {
                    SaveButtonVisibility = Visibility.Visible;
                }
                else
                {
                    SaveButtonVisibility = Visibility.Hidden;
                }
            }
        }

        public Visibility SaveButtonVisibility
        {
            get => GetPropertyValue<Visibility>();
            set => SetPropertyValue(value);
        }

        public string FilePath
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }

        public string FileName
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }
      
        public string FileNameWithoutExtension
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }

        [XmlIgnore]
        public SavedFacPointDefaultsConfigModel ThisConfigModel { get; private set; }

        [XmlIgnore]
        public CompProgram LoadedConfig { get; private set; }

        // Attempt to check for unsaved changes
        public bool TryClose()
        {
            var configSaveStatus = CheckConfigSaved();

            if (configSaveStatus == SaveStatuses.DialogCancelled ||
              configSaveStatus == SaveStatuses.SaveFailed)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        //public void SetFilePath(string fullPath)
        //{
        //    FullFilePath = new FileInfo(fullPath);
        //}

        public void ResetModified()
        {
            ConfigIsModified = false;
        }

        public bool CreateNewConfig()
        {
            if (TryClose())
            {
                InitNewConfig();
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool OpenConfigFilePath(string configFilePath)
        {
            //// CODE HERE => Implement recent folder
            //if (!TryClose())
            //    return false;

            //var openFileDlg = new OpenFileDialog
            //{
            //    Filter = "XML Files (*.xml) | *.xml",
            //    DefaultExt = "xml"
            //};



            //if (openFileDlg.ShowDialog(OwnerWindow) == true)
            //{
            //    var thisConfigPath = openFileDlg.FileName;

            var loadedConfig = XmlFileSerialization<SavedFacPointDefaultsConfigModel>.LoadModelFromFile(configFilePath);

            if (loadedConfig == null)
            {
                //Log.Error("Failed to load config file. Please check the format or create a new one.");


                return false;
            }
            else
            {
                FilePath = Path.GetDirectoryName(configFilePath);
                FileName = Path.GetFileName(configFilePath);
                FileNameWithoutExtension = Path.GetFileNameWithoutExtension(configFilePath);

                ThisConfigModel = loadedConfig;
                ThisConfigModel.PropertyChanged += SyncConfig_PropertyChanged;
                return true;
            }
        }

        public bool OpenTemplateConfigFile()
        {
            {
                // CODE HERE => Implement recent folder
                if (!TryClose())
                    return false;

                var openFileDlg = new OpenFileDialog
                {
                    Filter = "XML Files (*.xml) | *.xml",
                    DefaultExt = "xml"
                };

                if (openFileDlg.ShowDialog(OwnerWindow) == true)
                {
                    var thisConfigPath = openFileDlg.FileName;

                    var loadedConfig = XmlFileSerialization<CompProgram>.LoadModelFromFile(thisConfigPath);

                    if (loadedConfig == null)
                    {
                        MessageBox.Show(OwnerWindow, "Failed to load config file. Please check the format or create a new one.");
                        InitNewConfig();

                        return false;
                    }
                    else
                    {
                        FilePath = Path.GetDirectoryName(thisConfigPath);
                        FileName = Path.GetFileName(thisConfigPath);
                        FileNameWithoutExtension = Path.GetFileNameWithoutExtension(thisConfigPath);

                        LoadedConfig = loadedConfig;
          
                        PropertyChanged += SyncConfig_PropertyChanged;

                        return true;
                    }
                }

                return false;
            }
        }

        public bool OpenConfigFile()
        {
            // CODE HERE => Implement recent folder
            if (!TryClose())
                return false;

            var openFileDlg = new OpenFileDialog
            {
                Filter = "XML Files (*.xml) | *.xml",
                DefaultExt = "xml"
            };

            if (openFileDlg.ShowDialog(OwnerWindow) == true)
            {
                var thisConfigPath = openFileDlg.FileName;

                var loadedConfig = XmlFileSerialization<SavedFacPointDefaultsConfigModel>.LoadModelFromFile(thisConfigPath);

                if (loadedConfig == null)
                {
                    MessageBox.Show(OwnerWindow, "Failed to load config file. Please check the format or create a new one.");
                    InitNewConfig();
                    return false;
                }
                else
                {
                    FilePath = Path.GetDirectoryName(thisConfigPath);
                    FileName = Path.GetFileName(thisConfigPath);
                    FileNameWithoutExtension = Path.GetFileNameWithoutExtension(thisConfigPath);

                    ThisConfigModel = loadedConfig;
                    ThisConfigModel.PropertyChanged += SyncConfig_PropertyChanged;

                    return true;
                }
            }

            return false;
        }

        public bool SaveConfigFile(bool saveNew = false)
        {
            if (string.IsNullOrWhiteSpace(FilePath) || saveNew)
            {
                var saveFileDialog = new SaveFileDialog
                {
                    // Browse for path
                    DefaultExt = "xml",
                    Filter = "XML Files (*.xml)|*.xml"
                };

                if (saveFileDialog.ShowDialog(OwnerWindow) == true)
                {
                    FilePath = Path.GetDirectoryName(saveFileDialog.FileName);
                    FileName = Path.GetFileName(saveFileDialog.FileName);
                    FileNameWithoutExtension = Path.GetFileNameWithoutExtension(saveFileDialog.FileName);
                }
                else
                {
                    return false;
                }
            }

            try
            {
                //Check if the config file path exists
                if (!Directory.Exists(Path.GetDirectoryName(FilePath)))
                {
                    MessageBox.Show(OwnerWindow, "Config File path is invalid. Save failed.");
                    return false;
                }

                if (SaveConfig())
                {
                    ConfigIsModified = false;
                    return true;
                }
                else
                {
                    MessageBox.Show(OwnerWindow, "Failed to save file. Please verify that the file is not open in another program " +
                        "and that you have permission to write to the destination folder.");
                    return false;
                }
            }
            catch (Exception ex)
            {
                //if (ThrowExceptions) { throw; }
                //Log.Error(ex, "Failure saving config file");
                MessageBox.Show(OwnerWindow, $"Failed to save file with exception: {ex.Message}");
                return false;
            }
        }

        private void InitNewConfig()
        {
            FilePath = null;
            FileName = "New Compressor Config.xml";
            FileNameWithoutExtension = "New Compressor Config";

            ThisConfigModel = new SavedFacPointDefaultsConfigModel();
            ThisConfigModel.PropertyChanged += SyncConfig_PropertyChanged;

            NewConfigCreated?.Invoke(this, new EventArgs());
            //_SyncCygService = new SyncCygnetService(ThisConfigModel.CygNetGeneral);
        }

        private void SyncConfig_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            ConfigIsModified = true;
            ConfigModified?.Invoke(this, new EventArgs());
        }

        private bool SaveConfig()
        {
            try
            {
                using (var sw = new StreamWriter(Path.Combine(FilePath, FileName)))
                {
                    //XmlSerializer xmlserial = new XmlSerializer(typeof(T), new XmlRootAttribute("SyncConfig"));

                    var xmlSerial = new XmlSerializer(typeof(SavedFacPointDefaultsConfigModel), DerivedTypes);
                    xmlSerial.Serialize((TextWriter) sw, (object) ThisConfigModel);
                    return true;
                }
            }
            catch (Exception ex)
            {
               // Log.Error(ex, $@"Error saving config file to {FilePath}\{FileName}");
                return false;
            }
        }

        private SaveStatuses CheckConfigSaved()
        {
            if (ConfigIsModified)
            {
                var saveButton = MessageBox.Show(
                        OwnerWindow,
                        "The current config has not been saved, would you like to save it now?",
                        "Save File",
                        MessageBoxButton.YesNoCancel);

                switch (saveButton)
                {
                    case MessageBoxResult.Cancel:
                        return SaveStatuses.DialogCancelled;

                    case MessageBoxResult.Yes:
                        if (SaveConfigFile())
                        {
                            return SaveStatuses.SaveSucceeded;
                        }
                        else
                        {
                            return SaveStatuses.SaveFailed;
                        }
                    case MessageBoxResult.No:
                        return SaveStatuses.SaveRejected;

                    default:
                        return SaveStatuses.DialogCancelled;
                }
            }

            return SaveStatuses.SaveNotNeeded;
        }

        private enum SaveStatuses
        {
            SaveNotNeeded,
            SaveSucceeded,
            SaveFailed,
            SaveRejected,
            DialogCancelled
        }
    }
}
