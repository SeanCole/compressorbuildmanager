﻿using System.ComponentModel;
using CompressorTemplateDataModel;
using XmlDataModelUtility;

namespace CompressorBuilder.ViewModels
{
    public class CompProgramViewModel : NotifyDynamicBase<CompProgram>
    {
        public CompProgramViewModel(CompProgram srcModel) : base(srcModel)
        {
            CompPointList = new BindingList<CompPointViewModel>();
            for (var i = 0; i < srcModel.CompPoints.Count; i++)
            {
                var model = new CompPointViewModel(MyDataModel.CompPoints[i]);
                CompPointList.Add(model);
            }
        
        }
        public BindingList<CompPointViewModel> CompPointList
        {
            get => GetPropertyValue<BindingList<CompPointViewModel>>();
            set => SetPropertyValue(value);
        }

    }
}
