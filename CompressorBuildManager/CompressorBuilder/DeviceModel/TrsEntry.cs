﻿namespace CompressorBuilder.DeviceModel
{
    public partial class ModbusDeviceBuilder
    {
        public class TrsEntry
        {
            public string TableCol;
            public string EntryCol;
            public string DescCol;

            public TrsEntry(string table, string entry, string desc)
            {
                TableCol = table;
                EntryCol = entry;
                DescCol = desc;
            }

            public override bool Equals(object obj)
            {
                var compareObj = (TrsEntry)obj;
                return (TableCol == compareObj.TableCol && EntryCol == compareObj.EntryCol && DescCol == compareObj.DescCol);
            }
            //public override int GetHashCode()
            //{
            //    return ToString().GetHashCode();
            //}

        }
    }
}
