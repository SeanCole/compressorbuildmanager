﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CompressorBuilder.DataModel.Sub;
using CygNet.Data.Core;
using CygNet.Data.Facilities;
using CygNet.Data.Points;
using Techneaux.CygNetWrapper.Services;
using Techneaux.CygNetWrapper.Services.DDS;
using Techneaux.CygNetWrapper.Services.DDS.Devices;
using Techneaux.CygNetWrapper.Services.FAC;
using Techneaux.CygNetWrapper.Services.PNT;
using Techneaux.CygNetWrapper.Services.TRS;
using GenericRuleModel.Rules;
using System.Threading;
using System.Data.Odbc;
using Serilog;

namespace CompressorBuilder.DeviceModel
{
    public partial class ModbusDeviceBuilder
    {
        private DeviceDefinitions.CygNetDevice MyDevice { get; }
        public DeviceDefinitionService DdsServ { get; }
        private List<CompPointExtended> SrcPoints { get; }
        private List<DeviceFacilityConfig> SrcFacs { get; }
        private EnumerationTables EnumTables { get; }
        private TableReferenceService TrsServ { get; }
        public DeviceOptions DevOpts { get; set; }


        public ModbusDeviceBuilder(
            DeviceDefinitions.CygNetDevice srcDevice,
            IEnumerable<CompPointExtended> points,
            IEnumerable<DeviceFacilityConfig> facs,
            DeviceDefinitionService ddsServ,
            TableReferenceService trsServ,
            EnumerationTables enumerationTables,
            DeviceOptions deviceOpts)
        {
            EnumTables = enumerationTables;
            TrsServ = trsServ;
            MyDevice = srcDevice;
            DdsServ = ddsServ;
            SrcPoints = points.ToList();
            SrcFacs = facs.ToList();
            DevOpts = deviceOpts;

            MyDevice.Id = deviceOpts.ID.ToUpper();
        }

        private async Task<(CurrentValueService uis, PointService pnt, FacilityService fac)> GetServices()
        {
            var uisService = await DdsServ.GetAssociatedUis();
            var pntService = await uisService.GetAssociatedPnt();
            var facService = await uisService.GetAssociatedFac();

            return (uisService, pntService, facService);
        }

        private async Task<(Dictionary<string, FacilityRecord>, string)> GetFinalFacilitiesList()
        {
            var (uis, _, _) = await GetServices();
            try
            {
                var facDict = new Dictionary<string, FacilityRecord>();
                foreach (var deviceFacilityConfig in SrcFacs)
                {
                    deviceFacilityConfig.Facility.Category = "DDSFAC";

                    if (deviceFacilityConfig.FacilityId == null)
                    {
                        return (null, "Please update all facility id's in grid and make sure they are in proper format.");
                    }
                    var facTag = new FacilityTag(uis.SiteService, deviceFacilityConfig.FacilityId);

                    deviceFacilityConfig.Facility.Tag = facTag;
                    facDict[deviceFacilityConfig.FacilityGrpId] = deviceFacilityConfig.Facility;
                }
                return (facDict, "");
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Failed to get facilities.");
                return (null, ex.Message);
            }
        }

        public async Task<(Dictionary<string, PointConfigRecord>, string)> GetFinalPoints()
        {
            var facs = await GetFinalFacilitiesList();
            if (facs.Item1 == null)
                return (null, facs.Item2);
            var success = true;
            SrcPoints
                .ForEach(pnt =>
                {
                    pnt.PointRecord.Description = pnt.DTFPnt.Description;
                    FacilityRecord dictFac;
                    if (facs.Item1.TryGetValue(pnt.DTFPnt.FacGroupTag, out dictFac))
                    {
                        pnt.PointRecord.Tag = new PointTag(dictFac.Tag, pnt.Udc);
                    }
                    else
                    {
                        success = false;
                    }
                });
            if (!success)
                return (null, "Point fac group tag does not exist.");

            SrcPoints.Where(pnt => !string.IsNullOrWhiteSpace(pnt.StatusEnumTableName))
                    .ToList()
                    .ForEach(pnt =>
                    {
                        pnt.PointRecord.PointDataType = "EI";
                        pnt.PointRecord.EnumTableName = pnt.StatusEnumTableName;
                    });

            //SrcPoints
            //    .ForEach(pnt => pnt.PointRecord.Tag = pntServ.ServiceClient.ResolveTag(
            //        new PointTag(facs.Item1[pnt.DTFPnt.FacGroupTag].Tag, pnt.Udc)));

            //SrcPoints.ForEach(pnt => pnt.PointRecord.Tag. = facs.Item1[pnt.DTFPnt.FacGroupTag].Tag.FacilityId  + "_" + pnt.Udc);
            var finalPnts = SrcPoints.ToDictionary(pt => pt.DTFPnt.RegTag, pt => pt.PointRecord);

            return (finalPnts, "");
        }

        public async Task<(bool isSuccessful, string message)> BuildPoints()
        {
            var services = await GetServices();
            var pnts = (await GetFinalPoints());
            if (pnts.Item1 == null)
                return (false, pnts.Item2);

            //return (true, "");

            var result = await services.pnt.AddOrUpdatePoints(pnts.Item1.Values);

            if (result.isSuccess)
                return (true, "");

            return (false, $"Point with facility=[{result.pointAffected?.Tag.FacilityId}], udc=[{result.pointAffected?.Tag.UDC}] failed with error {result.ex.Message}");
        }

        public async Task<(bool, string)> BuildFacilities()
        {
            var (_, _, fac) = await GetServices();
            var facs = await GetFinalFacilitiesList();
            if (facs.Item1 == null)
                return (false, facs.Item2);
            var result = await fac.AddOrUpdateFacilities(facs.Item1.Values);

            return (result);
        }

        public async Task<(bool, string)> BuildDataGroups()
        {
            var pnts = await GetFinalPoints();
            if (pnts.Item1 == null)
                return (false, pnts.Item2);
            MyDevice.DataGroups.Clear();

            var dgElms = new List<DatagroupElement>();
            var mappings = new List<UdcMapping>();

            MyDevice.FacilityLinks = SrcFacs.Select(fac => new DeviceDefinitions.CygNetDevice.FacilityLink
            {
                Ordinal = fac.Ordinal,
                Id = fac.FacilityId
            }).ToList();

            foreach (var pt in SrcPoints)
            {
                var pntTag = pnts.Item1[pt.DTFPnt.RegTag].Tag;

                var newElm = new DatagroupElement
                {
                    ElmId = pt.DTFPnt.RegTag,
                    Desc = pt.DTFPnt.Description,
                    Type = pt.DTFPnt.DataType.ToString(),
                    RegNum = DevOpts.BaseAddress + pt.DTFPnt.Address
                };

                dgElms.Add(newElm);

                var newMapping = new UdcMapping
                {
                    ElementId = pt.DTFPnt.RegTag,
                    Facility = pntTag.FacilityId,
                    Udc = pt.Udc
                };

                mappings.Add(newMapping);
            }

            var newDg = new DgDefinition.DataGroup
            {
                Attributes = new DgDefinition.DataGroup.DataGroupAttributes
                {
                    Ordinal = 0,
                    Description = "Compressor Status",
                    DataGroupType = "CfgMB",
                    DataGroupSecurityApp = "DDS",
                    DataGroupSecurityEvent = "ACCESS",
                    FacilityId = MyDevice.Id,
                    DataGroupSpecific = $"CurTime={DateTime.Now:yyyyMMddhhmmssfff};HasMetadata=1;"
                },

                UdcMappings = mappings,

                MetaData = new DgDefinition.DataGroup.DataGroupMetaData
                {
                    DataStore =
                    {
                        Version = 1,
                        DgDefinition = new DgDefinition
                        {
                            Copyable = true,
                            Format = "xml",

                            NamedDatagroup = new NamedDatagroup
                            {
                                Name = "CfgMB",
                                IsDeviceDataGroup = false,
                                AudoAdd = false,
                                BaseOrd = 0,
                                MajorRev = 1,
                                MinorRev = 1,
                                CanSend = false,
                                CanRecv = true,
                                IsUccSend = false,
                                IsUccRecv = true,
                                NiceName = "Compressor Status",
                                DgElements =
                                {
                                    ChildElements = dgElms,
                                    ByteOrder = "bigEndian",
                                    SecurityLevel = 4
                                }
                            }
                        }
                    }
                }
            };

            newDg.MetaData.DataStore.DgDefinition.NamedDatagroup.ModbusReadBlocks = GetModbusBlocks();
            MyDevice.DataGroups.Add(newDg);

            return (true, "");
        }

        private List<ModbusReadWriteBlock> GetModbusBlocks()
        {
            var sortedSrcPoints = SrcPoints.OrderBy(pnt => pnt.DTFPnt.Address).ToList();

            var blocks = new List<ModbusReadWriteBlock>();
            var blockNum = 1;

            var regNum = 0;
            while (regNum < SrcPoints.Count)
            {
                var sublist = new List<CompPointExtended>();

                for (var i = regNum; i < SrcPoints.Count; i++)
                {
                    if (sublist.Any() && sortedSrcPoints[i].DTFPnt.Address - sublist.First().DTFPnt.Address > 40)
                    {
                        regNum = i;
                        break;
                    }

                    if (i > 0 &&
                        sublist.Any() &&
                        sortedSrcPoints[i].DTFPnt.Address - sortedSrcPoints[i - 1].DTFPnt.Address > 8)
                    {
                        regNum = i;
                        break;
                    }

                    sublist.Add(sortedSrcPoints[i]);
                    regNum = i + 1;
                }



                var newblk = new ModbusReadWriteBlock();
                newblk.RegNum = DevOpts.BaseAddress + sublist.First().DTFPnt.Address;
                newblk.BlockId = $"block{blockNum}";
                newblk.RegByteLen = 2;
                newblk.RegCnt = sublist.Last().DTFPnt.Address - sublist.First().DTFPnt.Address + 1;
                newblk.RegOff = DevOpts.program.RegOffset;
                

                blocks.Add(newblk);

                blockNum++;
            }

            return blocks;
        }

        //public bool LinkFacilities(IEnumerable<DeviceFacilityConfig> facs)
        //{
        //    throw new NotImplementedException();
        //}

        public bool BuildUisCommands()
        {
            MyDevice.UisCommands.Clear();

            // SEAN HERE
            var newCmd = new UisCommand
            {
                Attributes =
                {
                    Name = "COMPSTAT",
                    Facility = MyDevice.Id,
                    Type = "",
                    LockTime = DateTime.Now.ToOADate(),
                    VerTime = DateTime.Now.ToOADate(),
                    Description = "Compressor Status"
                },
                CommandComponents =
                {
                    new UisCommand.Component
                    {
                        Type = "DG_F_DEV",
                        Position = 0,

                        Param =
                        {
                            new UisCommand.Component.ComponentParam
                            {
                                Key = "DGORD",
                                Value = "0"
                            },
                            new UisCommand.Component.ComponentParam
                            {
                                Key = "DGTYPE",
                                Value = "CfgMB"
                            }
                        }
                    }
                }
            };

            MyDevice.UisCommands.Add(newCmd);
            return true;
        }

        public async Task<bool> BuildDevice()
        {
            return await DdsServ.AddOrReplaceDevice(MyDevice);
        }

        public void BuildTrsEntries()
        {
            var cts = new CancellationTokenSource();
            AddTrsTable(SrcPoints.Select(x => x.StatusEnumTableName).ToList(), cts.Token);
        }

        private List<string> GetSqlCmdForInsertTrs(List<TrsEntry> pairList)
        {
            var commandList = new List<string>();
            //check if any of the above tables exist

            foreach (var pair in pairList)
            {
                var siteService = TrsServ.DomainSiteService.SiteService.ToString();
                siteService = siteService.Replace(".", "_");
                siteService = siteService.ToLower();
                var sqlCommand = $@"INSERT INTO {siteService}.table_header_record (table_name , table_entry, [desc]) " +
                                 $@"VALUES ('{pair.TableCol}', '{pair.EntryCol}', '{pair.DescCol}');";
                commandList.Add(sqlCommand);
            }

            return commandList;
        }

        private List<TrsEntry> GetAllPairs(List<string> tableIds)
        {
            if (EnumTables?.EnumTables == null || EnumTables.EnumTables.Count == 0)
                return new List<TrsEntry>();
            var pairList = new List<TrsEntry>();

            var pointTables = new List<EnumerationTable>();
            foreach (var pntEnum in SrcPoints.Select(x => x.StatusEnumTableName).Distinct().ToList())
            {
                if (pntEnum != null)
                {
                    var table = EnumTables.GetTable(pntEnum);
                    if (table != null)
                    {
                        pointTables.Add(table);
                    }
                }
            }

            foreach (var tbl in pointTables)
            {
                foreach (var pair in tbl.EnumerationPairs)
                {
                    pairList.Add(new TrsEntry(tbl.Name, pair.Key, pair.Value));
                }
            }
            return pairList;
        }

        private async void AddTrsTable(List<string> tableIds,
        CancellationToken ct)
        {
            var pairList = GetAllPairs(tableIds);
            try
            {
                var dsn = "DSN=CygNet;ServiceFilter=" + TrsServ.DomainSiteService.SiteService;
                using (var dbConnection = new OdbcConnection(dsn))
                {
                    await dbConnection.OpenAsync(ct);

                    using (var dbCommand = dbConnection.CreateCommand())
                    {
                        dbCommand.CommandTimeout = 300;
                        var siteService = TrsServ.DomainSiteService.SiteService.ToString();
                        siteService = siteService.Replace(".", "_");
                        siteService = siteService.ToLower();
                        var sqlQuery = $@"SELECT table_name, table_entry, [desc] " +
                        $@" FROM {siteService}.table_header_record;";
                        dbCommand.CommandText = sqlQuery;

                        var dbReader = await dbCommand.ExecuteReaderAsync(ct);

                        while (dbReader.Read())
                        {
                            ct.ThrowIfCancellationRequested();
                            var tempEntry = new TrsEntry(dbReader["table_name"] as string, dbReader["table_entry"] as string, dbReader["desc"] as string);
                            if (pairList.Contains(tempEntry))
                            {
                                pairList.Remove(tempEntry);
                            }
                        }
                    }
                    var sqlInsertQuery = GetSqlCmdForInsertTrs(pairList);
                    if (sqlInsertQuery == null)
                        return;
                    foreach (var command in sqlInsertQuery)
                    {
                        using (var dbCommandInsert = dbConnection.CreateCommand())
                        {
                            dbCommandInsert.CommandTimeout = 300;

                            dbCommandInsert.CommandText = command;
                            var dbReaderInsert = await dbCommandInsert.ExecuteNonQueryAsync(ct);
                        }
                    }
                    return;
                }
            }
            catch (OperationCanceledException) { throw; }
            catch (OdbcException ex) when (ex.HResult == -2146232009)
            {
                Log.Warning(ex, "ODBC Operation cancelled");
            } // Odbc cancellation exception
            catch (Exception ex)
            {
                Log.Error(ex, "Error getting simple facility list");
                // General exception
            }
        }
    }
}
