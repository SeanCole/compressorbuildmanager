﻿using System.Windows.Controls;

namespace CompressorBuilder.View
{
    /// <summary>
    /// Interaction logic for FacilityAttributeEnabler.xaml
    /// </summary>
    public partial class FacilityAttributeEnabler : UserControl
    {
        public FacilityAttributeEnabler()
        {
            InitializeComponent();
        }
    }
}
