﻿using MahApps.Metro.Controls;
using System;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using CompressorBuilder.ViewModels;
using Application = System.Windows.Application;
using GenericRuleModel.Helper;

namespace CompressorBuilder.Forms
{
    /// <summary>
    /// Interaction logic for FacilityLinkingAttributeWindow.xaml
    /// </summary>
    public partial class FacGroupWindow : MetroWindow
    {
        public static object StaticDataContext { get; set; }
      
        public FacGroupWindow()
        {
            InitializeComponent();

            FacGroupListView.MouseDoubleClick += FacGroupListView_MouseDoubleClick;
        }

        private void FacGroupListView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var selectedRowItem = FacGroupListView.SelectedItem as DefaultDeviceFacilityViewModel;
            if (selectedRowItem == null)
            {
                return;
            }
            
            ThisChooserResult.Value = selectedRowItem.MyDataModel.FacilityGrpId;
            ThisChooserResult.Description = "";

            WasCancelled = false;

            Close();
        }

        private void OnCloseExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            Hide();
        }

        public static FacGroupWindow Instance()
        {
            if (StaticDataContext == null)
            {
                throw new InvalidOperationException("Static Data Context must not be null");
            }

            var instance = new FacGroupWindow
            {
                DataContext = StaticDataContext,
                Owner = Application.Current.MainWindow
            };

            return instance;
        }

        public bool TryGetKeyDescId(out ValueDescription result)
        {
            ShowDialog();

            result = ThisChooserResult;
            return !WasCancelled;
        }

        public bool WasCancelled = false;
        public ValueDescription ThisChooserResult { get; private set; } = new ValueDescription();
        

        private void SelectButton_Click(object sender, RoutedEventArgs e)
        {
            var selectedRowItem = FacGroupListView.SelectedItem as DefaultDeviceFacilityViewModel;
            if (selectedRowItem == null)
            {
                return;
            }


            ThisChooserResult.Value = selectedRowItem.MyDataModel.FacilityGrpId;
            ThisChooserResult.Description = "";
            WasCancelled = false;
            Close();
        }

        private void CancelButtonClick(object sender, RoutedEventArgs e)
        {

            WasCancelled = true;
            Close();
        }
    }
}
