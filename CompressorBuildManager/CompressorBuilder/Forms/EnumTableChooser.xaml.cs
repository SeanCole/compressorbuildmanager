﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using CompressorBuilder.ViewModels;
using GenericRuleModel.Helper;
using GenericRuleModel.Rules;
using MahApps.Metro.Controls;

namespace CompressorBuilder.Forms
{
    /// <summary>
    /// Interaction logic for FacilityLinkingAttributeWindow.xaml
    /// </summary>
    public partial class EnumTableChooser : MetroWindow
    {
        public EnumTableChooser()
        {
            InitializeComponent();
            
            StaticDataContext = new EnumerationTablesViewModel(EnumerationTableHandler.EnumTableDefinition);
            
        }

        private void OnCloseExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            Hide();
        }

        public static object StaticDataContext;
        public static EnumTableChooser Instance()
        {
            var instance = new EnumTableChooser();
            if (StaticDataContext == null)
            {
                throw new InvalidOperationException("Static Data Context must not be null");
            }
            instance.DataContext = StaticDataContext;

            return instance;
        }

        //private void EnumTablesView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        //{
        //    EnumerationTableViewModel selectedItem = EnumTablesView.SelectedItem as EnumerationTableViewModel;
        //    if (selectedItem == null)
        //    {
        //        return;
        //    }

        //    ThisChooserResult.Value = selectedItem.MyData.Name;
        //    ThisChooserResult.Description = selectedItem.MyData.Description;

        //    WasCancelled = false;

        //    this.Close();
        //}


        public bool TryGetKeyDescId(out ValueDescription result)
        {
            ShowDialog();

            result = _thisChooserResult;
            return !_wasCancelled;
        }

        private bool _wasCancelled = false;
        private readonly ValueDescription _thisChooserResult = new ValueDescription();

        private void SelectButton_Click(object sender, RoutedEventArgs e)
        {
            var selectedItem = (EnumerationTableViewModel)EnumTablesView.SelectedItem;
            if (selectedItem == null)
            {
                return;
            }

            _thisChooserResult.Value = ((dynamic)selectedItem).Name;
            _thisChooserResult.Description = ((dynamic)selectedItem).Description;

            _wasCancelled = false;

            Close();
        }

        private void PasteButton_Click(object sender, RoutedEventArgs e)
        {
            var selectedItem = (EnumerationTableViewModel)EnumTablesView.SelectedItem;
            if (selectedItem == null)
            {

                return;
            }
            try
            {
                if (Clipboard.ContainsText(TextDataFormat.Text))
                {
                    string clipboardText = Clipboard.GetText(TextDataFormat.Text);
                    var enumPairs = clipboardText.Split(new[] { Environment.NewLine }, StringSplitOptions.None).ToList();
                    if(enumPairs.Count - 1 > 0)
                    {
                        enumPairs.RemoveAt(0);
                        foreach (var pair in enumPairs)
                        {
                            if (pair.Contains("\t"))
                            {
                                var KeyValue = pair.Split(new[] { "\t" }, StringSplitOptions.None);
                                selectedItem.AddEnumPairValues(KeyValue[0], KeyValue[1]);
                            }

                        }
                    }

                }
            }
            catch(Exception ex)
            {
                MessageBox.Show($"Failed to paste clipboard text with exception: {ex.Message}");

            }

        }

        private void CancelButtonClick(object sender, RoutedEventArgs e)
        {

            _wasCancelled = true;
            Close();
        }


    }
}
