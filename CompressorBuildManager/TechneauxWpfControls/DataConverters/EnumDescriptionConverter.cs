using System;
using System.Collections.Generic;
using GenericRuleModel.Helper;
using System.Windows.Data;
using System.Globalization;
using System.Windows.Markup;
using TechneauxReportingDataModel.Helper;

namespace TechneauxWpfControls.DataConverters
{
    [ValueConversion(typeof(Enum), typeof(IEnumerable<ValueDescription>))]
    public class EnumDescriptionConverter : MarkupExtension, IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            

             return EnumHelper.GetAllValuesAndDescriptions(value.GetType());
   
        }
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }
    }
}
