using System;
using System.Windows.Data;
using System.Globalization;
using System.Windows.Markup;


namespace TechneauxWpfControls.DataConverters
{
    public class NullStringConverter : MarkupExtension, IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var IdString = value as string;

            if (string.IsNullOrEmpty(IdString))
            {
                return "[None]";
            }
            else
            {
                return IdString;
            }
        }
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }

    }
}
