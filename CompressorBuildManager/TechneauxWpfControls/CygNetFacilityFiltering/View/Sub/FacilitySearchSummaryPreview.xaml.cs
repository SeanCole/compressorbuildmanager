using System.Windows.Controls;


namespace TechneauxWpfControls.CygNetFacilityFiltering.View.Sub
{
    /// <summary>
    /// Interaction logic for FacilitySearchSummaryPreview.xaml
    /// </summary>
    public partial class FacilitySearchSummaryPreview : UserControl
    {
        public FacilitySearchSummaryPreview()
        {
            InitializeComponent();
        }
    }

}
