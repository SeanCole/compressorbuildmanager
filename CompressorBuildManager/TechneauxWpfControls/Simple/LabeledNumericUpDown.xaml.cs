using System.Windows;
using System.Windows.Controls;
using MahApps.Metro.Controls;

namespace TechneauxWpfControls.Simple
{
    /// <summary>
    /// Interaction logic for LabeledNumericUpDown.xaml
    /// </summary>
    public partial class LabeledNumericUpDown : UserControl
    {
 
        public static readonly RoutedEvent ValueIncreasedEvent = EventManager.RegisterRoutedEvent(
        "ValueIncreased", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(LabeledNumericUpDown));
        public static readonly RoutedEvent ValueDecreasedEvent = EventManager.RegisterRoutedEvent(
        "ValueDecreased", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(LabeledNumericUpDown));


        public static readonly DependencyProperty LabelProperty
            = DependencyProperty.Register(
                  "Label",
                  typeof(string),
                  typeof(LabeledNumericUpDown),
                  new PropertyMetadata("")
                  );

        public static readonly DependencyProperty HasDecimalsProperty
           = DependencyProperty.Register(
                 "HasDecimals",
                 typeof(bool),
                 typeof(LabeledNumericUpDown),
                 new PropertyMetadata(false)
                 );
        public static readonly DependencyProperty ValueProperty
           = DependencyProperty.Register(
                 "Value",
                 typeof(double),
                 typeof(LabeledNumericUpDown),
                 new FrameworkPropertyMetadata((double)0, (FrameworkPropertyMetadataOptions.BindsTwoWayByDefault),
        new PropertyChangedCallback(OnMyValuePropertyChanged))
                 );

        public static readonly DependencyProperty IntervalNumberProperty
   = DependencyProperty.Register(
         "IntervalNumber",
         typeof(double),
         typeof(LabeledNumericUpDown),
                new FrameworkPropertyMetadata((double)1, (FrameworkPropertyMetadataOptions.BindsTwoWayByDefault),
        new PropertyChangedCallback(OnMyValuePropertyChanged))
                 );


        public static readonly DependencyProperty MinProperty
            = DependencyProperty.Register(
                "Min",
            typeof(decimal),
         typeof(LabeledNumericUpDown),
         new PropertyMetadata((decimal)0)
         );

        public static readonly DependencyProperty ReadOnlyProperty
    = DependencyProperty.Register(
 "ReadOnly",
 typeof(bool),
 typeof(LabeledNumericUpDown),
 new PropertyMetadata(false)
 );

        public static readonly DependencyProperty ManualEnterProperty
= DependencyProperty.Register(
"ManualEnter",
typeof(bool),
typeof(LabeledNumericUpDown),
new PropertyMetadata(true)
);

        public static readonly DependencyProperty MaxProperty
             = DependencyProperty.Register(
         "Max",
         typeof(decimal),
         typeof(LabeledNumericUpDown),
         new PropertyMetadata((decimal)9999)
         );

        public static readonly DependencyProperty OrientationProperty
     = DependencyProperty.Register(
     "Orientation",
 typeof(Orientation),
 typeof(LabeledNumericUpDown),
 new PropertyMetadata(Orientation.Vertical)
 );

        public static readonly DependencyProperty NumericVisibilityProperty
= DependencyProperty.Register(
"NumericVisibility",
typeof(Visibility),
typeof(LabeledNumericUpDown),
new PropertyMetadata(Visibility.Visible)
);

        public LabeledNumericUpDown()
        {
            InitializeComponent();
        }
        static LabeledNumericUpDown()
        {
            EventManager.RegisterClassHandler(typeof(LabeledNumericUpDown), NumericUpDown.ValueIncrementedEvent, new NumericUpDownChangedRoutedEventHandler(OnValueIncreased));
            EventManager.RegisterClassHandler(typeof(LabeledNumericUpDown), NumericUpDown.ValueDecrementedEvent, new NumericUpDownChangedRoutedEventHandler(OnValueDecreased));
        }
        public string Label
        {
            get => (string)GetValue(LabelProperty);
            set => SetValue(LabelProperty, value);
        }
        private static void OnMyValuePropertyChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs e)
        {

        }
        public bool HasDecimals
        {
            get => (bool)GetValue(HasDecimalsProperty);
            set => SetValue(HasDecimalsProperty, value);
        }
        public bool ReadOnly
        {
            get => (bool)GetValue(ReadOnlyProperty);
            set => SetValue(ReadOnlyProperty, value);
        }

        public bool ManualEnter
        {
            get => (bool)GetValue(ManualEnterProperty);
            set => SetValue(ManualEnterProperty, value);
        }
        public double Value
        {
            get => (double)GetValue(ValueProperty);
            set => SetValue(ValueProperty, value);
        }
        public double IntervalNumber
        {
            get => (double)GetValue(IntervalNumberProperty);
            set => SetValue(IntervalNumberProperty, value);
        }
        public event RoutedEventHandler ValueIncreased
        {
            add => AddHandler(ValueIncreasedEvent, value);
            remove => RemoveHandler(ValueIncreasedEvent, value);
        }
        public event RoutedEventHandler ValueDecreased
        {
            add => AddHandler(ValueDecreasedEvent, value);
            remove => RemoveHandler(ValueDecreasedEvent, value);
        }
        public decimal Min
        {
            get => (decimal)GetValue(MinProperty);
            set => SetValue(MinProperty, value);
        }

        public decimal Max
        {
            get => (decimal)GetValue(MaxProperty);
            set => SetValue(MaxProperty, value);
        }

        public Orientation Orientation
        {
            get => (Orientation)GetValue(OrientationProperty);
            set => SetValue(OrientationProperty, value);
        }

        public Visibility NumericVisibility
        {
            get => (Visibility)GetValue(NumericVisibilityProperty);
            set => SetValue(NumericVisibilityProperty, value);
        }

        private void NumericUpDown_GotFocus(object sender, RoutedEventArgs e)
        {
            var focused = sender as NumericUpDown;
            focused.HideUpDownButtons = false;
        }

        private void NumericUpDown_LostFocus(object sender, RoutedEventArgs e)
        {
            var LostFocused = sender as NumericUpDown;
            LostFocused.HideUpDownButtons = true;
        }

        static void OnValueIncreased(object sender, NumericUpDownChangedRoutedEventArgs e)
        {
            var invoker = sender as LabeledNumericUpDown;
            invoker.RaiseValueIncreasedEvent();
        }
        static void OnValueDecreased(object sender, NumericUpDownChangedRoutedEventArgs e)
        {
            
            var invoker = sender as LabeledNumericUpDown;
            invoker.RaiseValueDecreasedEvent();
        }
        void RaiseValueDecreasedEvent()
        {
            var newEventArgs = new RoutedEventArgs(ValueDecreasedEvent);
            RaiseEvent(newEventArgs);
        }

        void RaiseValueIncreasedEvent()
        {
           
            var newEventArgs = new RoutedEventArgs(ValueIncreasedEvent);
            RaiseEvent(newEventArgs);
        }


        //private void NumericControl_ValueIncremented(object sender, NumericUpDownChangedRoutedEventArgs args)
        //{
        //    RaiseValueIncreasedEvent();
        //}

        //private void NumericControl_ValueDecremented(object sender, NumericUpDownChangedRoutedEventArgs args)
        //{
        //    RaiseValueDecreasedEvent();
        //}
    }
}
