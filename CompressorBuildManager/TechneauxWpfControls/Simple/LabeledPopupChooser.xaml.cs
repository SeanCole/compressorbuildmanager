using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace TechneauxWpfControls.Simple
{
    /// <summary>
    /// Interaction logic for LabeledPopupChooser.xaml
    /// </summary>
    public partial class LabeledPopupChooser : UserControl
    {
        public static readonly DependencyProperty TextKeyProperty
           = DependencyProperty.Register(
                 "TextKey",
                 typeof(string),
                 typeof(LabeledPopupChooser),
                 new PropertyMetadata("", new PropertyChangedCallback(TextKeyCallback))
                 );
        public static readonly DependencyProperty HorizontalAlignmentLabelProperty
   = DependencyProperty.Register(
         "HorizontalAlignmentLabel",
         typeof(HorizontalAlignment),
         typeof(LabeledPopupChooser),
         new PropertyMetadata(HorizontalAlignment.Center)
         );
        public static readonly DependencyProperty HorizontalContentAlignmentLabelProperty
    = DependencyProperty.Register(
    "HorizontalContentAlignmentLabel",
    typeof(HorizontalAlignment),
    typeof(LabeledPopupChooser),
    new PropertyMetadata(HorizontalAlignment.Center)
     );

        public static readonly DependencyProperty TextDescriptionProperty
            = DependencyProperty.Register(
         "TextDescription",
         typeof(string),
         typeof(LabeledPopupChooser),
         new PropertyMetadata("")
         );

        public static readonly DependencyProperty TextPromptProperty
    = DependencyProperty.Register(
 "TextPrompt",
 typeof(string),
 typeof(LabeledPopupChooser),
 new PropertyMetadata("Please choose an attribute.")
 );

        public static readonly DependencyProperty ButtonCommandProperty
= DependencyProperty.Register(
"ButtonCommand",
typeof(ICommand),
typeof(LabeledPopupChooser),
new PropertyMetadata(null)
);

        public static readonly DependencyProperty LabelProperty
           = DependencyProperty.Register(
                 "Label",
                 typeof(string),
                 typeof(LabeledPopupChooser),
                 new PropertyMetadata("")
                 );

        public static readonly DependencyProperty OrientationProperty = 
            DependencyProperty.Register(
                "Orientation",
                typeof(Orientation),
                typeof(LabeledPopupChooser),
                new PropertyMetadata(Orientation.Vertical));

        public LabeledPopupChooser()
        {

            InitializeComponent();
            (Content as FrameworkElement).DataContext = this;

        }
        static LabeledPopupChooser()
        {

        }
        public string TextKey
        {
            get => (string)GetValue(TextKeyProperty);
            set => SetValue(TextKeyProperty, value);
        }

        public HorizontalAlignment HorizontalAlignmentLabel
        {
            get => (HorizontalAlignment)GetValue(HorizontalAlignmentLabelProperty);
            set => SetValue(HorizontalAlignmentLabelProperty, value);
        }

        public HorizontalAlignment HorizontalContentAlignmentLabel
        {
            get => (HorizontalAlignment)GetValue(HorizontalContentAlignmentLabelProperty);
            set => SetValue(HorizontalContentAlignmentLabelProperty, value);
        }

        public Orientation Orientation
        {
            get => (Orientation)GetValue(OrientationProperty);
            set => SetValue(OrientationProperty, value);
        }

        public string Label
        {
            get => (string)GetValue(LabelProperty);
            set => SetValue(LabelProperty, value);
        }

        public string TextDescription
        {
            get => (string)GetValue(TextDescriptionProperty);
            set => SetValue(TextDescriptionProperty, value);
        }
        public string TextPrompt
        {
            get => (string)GetValue(TextPromptProperty);
            set => SetValue(TextPromptProperty, value);
        }


        //
        private static void TextKeyCallback(DependencyObject property, DependencyPropertyChangedEventArgs e)
        {
            var popupChooser = (LabeledPopupChooser)property;
            popupChooser.TextKey = (string)e.NewValue;

            // now, do something
        }


        public ICommand ButtonCommand
        {
            get => (ICommand)GetValue(ButtonCommandProperty);
            set => SetValue(ButtonCommandProperty, value);
        }
    }
}

