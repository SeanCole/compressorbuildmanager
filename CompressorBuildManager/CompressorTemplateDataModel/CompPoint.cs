﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XmlDataModelUtility;

namespace CompressorTemplateDataModel
{
    public class CompPoint : NotifyCopyDataModel
    {
        public CompPoint() : base()
        {
            InUse = false;
            StatusEnumTableName = "";
            StatusEnumTableDescription = "";
            DTFPnt = new DTFPoint();
        }

        public DTFPoint DTFPnt
        {
            get => GetPropertyValue<DTFPoint>();
            set => SetPropertyValue(value);
        }
        public bool InUse
        {
            get => GetPropertyValue<bool>();
            set => SetPropertyValue(value);
        }

        public string StatusEnumTableName
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }

        public string StatusEnumTableDescription
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }
    }
}
