﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using GenericRuleModel.Rules;
using XmlDataModelUtility;

namespace CompressorTemplateDataModel
{
    public class CompProgram : NotifyCopyDataModel
    {
        public CompProgram()
        {
            CompPoints = new BindingList<CompPoint>();
            Description = "";
            ID = "";
            RegOffset = 0;
        }

        public int RegOffset
        {
            get => GetPropertyValue<int>();
            set => SetPropertyValue(value);
        }

        public string ID
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }

        public uint BaseAddress
        {
            get => GetPropertyValue<uint>();
            set => SetPropertyValue(value);
        }
        public uint OrdinalOffset
        {
            get => GetPropertyValue<uint>();
            set => SetPropertyValue(value);
        }

        public string Description
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }

        public List<CompPoint> CompPointsInUse
        {
            get => GetPropertyValue<List<CompPoint>>();
            set => SetPropertyValue(value);
        }

        public BindingList<CompPoint> CompPoints
        {
            get => GetPropertyValue<BindingList<CompPoint>>();
            set => SetPropertyValue(value);
        }

        public EnumerationTables EnumTables { get; set; } = new EnumerationTables();

    }
}
