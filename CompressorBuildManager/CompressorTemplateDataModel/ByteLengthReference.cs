﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static CompressorTemplateDataModel.DTFPoint;

namespace CompressorTemplateDataModel
{
    public static class ByteLengthReference
    {
        public static Dictionary<ModBusDataTypes, int> ByteRef = new Dictionary<ModBusDataTypes, int>
        {
            {ModBusDataTypes.BOOLEAN, 1},
            {ModBusDataTypes.I1, 1 },
            {ModBusDataTypes.I2, 2 },
            {ModBusDataTypes.I4, 4 },
            {ModBusDataTypes.R2, 2 }, 
            {ModBusDataTypes.R4, 4 },
            {ModBusDataTypes.R8, 8 },
            {ModBusDataTypes.UI1, 1 },
            {ModBusDataTypes.UI2, 2 },
            {ModBusDataTypes.UI4, 4},
            {ModBusDataTypes.CHAR, 1 },
            {ModBusDataTypes.unknown, -1}
                

        };
    }
}
