﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XmlDataModelUtility;

namespace CompressorTemplateDataModel
{
    public class DTFPoint : NotifyCopyDataModel
    {
        public DTFPoint()
        {
            DefaultUDC = "";

            DataType = ModBusDataTypes.unknown;
            Description = "";
            Grouping = "";
            FacGroupTag = "";
            Address = 0;
            RegTag = "";
        }

        public enum ModBusDataTypes
        {
            unknown,
            R2,
            R4,
            //[Description("Unsigned intger 1 byte")]
            UI1,
            UI2,
            UI4,
            I1,
            I2,
            I4,
            R8,
            CHAR,
            BOOLEAN        
        }        
    
        public string DefaultUDC
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }
        


        public string Description
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }
        public string Grouping
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }

       

        public uint Address
        {
            get => GetPropertyValue<uint>();
            set => SetPropertyValue(value);
        }

        public string FacGroupTag
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }
 

        public ModBusDataTypes DataType
        {
            get => GetPropertyValue<ModBusDataTypes>();
            set => SetPropertyValue(value);
        }

        public string RegTag
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }

 
    }
}
